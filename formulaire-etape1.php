<!DOCTYPE html>
<html>

<head>
<meta charset="utf-8">
<title>Le site d'Orléans et son AgglO - Formulaire</title>
<?php include('blocs/styles.php') ?>

    <body>
        <?php include( 'blocs/header.php') ?>
        <div class="container formulaire">
            <ol class="breadcrumb hidden-xs">
                <li><a href="index.php">Accueil</a>
                </li>
                <li><a href="#">Formulaire de contacte</a>
                </li>
                <li><a href="#">Intervention </a>
                </li>
                <li class="active">Demande d'arrêté de circulation</li>
            </ol>
            <section>
                <header>
                    <h1>Demande d'arrêté de circulation</h1>
                    <ul class="list-unstyled etape">
                        <li class="active"><span class="step">1</span> Demandeur</li>
                        <br class="visible-sm visible-xs">
                        <li class="inactive "><a href="formulaire-etape2.php"><span class="step">2</span> Informations sur les travaux</a>
                        </li>
                        <br class="visible-sm visible-xs">
                        <li class="inactive "><a href="formulaire-etape3.php"><span class="step">3</span> Mesures de circulation sollicitées</a>
                        </li>
                        <br class="visible-sm visible-xs">
                        <li class="inactive "><a href="formulaire-etape4.php"><span class="step">4</span> Validation</a>
                        </li>
                    </ul>
                </header>
                <h2 class="sr-only">Formulaire de demande - Première étape</h2>
                <form class="form-horizontal" method="post">
                    <h3 class="sous-titre">Demandeur</h3>	
                    <div class="require">
                        <div class="form-group">
                            <label class="control-label col-md-4">*Vous êtes</label>
                            <div class="col-md-4">
                                <div class="radio">
                                    <label>
                                        <input type="radio" class="tx-artificaform-pi1-2782-condition-field1385635257-0" value="une entreprise" name="tx_artificaform_pi1[field2]">une entreprise
                                    </label>
                                    <br>
                                    <label>
                                        <input type="radio" class="tx-artificaform-pi1-2782-condition-field1385635257-1" value="un particulier" name="tx_artificaform_pi1[field2]">un particulier
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="require">
                        <div class="form-group">
                            <label class="control-label col-md-4">*Adresse</label>
                            <div class="col-md-4">
                                <textarea class="form-control" rows="3" placeholder="Adresse"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="require">
                        <div class="form-group has-success">
                            <label class="control-label col-md-4" for="inputSuccess1">*Code postal</label>
                            <div class="col-md-4">
                                <input type="text" class="form-control" placeholder="Code postal" id="inputSuccess1">
                                <span class="fa fa-ok form-control-feedback" aria-hidden="true"></span>
                                <span id="inputSuccess1Status" class="sr-only">(success)</span>
                            </div>
                        </div>
                    </div>
                    <div class="require">
                        <div class="form-group has-error">
                            <label class="control-label col-md-4" for="inputError1">*Ville</label>
                            <div class="col-md-4">
                                <input class="form-control" placeholder="Ville" id="inputError1">
                                <span class="fa fa-remove form-control-feedback" aria-hidden="true"></span>
                                <span id="inputError1Status" class="sr-only">(error)</span>
                            </div>
                        </div>
                    </div>
                    <div class="require">
                        <div class="form-group">
                            <label class="control-label col-md-4">*Téléphone</label>
                            <div class="col-md-4">
                                <input class="form-control" placeholder="Téléphone">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4">Fax</label>
                        <div class="col-md-4">
                            <input class="form-control" placeholder="Fax">
                        </div>
                    </div>
                    <div class="require">
                        <div class="form-group">
                            <label class="control-label col-md-4">*Email</label>
                            <div class="col-md-4">
                                <input type="email" class="form-control" id="inputEmail3" placeholder="Email">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4">Pour le compte de</label>
                        <div class="col-md-4">
                            <input class="form-control" placeholder="Pour le compte de">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4">Nom du responsable</label>
                        <div class="col-md-4">
                            <input class="form-control" placeholder="Nom du responsable">
                        </div>
                    </div>
                    <div class="require">
                        <div class="form-group">
                            <label class="control-label col-md-4">*Dossier suivi par la Ville d'Orléans ou l'agglomération</label>
                            <div class="col-md-4">
                                <div class="radio">
                                    <label>
                                        <input type="radio" class="tx-artificaform-pi1-2782-condition-field1385635257-0" value="oui" name="tx_artificaform_pi1[field2]">oui
                                    </label>
                                    <br>
                                    <label>
                                        <input type="radio" class="tx-artificaform-pi1-2782-condition-field1385635257-1" value="non" name="tx_artificaform_pi1[field2]">non
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <p class="text-right obligatoires">*Champs obligatoires</p>
                    <p class="text-center"><a href="formulaire-etape2.php" class="btn btn-primary"><span class="fa fa-arrow-right"></span> &Eacute;tape suivante</a>
                    </p>
                    <br>
                </form>
            </section>
        </div>
        <?php include( 'blocs/footer.php'); ?>
        <?php include( 'blocs/scripts.php'); ?>
    </body>

</html>



