<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Le site d'Orléans et son AgglO - Liste d'événements</title>
<? include('blocs/styles.php') ?>
</head>
<body>
<? include( 'blocs/header.php') ?>
<div class="container diapo">
  <section>
    <div class="row">
      <div class="col-md-8 col-lg-9">
        <ol class="breadcrumb hidden-xs">
          <li><a href="index.php">Accueil</a> </li>
          <li><a href="#">Vidéos</a> </li>
          <li class="active">Photos</li>
        </ol>
        <h1>Photos</h1>
        <div class="row liste-item">
          <div class="col-sm-6 col-lg-4">
            <article> <a href="diapo-single.php">
              <div class="visuel">
                <figure> <img class="img-responsive" src="http://www.orleans.fr/fileadmin/orleans/MEDIA/diaporama/sport/uso-sochaux/IMG_2443.jpg" alt="">
                  <figcaption class="sr-only">US Orléans 1-0 FC Sochaux-Montbéliard</figcaption>
                </figure>
              </div>
              <h3>US Orléans 1-0 FC Sochaux-Montbéliard</h3>
              </a>
              <p>Malgré la victoire au score l'USO termine à la 18ème place du championnat, relégable avec Châteauroux et Arles-Avignon.</p>
            </article>
          </div>
          <div class="col-sm-6 col-lg-4">
            <article> <a href="diapo-single.php">
              <div class="visuel">
                <figure> <img class="img-responsive" src="http://www.orleans.fr/fileadmin/orleans/MEDIA/diaporama/jean_zay_hommage/IMG_5030.JPG" alt="">
                  <figcaption class="sr-only">Hommage à Jean Zay</figcaption>
                </figure>
              </div>
              <h3>Hommage à Jean Zay</h3>
              </a>
              <p>Une cérémonie solennelle d’hommage est organisée le 18 mai. Une marche républicaine, avec des étapes sur des sites symboliques, a ainsi été...</p>
            </article>
          </div>
          <div class="col-sm-6 col-lg-4">
            <article> <a href="diapo-single.php">
              <div class="visuel">
                <figure> <img class="img-responsive" src="http://www.orleans.fr/fileadmin/orleans/MEDIA/diaporama/fetes_jeanne_arc/2015/05_08/00IMG_0582.jpg" alt="">
                  <figcaption class="sr-only">Fêtes de Jeanne d'Arc 2015 - 8 mai</figcaption>
                </figure>
              </div>
              <h3>Fêtes de Jeanne d'Arc 2015 - 8 mai</h3>
              </a>
              <p>Chevauchée historique évoquant l’épopée de Jeanne d’Arc à travers la ville et hommage de la jeunesse chrétienne à Jeanne d’Arc.</p>
            </article>
          </div>
          <div class="col-sm-6 col-lg-4">
            <article> <a href="diapo-single.php">
              <div class="visuel">
                <figure> <img class="img-responsive" src="http://www.orleans.fr/fileadmin/orleans/MEDIA/diaporama/fetes_jeanne_arc/2015/05_07/IMG_4337.jpg" alt="">
                  <figcaption class="sr-only">Fêtes de Jeanne d'Arc 2015 - 7 mai</figcaption>
                </figure>
              </div>
              <h3>Fêtes de Jeanne d'Arc 2015 - 7 mai</h3>
              </a>
              <p>Cérémonie de remise de l'Etendard - Son et Lumière place sainte croix suivi du Set Electro, parvis du théâtre.</p>
            </article>
          </div>
          <div class="col-sm-6 col-lg-4">
            <article> <a href="diapo-single.php">
              <div class="visuel">
                <figure> <img class="img-responsive" src="http://www.orleans.fr/fileadmin/orleans/MEDIA/diaporama/sport/handball_pantheres_coupe_europe/IMG_4136.JPG" alt="">
                  <figcaption class="sr-only">Finale de Coupe d'Europe de Handball</figcaption>
                </figure>
              </div>
              <h3>Finale de Coupe d'Europe de Handball</h3>
              </a>
              <p>Dimanche dernier, Les Panthères nous ont offert un spectacle grandiose, lors du match aller de la finale de Coupe d’Europe des Vainqueurs de Coupe....</p>
            </article>
          </div>
          <div class="col-sm-6 col-lg-4">
            <article> <a href="diapo-single.php">
              <div class="visuel">
                <figure> <img class="img-responsive" src="http://www.orleans.fr/fileadmin/orleans/MEDIA/diaporama/fetes_jeanne_arc/2015/05_01/001.jpg" alt="">
                  <figcaption class="sr-only">Fêtes de Jeanne d'Arc 2015 - 1er mai</figcaption>
                </figure>
              </div>
              <h3>Fêtes de Jeanne d'Arc 2015 - 1er mai</h3>
              </a>
              <p>Chevauchée historique évoquant l’épopée de Jeanne d’Arc à travers la ville et hommage de la jeunesse chrétienne à Jeanne d’Arc.</p>
            </article>
          </div>
          <div class="col-sm-6 col-lg-4">
            <article> <a href="diapo-single.php">
              <div class="visuel">
                <figure> <img class="img-responsive" src="http://www.orleans.fr/fileadmin/orleans/MEDIA/diaporama/fetes_jeanne_arc/2015/04_29/2015_04_29_174.jpg" alt="">
                  <figcaption class="sr-only">Fêtes de Jeanne d'Arc 2015 - 29 avril</figcaption>
                </figure>
              </div>
              <h3>Fêtes de Jeanne d'Arc 2015 - 29 avril</h3>
              </a>
              <p>Commémoration de l'entrée de Jeanne d’Arc à Orléans - Concert à la cathédrale sainte Croix.</p>
            </article>
          </div>
          <div class="col-sm-6 col-lg-4">
            <article> <a href="diapo-single.php">
              <div class="visuel">
                <figure> <img class="img-responsive" src="http://www.orleans.fr/fileadmin/orleans/MEDIA/diaporama/fetes_jeanne_arc/2015/05_07/IMG_4337.jpg" alt="">
                  <figcaption class="sr-only">Salon des Arts du Jardin</figcaption>
                </figure>
              </div>
              <h3>Salon des Arts du Jardin</h3>
              </a>
              <p>Les 11 & 12 avril 2015 au Parc Floral de La Source, Orléans - Loiret</p>
            </article>
          </div>
          <div class="col-sm-6 col-lg-4">
            <article> <a href="diapo-single.php">
              <div class="visuel">
                <figure> <img class="img-responsive" src="http://www.orleans.fr/fileadmin/orleans/MEDIA/diaporama/sport/golf-limere-grand-prix/001IMG_7553.JPG" alt="">
                  <figcaption class="sr-only">Golf : Grand Prix de Limère</figcaption>
                </figure>
              </div>
              <h3>Golf : Grand Prix de Limère</h3>
              </a>
              <p>Le Grand Prix de Limère s'est déroulé du 10 au 12 avril 2015</p>
            </article>
          </div>
          <div class="col-sm-6 col-lg-4">
            <article> <a href="diapo-single.php">
              <div class="visuel">
                <figure> <img class="img-responsive" src="http://www.orleans.fr/fileadmin/orleans/MEDIA/diaporama/sport/badminton-international2015/001IMG_5551.jpg" alt="">
                  <figcaption class="sr-only">Orléans International Challenge Badminton</figcaption>
                </figure>
              </div>
              <h3>Orléans International Challenge Badminton</h3>
              </a>
              <p>L'édition 2015 du tournoi a réuni 228 joueurs de 38 pays au palais des sports d'Orléans.</p>
            </article>
          </div>
          <div class="col-sm-6 col-lg-4">
            <article> <a href="diapo-single.php">
              <div class="visuel">
                <figure> <img class="img-responsive" src="http://www.orleans.fr/fileadmin/orleans/MEDIA/diaporama/carnaval-lasource/2015_03_19_020.jpg" alt="">
                  <figcaption class="sr-only">Un air de carnaval à la Source</figcaption>
                </figure>
              </div>
              <h3>Un air de carnaval à la Source</h3>
              </a>
              <p>Fête au complexe sportif de La Source et défilé avec les enfants déguisés des écoles du quartier.</p>
            </article>
          </div>
          <div class="col-sm-6 col-lg-4">
            <article> <a href="diapo-single.php">
              <div class="visuel">
                <figure> <img class="img-responsive" src="http://www.orleans.fr/fileadmin/orleans/MEDIA/diaporama/carmen-zenith/001IMG_4666.jpg" alt="">
                  <figcaption class="sr-only">Carmen</figcaption>
                </figure>
              </div>
              <h3>Carmen</h3>
              </a>
              <p>Répétition générale de l'Opéra en 4 actes de Georges Bizet qui est joué les 20, 21 et 22 mars 2015 au Zénith d’Orléans.</p>
            </article>
          </div>
        </div>
        <nav class="text-center">
          <h2 class="sr-only">Pagination</h2>
          <? include( "blocs/pagination.php") ?>
        </nav>
      </div>
      <div class="col-md-4 col-lg-3">
        <? include( "blocs/diapo-filter.php"); ?>
      </div>
    </div>
  </section>
</div>
</div>
<? include( 'blocs/footer.php'); ?>
<? include( 'blocs/scripts.php'); ?>
</body>
</html>
