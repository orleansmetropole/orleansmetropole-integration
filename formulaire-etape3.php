<!DOCTYPE html>
<html>

<head>
<meta charset="utf-8">
<title>Le site d'Orléans et son AgglO - Accueil</title>
<? include('blocs/styles.php') ?>

    <body>
        <? include( 'blocs/header.php') ?>
        <div class="container formulaire">
            <ol class="breadcrumb hidden-xs">
                <li><a href="index.php">Accueil</a>
                </li>
                <li><a href="#">Formulaire de contacte</a>
                </li>
                <li><a href="#">Intervention </a>
                </li>
                <li class="active">Demande d'arrêté de circulation</li>
            </ol>
            <section>
                <header>
                    <h1>Demande d'arrêté de circulation</h1>
                    <ul class="list-unstyled etape">
                        <li class="inactive"><a href="formulaire-etape1.php"><span class="step">1</span> Demandeur</a>
                        </li>
                        <br class="visible-sm visible-xs">
                        <li class="inactive "><a href="formulaire-etape2.php"><span class="step">2</span> Informations sur les travaux</a>
                        </li>
                        <br class="visible-sm visible-xs">
                        <li class="active "><span class="step">3</span> Mesures de circulation sollicitées</li>
                        <br class="visible-sm visible-xs">
                        <li class="inactive "><a href="formulaire-etape4.php"><span class="step">4</span> Validation</a>
                        </li>
                    </ul>
                </header>
                <h2 class="sr-only">Formulaire de demande - Troisième étape</h2>	
                <form class="form-horizontal" method="post">
                    <h3 class="sous-titre">Mesures de circulation sollicitées</h3>	
                    <div class="require">
                        <div class="form-group">
                            <label class="control-label col-md-4">*Voie(s)</label>
                            <div class="col-md-4">
                                <textarea class="form-control" rows="3" placeholder="Voie(s)"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4">Intervention sur une rue où passe le tramway</label>
                        <div class="col-md-4">
                            <div class="radio">
                                <label>
                                    <input type="radio" value="oui" name="tx_artificaform_pi1[field2]">oui
                                </label>
                                <br>
                                <label>
                                    <input type="radio" value="non" name="tx_artificaform_pi1[field2]">non
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="require">
                        <div class="form-group">
                            <label class="control-label col-md-4">*Stationnement à interdire</label>
                            <div class="col-md-4">
                                <div class="radio">
                                    <label>
                                        <input type="radio" value="" name="tx_artificaform_pi1[field2]">aucun
                                    </label>
                                    <br>
                                    <label>
                                        <input type="radio" value="" name="tx_artificaform_pi1[field2]">côté n° pairs
                                    </label>
                                    <br>
                                    <label>
                                        <input type="radio" value="" name="tx_artificaform_pi1[field2]">côté n° impairs
                                    </label>
                                    <br>
                                    <label>
                                        <input type="radio" value="" name="tx_artificaform_pi1[field2]">des deux côtés
                                    </label>
                                    <br>
                                    <label>
                                        <input type="radio" value="" name="tx_artificaform_pi1[field2]">du n° au n°
                                    </label>
                                    <br>
                                    <label>
                                        <input type="radio" value="" name="tx_artificaform_pi1[field2]">sur quelques mètres
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="require">
                        <div class="form-group">
                            <label class="control-label col-md-4">*Rue barrée</label>
                            <div class="col-md-4">
                                <div class="radio">
                                    <label>
                                        <input type="radio" value="oui" name="tx_artificaform_pi1[field2]">oui
                                    </label>
                                    <br>
                                    <label>
                                        <input type="radio" value="non" name="tx_artificaform_pi1[field2]">non
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="require">
                        <div class="form-group">
                            <label class="control-label col-md-4">*Suppression d'un sens de circulation</label>
                            <div class="col-md-4">
                                <div class="radio">
                                    <label>
                                        <input type="radio" value="oui" name="tx_artificaform_pi1[field2]">oui
                                    </label>
                                    <br>
                                    <label>
                                        <input type="radio" value="non" name="tx_artificaform_pi1[field2]">non
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="require">
                        <div class="form-group">
                            <label class="control-label col-md-4">*Chaussée rétrécie</label>
                            <div class="col-md-4">
                                <div class="radio">
                                    <label>
                                        <input type="radio" value="oui" name="tx_artificaform_pi1[field2]">oui
                                    </label>
                                    <br>
                                    <label>
                                        <input type="radio" value="non" name="tx_artificaform_pi1[field2]">non
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="require">
                        <div class="form-group">
                            <label class="control-label col-md-4">*Neutralisation d'un couloir de circulation</label>
                            <div class="col-md-4">
                                <div class="radio">
                                    <label>
                                        <input type="radio" value="oui" name="tx_artificaform_pi1[field2]">oui
                                    </label>
                                    <br>
                                    <label>
                                        <input type="radio" value="non" name="tx_artificaform_pi1[field2]">non
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4">Limitation de vitesse</label>
                        <div class="col-md-4">
                            <div class="radio">
                                <label>
                                    <input type="radio" value="oui" name="tx_artificaform_pi1[field2]">oui
                                </label>
                                <br>
                                <label>
                                    <input type="radio" value="non" name="tx_artificaform_pi1[field2]">non
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="require">
                        <div class="form-group">
                            <label class="control-label col-md-4">*Circulation alternée</label>
                            <div class="col-md-4">
                                <div class="radio">
                                    <label>
                                        <input type="radio" value="oui" name="tx_artificaform_pi1[field2]">oui
                                    </label>
                                    <br>
                                    <label>
                                        <input type="radio" value="non" name="tx_artificaform_pi1[field2]">non
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="require">
                        <div class="form-group">
                            <label class="control-label col-md-4">*Circulation des vélos</label>
                            <div class="col-md-4">
                                <div class="radio">
                                    <label>
                                        <input type="radio" value="" name="tx_artificaform_pi1[field2]">maintenue
                                    </label>
                                    <br>
                                    <label>
                                        <input type="radio" value="" name="tx_artificaform_pi1[field2]">supprimée
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="require">
                        <div class="form-group">
                            <label class="control-label col-md-4">*Circulation des piétons</label>
                            <div class="col-md-4">
                                <div class="radio">
                                    <label>
                                        <input type="radio" value="oui" name="tx_artificaform_pi1[field2]">oui
                                    </label>
                                    <br>
                                    <label>
                                        <input type="radio" value="non" name="tx_artificaform_pi1[field2]">non
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4">Autres mesures sollicitées</label>
                        <div class="col-md-4">
                            <textarea class="form-control" rows="3" placeholder="Autres mesures sollicitées"></textarea>
                        </div>
                    </div>
                    <div class="require">
                        <div class="form-group">
                            <label class="control-label col-md-4">*Informations riverains</label>
                            <div class="col-md-4">
                                <div class="radio">
                                    <label>
                                        <input type="radio" value="" name="tx_artificaform_pi1[field2]">pas nécessaire
                                    </label>
                                    <br>
                                    <label>
                                        <input type="radio" value="" name="tx_artificaform_pi1[field2]">prévue
                                    </label>
                                    <br>
                                    <label>
                                        <input type="radio" value="" name="tx_artificaform_pi1[field2]">médias utilisés
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <p class="text-right obligatoires">*Champs obligatoires</p>
                    <p class="text-center"><a href="formulaire-etape4.php" class="btn btn-primary"><span class="fa fa-arrow-right"></span> &Eacute;tape suivante</a>
                    </p>
                    <br>
                </form>
            </section>
        </div>
        <? include( 'blocs/footer.php'); ?>
        <? include( 'blocs/scripts.php'); ?>
    </body>

</html>



