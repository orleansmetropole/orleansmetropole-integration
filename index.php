<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Le site d'Orléans et son AgglO - Accueil</title>
<?php include( 'blocs/styles.php') ?>
<body>
<h1 class="sr-only"><strong>Orléans</strong> & son <strong>AgglO</strong></h1>
<?php include( 'blocs/header.php') ?>
<div class="container home">
  <div class="row">
    <div class="col-lg-9">
      <section>
        <div id="carousel-home" class="carousel carousel-home slide carousel-fade" data-ride="carousel">
          <h2 class="sr-only">A la une</h2>
          <!-- Indicators -->
          <ol class="carousel-indicators">
            <li data-target="#carousel-home" data-slide-to="0" class="active"></li>
            <li data-target="#carousel-home" data-slide-to="1"></li>
            <li data-target="#carousel-home" data-slide-to="2"></li>
          </ol>
          <!-- Wrapper for slides -->
          <div class="carousel-inner">

            <section class="item active">
              <div class="bg"></div>
              <a href="#">
                <figure> <img src="img/orleanoide.jpg" class="img-responsive" alt="Orleanoide">
                <figcaption class="sr-only">Festival Orléanoïde³</figcaption>
                </figure>
              </a>
              <div class="carousel-caption">
                  <span class="badge">Culture</span>
                <h3> <a href="#">Festival Orléanoïde³</a></h3>
                <p>Du 30 janvier au 15 février 2015, le festival de la création numérique dynamite les codes établis.</p>
              </div>
            </section>

            <section class="item">
              <div class="bg"></div>
              <a href="#">
              <figure> <img src="img/ecole.jpg" class="img-responsive" alt="Ecole">
                <figcaption class="sr-only">Année scolaire 2015-2016 : inscriptions</figcaption>
              </figure>
              </a>
              <div class="carousel-caption"> <span class="badge">Éducation</span>
                <h3><a href="#">Année scolaire 2015-2016 : inscriptions</a></h3>
                <p>Inscriptions du lundi 12 janvier au vendredi 6 mars 2015</p>
              </div>
            </section>
            <section class="item">
              <div class="bg"></div>
              <a href="#">
              <figure> <img src="img/festival-de-loire.jpg" class="img-responsive" alt="Festival de Loire">
                <figcaption class="sr-only">Festival de Loire 2015 : cap à l’Est !</figcaption>
              </figure>
              </a>
              <div class="carousel-caption"> <span class="badge">Festival de Loire</span>
                <h3><a href="#">Festival de Loire 2015 : cap à l’Est !</a></h3>
                <p>Le Festival de Loire vous fera découvrir les 23 au 27 septembre la Pologne.</p>
              </div>
            </section>
          </div>
          <!-- Controls -->
          <a class="left carousel-control" href="#carousel-home" role="button" data-slide="prev"> <span class="fa fa-chevron-left"></span></a> <a class="right carousel-control" href="#carousel-home" role="button" data-slide="next"> <span class="fa fa-chevron-right"></span></a> </div>
      </section>
    </div>
    <div class="col-lg-3">
      <nav class="quicklink text-center">
        <h2 class="sr-only">Accès rapides</h2>
        <div class="well">
            <ul class="list-unstyled row">
                <li class="col-xs-12 col-sm-8 col-md-4 col-lg-12"> <a id="drop4" href="demarche.php?h=2" class="btn btn-default "><span class="fa fa-book rounded"></span><br>
                        Démarches en ligne</a> </li>
                <li class="col-xs-6 col-sm-4 col-md-2 col-lg-6"> <a href="agenda-list.php" class="btn btn-default "><span class="fa fa-calendar rounded"></span><br>
                        Notre <br class="hidden-sm">
                        agenda</a> </li>
                <li class="col-xs-6 col-sm-4 col-md-2 col-lg-6"> <a href="#" class="btn btn-default "><span class="fa fa-map-marker rounded"></span><br>
                        Plan <br class="hidden-sm">
                        de la ville</a> </li>
                <li class="col-xs-6 col-sm-4 col-md-2 col-lg-6"> <a href="" class="btn btn-default "><span class="fa fa-trash rounded"></span><br>
                        Les<br class="hidden-sm">
                        poubelles</a> </li>
                <li class="col-xs-6 col-sm-4 col-md-2 col-lg-6 visible-lg"> <a href="asso-list.php" class="btn btn-default"><span class="fa fa-users rounded"></span><br>
                        Les<br class="hidden-sm">
                        assos</a> </li>
                <li class="col-xs-6 col-sm-4 col-md-2 col-lg-12">
                    <div class="dropup">
                        <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownAccesDirect" data-toggle="dropdown" aria-expanded="true"> <span class="caret hidden"></span> <br class="hidden">
                            <span class="fa fa-plus inverse rounded"></span> <br>
                            Tous les services </button>
                        <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownAccesDirect">
                            <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Espace famille</a> </li>
                            <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Emploi</a> </li>
                            <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Publications</a> </li>
                        </ul>
                    </div>
                </li>
            </ul>
        </div>
      </nav>
    </div>
  </div>
  <div>
    <div class="bs-example bs-example-tabs">
      <ul class="nav nav-tabs" id="myTab">
        <li class="active"><a data-toggle="tab" href="#actus">Actualités</a> </li>
        <li><a data-toggle="tab" href="#agenda">Agenda</a> </li>
        <li><a data-toggle="tab" href="#kiosque">Le Kiosque</a> </li>
      </ul>
      <div class="tab-content" id="myTabContent">
        <section id="actus" class="tab-pane fade in active">
          <h2 class="sr-only">Actualités</h2>
          <div class="row liste-item">
            <div class="col-sm-6 col-md-4">
              <article> <a href="actu-single.php">
                <div class="visuel">
                  <figure> <img class="img-responsive" src="http://www.orleans.fr/typo3temp/pics/a7a5423722.jpg" alt=""><span class="badge">Urbanisme </span>
                    <figcaption class="sr-only">Argonne : le quartier en pleine transformation</figcaption>
                  </figure>
                </div>
                <h3>Argonne : le quartier en pleine transformation</h3>
                </a>
                <time datetime="2015-01-01" class="date">Publiée le Mardi 3 Février 2015</time>
                <p>Lancé en 2004 à l’Argonne, le programme de rénovation est aujourd’hui réalisé à 60%.</p>
              </article>
            </div>
            <div class="col-sm-6 col-md-4">
              <article> <a href="actu-single.php">
                <div class="visuel">
                  <figure> <img class="img-responsive" src="http://www.orleans.fr/typo3temp/pics/1a7a3a406f.jpg" alt=""><span class="badge">Festival de Loire </span>
                    <figcaption class="sr-only">Inscriptions pour les professionnels</figcaption>
                  </figure>
                </div>
                <h3>Inscriptions pour les professionnels</h3>
                </a>
                <time datetime="2015-01-01" class="date">Publiée le Mardi 3 Février 2015</time>
                <p>Restaurateurs, artisans, commerçants … sont invités à envoyer leur candidature pour la prochaine édition du festival</p>
              </article>
            </div>
            <div class="col-sm-6 col-md-4">
              <article> <a href="actu-single.php">
                <div class="visuel">
                  <figure> <img class="img-responsive" src="http://www.orleans.fr/typo3temp/pics/a593400cd0.jpg" alt=""><span class="badge">Sport</span>
                    <figcaption class="sr-only">6 800 spectateurs dans les tribunes</figcaption>
                  </figure>
                </div>
                <h3>6 800 spectateurs dans les tribunes</h3>
                </a>
                <time datetime="2015-01-01" class="date">Publiée le Lundi 2 Février 2015</time>
                <p>La compagnie du masque d'Or investit le Domaine de la Paillerie</p>
              </article>
            </div>
            <div class="col-sm-6 col-md-4">
              <article> <a href="actu-single.php">
                <div class="visuel">
                  <figure> <img class="img-responsive" src="http://www.orleans.fr/typo3temp/pics/6f9f5cf379.jpg" alt=""><span class="badge">Éducation </span>
                    <figcaption class="sr-only">Émis : vacances d'hiver sportives</figcaption>
                  </figure>
                </div>
                <h3>Émis : vacances d'hiver sportives</h3>
                </a>
                <time datetime="2015-01-01" class="date">Publiée le Vendredi 30 Janvier 2015</time>
                <p>L’École municipale d’initiation sportive organise des stages à destination des 4-11 ans</p>
              </article>
            </div>
            <div class="col-sm-6 col-md-4">
              <article> <a href="actu-single.php">
                <div class="visuel">
                  <figure> <img class="img-responsive" src="http://www.orleans.fr/uploads/tx_artificaevents/carrefour_parents_bandeau.jpg" alt=""><span class="badge">Solidarité - santé</span>
                    <figcaption class="sr-only">Carrefour des parents : rendez-vous !</figcaption>
                  </figure>
                </div>
                <h3>Carrefour des parents : rendez-vous !</h3>
                </a>
                <time datetime="2015-01-01" class="date">Publiée le Jeudi 29 Janvier 2015</time>
                <p>Il est ouvert à tous et organise chaque mois des échanges autour de la relation parents-enfants</p>
              </article>
            </div>
            <div class="col-sm-6 col-md-4">
              <article> <a href="actu-single.php">
                <div class="visuel">
                  <figure> <img class="img-responsive" src="http://www.orleans.fr/typo3temp/pics/388d6dd0f6.jpg" alt=""><span class="badge">Mairie - citoyenneté</span>
                    <figcaption class="sr-only">Recensement de la population</figcaption>
                  </figure>
                </div>
                <h3>Recensement de la population</h3>
                </a>
                <time datetime="2015-01-01" class="date"></time>
                <p>Le recensement de la population aura lieu du jeudi 15 janvier au samedi 21 février.</p>
              </article>
            </div>
            <div class="col-sm-6 col-md-4">
              <article> <a href="actu-single.php">
                <div class="visuel">
                  <figure> <img class="img-responsive" src="img/braderie.jpg" alt=""><span class="badge">Fête - foire - salon </span>
                    <figcaption class="sr-only">Braderie d’hiver</figcaption>
                  </figure>
                </div>
                <h3>Braderie d’hiver</h3>
                </a>
                <time datetime="2015-01-01" class="date">Du 13 Février au 14 Février</time>
                <p>Vous êtes un aficionado de la Grande Braderie qui, chaque année, à la fin août, vous donne envie de démarrer la rentrée du bon pied !</p>
              </article>
            </div>
            <div class="col-sm-6 col-md-4">
              <article> <a href="actu-single.php">
                <div class="visuel">
                  <figure> <img class="img-responsive" src="img/drone.jpg" alt=""><span class="badge">Orléanoide</span>
                    <figcaption class="sr-only">« Lâcher de drones » par Hypnos 451</figcaption>
                  </figure>
                </div>
                <h3>« Lâcher de drones » par Hypnos 451</h3>
                </a>
                <time datetime="2015-01-01" class="date">Du 13 Février au 14 Février</time>
                <p>La compagnie du masque d'Or investit le Domaine de la Paillerie</p>
              </article>
            </div>
            <div class="col-sm-6 col-md-4  hidden-sm">
              <article> <a href="actu-single.php">
                <div class="visuel">
                  <figure> <img class="img-responsive" src="http://www.orleans.fr/typo3temp/pics/a7a5423722.jpg" alt=""><span class="badge">Economie</span>
                    <figcaption class="sr-only">Le Lab’O, incubateur numérique</figcaption>
                  </figure>
                </div>
                <h3>Le Lab’O, incubateur numérique</h3>
                </a>
                <time datetime="2015-01-01" class="date">Publiée le Mercredi 28 Janvier 2015</time>
                <p>La friche industrielle Famar est en cours de réhabilitation pour accueillir demain les entreprises innovantes du numérique.</p>
              </article>
            </div>
          </div>
          <p class="text-center"><a href="actu-list.php" class="btn btn-warning btn-lg text-uppercase"><span class="fa fa-plus"></span> Toutes les actualités</a> </p>
        </section>
        <section id="agenda" class="tab-pane fade in">
          <h2 class="sr-only">Agenda</h2>
          <div class="row  liste-item">
            <div class="col-sm-6 col-md-4">
              <article> <a href="agenda-single.php">
                <div class="visuel">
                  <figure> <img class="img-responsive" src="http://www.orleans.fr/typo3temp/pics/6f9f5cf379.jpg" alt=""><span class="badge">Éducation </span>
                    <figcaption class="sr-only">Émis : vacances d'hiver sportives</figcaption>
                  </figure>
                </div>
                <h3>Émis : vacances d'hiver sportives</h3>
                </a>
                <time datetime="2015-01-01" class="date">Publiée le Vendredi 30 Janvier 2015</time>
                <p>L’École municipale d’initiation sportive organise des stages à destination des 4-11 ans</p>
              </article>
            </div>
            <div class="col-sm-6 col-md-4">
              <article> <a href="agenda-single.php">
                <div class="visuel">
                  <figure> <img class="img-responsive" src="http://www.orleans.fr/uploads/tx_artificaevents/carrefour_parents_bandeau.jpg" alt=""><span class="badge">Solidarité - santé</span>
                    <figcaption class="sr-only">Carrefour des parents : rendez-vous !</figcaption>
                  </figure>
                </div>
                <h3>Carrefour des parents : rendez-vous !</h3>
                </a>
                <time datetime="2015-01-01" class="date">Publiée le Jeudi 29 Janvier 2015</time>
                <p>Il est ouvert à tous et organise chaque mois des échanges autour de la relation parents-enfants</p>
              </article>
            </div>
            <div class="col-sm-6 col-md-4">
              <article> <a href="agenda-single.php">
                <div class="visuel">
                  <figure> <img class="img-responsive" src="http://www.orleans.fr/typo3temp/pics/388d6dd0f6.jpg" alt=""><span class="badge">Mairie - citoyenneté</span>
                    <figcaption class="sr-only">Recensement de la population</figcaption>
                  </figure>
                </div>
                <h3>Recensement de la population</h3>
                </a>
                <p>Le recensement de la population aura lieu du jeudi 15 janvier au samedi 21 février.</p>
              </article>
            </div>
            <div class="col-sm-6 col-md-4">
              <article> <a href="agenda-single.php">
                <div class="visuel">
                  <figure> <img class="img-responsive" src="http://www.orleans.fr/typo3temp/pics/a7a5423722.jpg" alt=""><span class="badge">Urbanisme </span>
                    <figcaption class="sr-only">Argonne : le quartier en pleine transformation</figcaption>
                  </figure>
                </div>
                <h3>Argonne : le quartier en pleine transformation</h3>
                </a>
                <time datetime="2015-01-01" class="date">Publiée le Mardi 3 Février 2015</time>
                <p>Lancé en 2004 à l’Argonne, le programme de rénovation est aujourd’hui réalisé à 60%.</p>
              </article>
            </div>
            <div class="col-sm-6 col-md-4">
              <article> <a href="agenda-single.php">
                <div class="visuel">
                  <figure> <img class="img-responsive" src="http://www.orleans.fr/typo3temp/pics/1a7a3a406f.jpg" alt=""><span class="badge">Festival de Loire </span>
                    <figcaption class="sr-only">Inscriptions pour les professionnels</figcaption>
                  </figure>
                </div>
                <h3>Inscriptions pour les professionnels</h3>
                </a>
                <time datetime="2015-01-01" class="date">Publiée le Mardi 3 Février 2015</time>
                <p>Restaurateurs, artisans, commerçants … sont invités à envoyer leur candidature pour la prochaine édition du festival</p>
              </article>
            </div>
            <div class="col-sm-6 col-md-4">
              <article> <a href="agenda-single.php">
                <div class="visuel">
                  <figure> <img class="img-responsive" src="http://www.orleans.fr/typo3temp/pics/a593400cd0.jpg" alt=""><span class="badge">Sport</span>
                    <figcaption class="sr-only">6 800 spectateurs dans les tribunes</figcaption>
                  </figure>
                </div>
                <h3>6 800 spectateurs dans les tribunes</h3>
                </a>
                <time datetime="2015-01-01" class="date">Publiée le Lundi 2 Février 2015</time>
                <p>La compagnie du masque d'Or investit le Domaine de la Paillerie</p>
              </article>
            </div>
            <div class="col-sm-6 col-md-4">
              <article> <a href="agenda-single.php">
                <div class="visuel">
                  <figure> <img class="img-responsive" src="img/braderie.jpg" alt=""><span class="badge">Fête - foire - salon </span>
                    <figcaption class="sr-only">Braderie d’hiver</figcaption>
                  </figure>
                </div>
                <h3>Braderie d’hiver</h3>
                </a>
                <time datetime="2015-01-01" class="date">Du 13 Février au 14 Février</time>
                <p>Vous êtes un aficionado de la Grande Braderie qui, chaque année, à la fin août, vous donne envie de démarrer la rentrée du bon pied !</p>
              </article>
            </div>
            <div class="col-sm-6 col-md-4">
              <article> <a href="agenda-single.php">
                <div class="visuel">
                  <figure> <img class="img-responsive" src="img/drone.jpg" alt=""><span class="badge">Orléanoide</span>
                    <figcaption class="sr-only">« Lâcher de drones » par Hypnos 45</figcaption>
                  </figure>
                </div>
                <h3>« Lâcher de drones » par Hypnos 451</h3>
                </a>
                <time datetime="2015-01-01" class="date">Du 13 Février au 14 Février</time>
                <p>La compagnie du masque d'Or investit le Domaine de la Paillerie</p>
              </article>
            </div>
            <div class="col-sm-6 col-md-4  hidden-sm">
              <article> <a href="agenda-single.php">
                <div class="visuel">
                  <figure> <img class="img-responsive" src="http://www.orleans.fr/typo3temp/pics/a7a5423722.jpg" alt=""><span class="badge">Economie</span>
                    <figcaption class="sr-only">Le Lab’O, incubateur numérique</figcaption>
                  </figure>
                </div>
                <h3>Le Lab’O, incubateur numérique</h3>
                </a>
                <time datetime="2015-01-01" class="date">Publiée le Mercredi 28 Janvier 2015</time>
                <p>La friche industrielle Famar est en cours de réhabilitation pour accueillir demain les entreprises innovantes du numérique.</p>
              </article>
            </div>
          </div>
          <p class="text-center"><a href="agenda-list.php" class="btn btn-warning btn-lg text-uppercase"><span class="fa fa-plus"></span> Toutes les événements</a> </p>
        </section>
        <section id="kiosque" class="tab-pane fade in">
          <h2 class="sr-only">Le Kiosque</h2>
          <div class="row liste-item">
            <div class="col-sm-6 col-md-4">
              <article> <a href="#">
                <div class="visuel">
                  <figure> <img class="img-responsive" src="img/braderie.jpg" alt=""><span class="badge">Fête - foire - salon </span>
                    <figcaption class="sr-only">Braderie d’hiver</figcaption>
                  </figure>
                </div>
                <h3>Braderie d’hiver</h3>
                </a>
                <time datetime="2015-01-01" class="date">Du 13 Février au 14 Février</time>
                <p>Vous êtes un aficionado de la Grande Braderie qui, chaque année, à la fin août, vous donne envie de démarrer la rentrée du bon pied !</p>
              </article>
            </div>
            <div class="col-sm-6 col-md-4">
              <article> <a href="#">
                <div class="visuel">
                  <figure> <img class="img-responsive" src="img/drone.jpg" alt=""><span class="badge">Orléanoide</span>
                    <figcaption class="sr-only">« Lâcher de drones » par Hypnos 451</figcaption>
                  </figure>
                </div>
                <h3>« Lâcher de drones » par Hypnos 451</h3>
                </a>
                <time datetime="2015-01-01" class="date">Du 13 Février au 14 Février</time>
                <p>La compagnie du masque d'Or investit le Domaine de la Paillerie</p>
              </article>
            </div>
            <div class="col-sm-6 col-md-4">
              <article> <a href="#">
                <div class="visuel">
                  <figure> <img class="img-responsive" src="http://www.orleans.fr/typo3temp/pics/a7a5423722.jpg" alt=""><span class="badge">Economie</span>
                    <figcaption class="sr-only">Le Lab’O, incubateur numérique</figcaption>
                  </figure>
                </div>
                <h3>Le Lab’O, incubateur numérique</h3>
                </a>
                <time datetime="2015-01-01" class="date">Publiée le Mercredi 28 Janvier 2015</time>
                <p>La friche industrielle Famar est en cours de réhabilitation pour accueillir demain les entreprises innovantes du numérique.</p>
              </article>
            </div>
            <div class="col-sm-6 col-md-4">
              <article> <a href="#">
                <div class="visuel">
                  <figure> <img class="img-responsive" src="http://www.orleans.fr/typo3temp/pics/a7a5423722.jpg" alt=""><span class="badge">Urbanisme </span>
                    <figcaption class="sr-only">Argonne : le quartier en pleine transformation</figcaption>
                  </figure>
                </div>
                <h3>Argonne : le quartier en pleine transformation</h3>
                </a>
                <time datetime="2015-01-01" class="date">Publiée le Mardi 3 Février 2015</time>
                <p>Lancé en 2004 à l’Argonne, le programme de rénovation est aujourd’hui réalisé à 60%.</p>
              </article>
            </div>
            <div class="col-sm-6 col-md-4">
              <article> <a href="#">
                <div class="visuel">
                  <figure> <img class="img-responsive" src="http://www.orleans.fr/typo3temp/pics/1a7a3a406f.jpg" alt=""><span class="badge">Festival de Loire </span>
                    <figcaption class="sr-only">Inscriptions pour les professionnels</figcaption>
                  </figure>
                </div>
                <h3>Inscriptions pour les professionnels</h3>
                </a>
                <time datetime="2015-01-01" class="date">Publiée le Mardi 3 Février 2015</time>
                <p>Restaurateurs, artisans, commerçants … sont invités à envoyer leur candidature pour la prochaine édition du festival</p>
              </article>
            </div>
            <div class="col-sm-6 col-md-4">
              <article> <a href="#">
                <div class="visuel">
                  <figure> <img class="img-responsive" src="http://www.orleans.fr/typo3temp/pics/a593400cd0.jpg" alt=""><span class="badge">Sport</span>
                    <figcaption class="sr-only">6 800 spectateurs dans les tribunes</figcaption>
                  </figure>
                </div>
                <h3>6 800 spectateurs dans les tribunes</h3>
                </a>
                <time datetime="2015-01-01" class="date">Publiée le Lundi 2 Février 2015</time>
                <p>La compagnie du masque d'Or investit le Domaine de la Paillerie</p>
              </article>
            </div>
            <div class="col-sm-6 col-md-4">
              <article> <a href="#">
                <div class="visuel">
                  <figure> <img class="img-responsive" src="http://www.orleans.fr/typo3temp/pics/6f9f5cf379.jpg" alt=""><span class="badge">Éducation </span>
                    <figcaption class="sr-only">Émis : vacances d'hiver sportives</figcaption>
                  </figure>
                </div>
                <h3>Émis : vacances d'hiver sportives</h3>
                </a>
                <time datetime="2015-01-01" class="date">Publiée le Vendredi 30 Janvier 2015</time>
                <p>L’École municipale d’initiation sportive organise des stages à destination des 4-11 ans</p>
              </article>
            </div>
            <div class="col-sm-6 col-md-4">
              <article> <a href="#">
                <div class="visuel">
                  <figure> <img class="img-responsive" src="http://www.orleans.fr/uploads/tx_artificaevents/carrefour_parents_bandeau.jpg" alt=""><span class="badge">Solidarité - santé</span>
                    <figcaption class="sr-only">Carrefour des parents : rendez-vous !</figcaption>
                  </figure>
                </div>
                <h3>Carrefour des parents : rendez-vous !</h3>
                </a>
                <time datetime="2015-01-01" class="date">Publiée le Jeudi 29 Janvier 2015</time>
                <p>Il est ouvert à tous et organise chaque mois des échanges autour de la relation parents-enfants</p>
              </article>
            </div>
            <div class="col-sm-6 col-md-4 hidden-sm">
              <article> <a href="#">
                <div class="visuel">
                  <figure> <img class="img-responsive" src="http://www.orleans.fr/typo3temp/pics/388d6dd0f6.jpg" alt=""><span class="badge">Mairie - citoyenneté</span>
                    <figcaption class="sr-only">Recensement de la population</figcaption>
                  </figure>
                </div>
                <h3>Recensement de la population</h3>
                </a>
                <p>Le recensement de la population aura lieu du jeudi 15 janvier au samedi 21 février.</p>
              </article>
            </div>
          </div>
          <p class="text-center"><a href="actu-list.php" class="btn btn-warning btn-lg text-uppercase"><span class="fa fa-plus"></span> Toutes les actualités</a> </p>
        </section>
      </div>
    </div>
  </div>
</div>
<div class="zoom-sur">
  <div class="container">
    <section>
      <h2>ZOOM SUR...</h2>
      <div id="carousel-zoom" class="carousel slide carousel-zoom" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators">
          <li data-target="#carousel-zoom" data-slide-to="0" class="active"></li>
          <li data-target="#carousel-zoom" data-slide-to="1"></li>
          <li data-target="#carousel-zoom" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner" role="listbox">
          <section class="item active">
            <figure> <img class="img-responsive" src="img/salon_train.jpg" alt="salon du train">
              <figcaption class="sr-only">14ème Salon du Train Miniature</figcaption>
            </figure>
            <div class="carousel-caption">
              <h3>14ème Salon du Train Miniature</h3>
              <p>Les 8 et 9 Novembre prochains au parc des exposition d'Orléans.</p>
              <p class="text-right-lg hidden-sm hidden-xs"><a href="#" class="btn btn-warning btn-alt text-uppercase"><span class="fa fa-plus"></span> Lire la suite</a> </p>
            </div>
          </section>
          <section class="item">
            <figure> <img class="img-responsive" src="img/orleans.jpg" alt="orleans">
              <figcaption class="sr-only">Jazz à l'évêché</figcaption>
            </figure>
            <div class="carousel-caption">
              <h3>Jazz à l'évêché</h3>
              <p>4 journées de concerts gratuits ! Découvrez le programme du 18 au 21 juin 2015.</p>
              <p class="text-right-lg hidden-sm hidden-xs"><a href="#" class="btn btn-warning btn-alt text-uppercase"><span class="fa fa-plus"></span> Lire la suite</a> </p>
            </div>
          </section>
          <section class="item">
            <figure> <img class="img-responsive" src="img/badminton.jpg" alt="badminton">
              <figcaption class="sr-only"> Journées environnement santé </figcaption>
            </figure>
            <div class="carousel-caption">
              <h3>Journées environnement santé</h3>
              <p>Deux jours pour s’interroger sur la qualité de l’air que nous respirons : 30 et 31 mai, place de la Loire.</p>
              <p class="text-right-lg hidden-sm hidden-xs"><a href="#" class="btn btn-warning btn-alt text-uppercase"><span class="fa fa-plus"></span> Lire la suite</a> </p>
            </div>
          </section>
        </div>
        <!-- Controls -->
        <a class="left carousel-control" href="#carousel-zoom" role="button" data-slide="prev"> <span class="fa fa-chevron-left"></span></a> <a class="right carousel-control" href="#carousel-zoom" role="button" data-slide="next"> <span class="fa fa-chevron-right"></span></a> </div>
    </section>
  </div>
</div>
<div class="text-center footer-social">
  <h2>Suivez <strong>#OrleansMetropole</strong> sur :</h2>
  <ul class="list-inline text-uppercase follow">
    <li class="twitter"><a class="btn btn-primary rounded-lg" target="_blank" title="Suivez-nous sur Twitter" href="https://twitter.com/OrleansAgglO"><span class="fa fa-twitter fa-3x"></span></a> </li>
    <li class="facebook"><a class="btn btn-primary rounded-lg" target="_blank" title="Suivez-nous sur Facebook" href="https://www.facebook.com/OrleansetsonAgglO"><span class="fa fa-facebook fa-3x"></span></a> </li>
    <li class="youtube"><a class="btn btn-primary rounded-lg" target="_blank" title="Suivez-nous sur Youtube" href="https://www.youtube.com/user/OrleansetsonAgglO"><span class="fa fa-youtube fa-3x"></span> </a> </li>
    <li class="googleplus"><a class="btn btn-primary rounded-lg" target="_blank" title="Suivez-nous sur Google+" href="https://plus.google.com/u/0/103392140269241914617/posts"><span class="fa fa-google-plus fa-3x"></span></a> </li>
  </ul>
</div>
<?php include( 'blocs/footer.php'); ?>
<?php include( 'blocs/scripts.php'); ?>
</body>
</html>
