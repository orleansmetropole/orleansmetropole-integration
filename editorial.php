<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <title>Le site d'Orléans et son AgglO - Liste d'événements</title>
  <?php include( 'blocs/styles.php') ?>
</head>

<body>
    <? include( 'blocs/header.php') ?>
    <div class="container edit">
        <div class="row">
                <article>
            <div class="col-md-8 col-lg-9">
                <ol class="breadcrumb hidden-xs">
                    <li><a href="index.php">Accueil</a>
                    </li>
                    <li><a href="#">Rubrique</a>
                    </li>
                    <li class="active">Festival Hip Hop 2014</li>
                </ol>
                    <header class="accroche header-article">
                        <div class="row">
                            <div class="col-sm-8">
                                <h1>Festival Hip Hop 2014</h1>
                            </div>
                            <div class="col-sm-4 hidden-xs">
                                <? include( "blocs/social.php"); ?>
                            </div>
                        </div>
                        <p class="texte-accroche">Pour sa 4e édition, du 12 au 19 avril, l’événement fait « par la jeunesse, pour la jeunesse », explore de nouveaux territoires artistiques et géographiques.</p>
                        <div class="visuel">
                            <figure>
                                <img src="img/editorial/RTEmagicC_affiche-hiphop-graf.jpg" class="img-responsive" alt="graff place de la Loire">
                                <figcaption class="legend">Benjamin, formateur et animateur, et Filipe, directeur du FabLab, lors d'un atelier de fabrication de drônes.</figcaption>
                            </figure>
                        </div>
                    </header>
                    <div class="rteContent">
                        <h2>En bref ...</h2>
                        <p>Du graffiti au skate, du rap aux battles de danse hip-hop, en passant par les jeux vidéo, la sérigraphie ou le BMX. Des shows grand public aux performances expérimentales, des concerts de groupes locaux aux démonstrations de crews internationaux…</p>
                        <p>En 2014, le Festival Hip-Hop d’Orléans prend une nouvelle impulsion, explorant de nouveaux lieux et de nouvelles disciplines. <em>«Il s’est enrichi, étoffé, grâce à l’incroyable implication des associations locales»</em>, s’exprime Adeline Tutois, responsable de la Mission jeunesse de la Mairie, co-organisatrice de l’événement. C’est la force de cette édition en ébullition : <em>«Être un terrain d’expression pour tous les styles, toutes les influences, tous les jours pendant une semaine, dans des lieux aussi variés que la place de la Loire, le cinéma des Carmes et le théâtre Gérard-Philipe, ou inattendus que le musée des Beaux-Arts et la gare. En faisant en sorte que les puristes comme les aficionados y trouvent leur compte.»</em> Les cultures urbaines n’ont jamais été aussi flamboyantes dans le ciel orléanais !</p>
                        <p>Tour d’horizon des lieux emblématiques investis, cette année, par le Festival hip-hop, et des nouvelles pratiques mises en lumière.</p>
                        <h2>Face au fleuve royal, la block party </h2>
                        <p>« Nouvelle formule » donne le coup d’envoi des festivités, le 12 avril, sur un périmètre plus étendu allant de la rue des Halles jusqu’aux quais, en faisant un crochet par la place de la Loire. Amateurs de sensations fortes, de danse, graffiti et sports extrêmes, ce rendez-vous est fait pour vous. Au programme : scène pour les rappeurs, miroirs mobiles pour les danseurs, émission radio en live, battle de graff sur camion, modules de skate… Et plein de nouveautés, entre arcades de jeux vidéo, ateliers de sérigraphie, slackline (l’art de progresser sur un câble comme un funambule) et street basket.</p>
                        <h2>Le collège Anatole-Bailly devient « l’âme » et le QG du festival </h2>
                        <p>Lieu de rassemblement des multiples associations et courants artistiques &ndash; connus ou plus underground &ndash;, plateforme d’échange avec le public, ce site en coeur de ville, propose une soirée d’ouverture exceptionnelle mêlant warm-up musical, impros et duet improbable, le 12 avril, à partir de 18h30. Un temps fort suivi d’une « programmation off » tout au long de la semaine, au cours de laquelle vont se succéder offterworks &ndash; rétrospective musicale et danse orchestrée par un DJ, les 14, 15, 16 et 17 avril à 17h30 &ndash; concerts, surprises culinaires, battle… et exposition « Criss Cross » des oeuvres produites par les artistes (le 19 avril à 15h).</p>
                        <h2>Inattendu là encore… </h2>
                        <p>le centre-ville sera le théâtre d’une rencontre étonnante le 13 avril, à 12h (choix du lieu en cours). Les musiciens baroques des, accompagnés par des chanteurs du Conservatoire et des choristes, entrent en collision artistique avec des danseurs hip-hop orléanais pour un projet totalement fou ! Un flash mob qui devrait surprendre et donner des frissons. Libre aux spectateurs d’entrer dans la danse, la vidéo de la chorégraphie imaginée par Abderzah Houmi étant projetée sur un écran !</p>
                        <h2>Le théâtre Gérard-Philipe </h2>
                        <p>Le théâtre est assiégé par l’association OP45 passée maître dans l’art du grand spectacle. Deux rendez-vous incontournables au compteur hip-hop : les 4e rencontres des arts urbains, affichant les créations hautes en couleur des groupes locaux et les chorégraphies époustouflantes des Wanted Posse, repérés sur l’émission « La France a un incroyable talent » (le 18 avril à 20h). Autre événement pour faire le buzz : les championnats de danse hip-hop, le CODE (Challenge of dance experience) pour les connaisseurs, qui rassembleront, le 19 avril, à 20h, une centaine de danseurs venus de toute la France. Leur défi : intégrer une musique rock dans leurs chorégraphies hip-hop !</p>
                        <div class="visuel">
                            <figure>
                                <img src="img/editorial/RTEmagicC_affiche-hiphop-graf.jpg" class="img-responsive" alt="hip hop aede">
                                <figcaption class="legend">Hip hop</figcaption>
                            </figure>
                        </div>
                        <h2>Au musée des Beaux-Arts</h2>
                        <p>Le 13 avril, à 15h. Aux manettes du Monde de Modan, démentielle battle chorégraphique mêlant mots et danse, la Compagnie Aede. Son idée : impulser une joute urbaine déambulant au milieu des oeuvres anciennes du musée, avec des duos composés de danseurs hip-hop associés à des rappeurs, slameurs ou comédiens. Dans une salve d’impros totalement libres et débridées, mariant gestualité de la danse et musicalité de la voix. De quoi susciter la curiosité du public et titiller l’imaginaire des artistes.</p>
                    </div>

                    <div class="dce--icon-text dce--icon-text--position-top">
                        <div class="dce--icon-text--icon dce--icon-text--size-default dce--icon-text--type-circle">
                            <span class="glyphicon glyphicon-th-large" style="color: #ffffff;background-color: #1f3f4b;"></span>
                        </div>
                        <div class="dce--icon-text-content">
                            <h2 class="text-center first-headline">Un concept inédit en France</h2>
                            <p>Plutôt que d’investir dans un bâtiment multifonction, Orléans Métropole a fait le choix d’un ensemble modulable et complémentaire</p>
                        </div>
                    </div>


                    <aside class="block-special">
                        <h2>Nouveau visage du hip-hop...</h2>
                        <div class="row">
                            <div class="col-sm-3">
                                <figure>
                                    <img class="img-responsive" width="186" height="279" alt="Guillaume Dechambenoît" src="img/editorial/RTEmagicC_Hip-hop_dechambenoit.jpg">
                                    <figcaption class="sr-only">Nouveau visage du hip-hop...</figcaption>
                                </figure>
                            </div>
                            <div class="col-sm-8">
                                <p>La création, l’exploration de nouveaux territoires, la transdisciplinarité… Autant de nouveaux facteurs, essentiels, pour le festival hip-hop d’Orléans. Parmi les électrons libres insufflant de l’énergie à la manifestation : la Compagnie Aede, à la croisée de la danse, de l’image, de la pédagogie et de la recherche. À sa tête &ndash; pensante &ndash; Guillaume Dechambenoît qui a imaginé moult rendez-vous du festival, commes les Offterworks au QJ Anatole-Bailly, et « Le Monde de Modan » au musée des Beaux-Arts. La devise du selfman hyperactif, au corps autodidacte et à l’esprit formé aux neurosciences : « Créer des ponts entre les disciplines, casser les codes. »</p>
                                <p>Pour ce danseur contemporain qui a métissé son style grâce à ses voyages entre Côte d’Ivoire, Québec et France, « le hip-hop est une danse sociale qui se partage, dans laquelle on échange et on se retrouve ensemble dans le plaisir ». Corps et âme, sans frontières ni limites.</p>
                            </div>
                        </div>
                    </aside>
                    <aside class="infos">
                        <h2>Retro 2013</h2>
                        <div class="embed-responsive embed-responsive-16by9">
                            <iframe class="embed-responsive-item" allowfullscreen="" src="//www.youtube.com/embed/uQCwyNLWo1Q"></iframe>
                        </div>
                    </aside>
                    <aside class="infos">
                        <div class="gallery">
                            <h2>En images ...</h2>
                            <ul class="list-unstyled row">
                                <li class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
                                    <a class="thumbnail" title="Festival Hip Hop  - © J Grelet - Block Party place de la Loire" href="http://www.orleans.fr/fileadmin/orleans/MEDIA/diaporama/hiphop2014/IMG_6835.JPG">
                                        <figure>
                                            <img class="img-responsive" alt="Festival Hip Hop" src="img/editorial/bffdfea845.jpg">
                                            <figcaption class="sr-only"></figcaption>
                                        </figure>
                                    </a>
                                </li>
                                <li class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
                                    <a class="thumbnail" title="Festival Hip Hop  - © J Grelet - Block Party place de la Loire" href="http://www.orleans.fr/fileadmin/orleans/MEDIA/diaporama/hiphop2014/IMG_6838.JPG">
                                        <figure>
                                            <img class="img-responsive" alt="Festival Hip Hop" src="img/editorial/9e995f041e.jpg">
                                            <figcaption class="sr-only"></figcaption>
                                        </figure>
                                    </a>
                                </li>
                                <li class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
                                    <a class="thumbnail" title="Festival Hip Hop  - © J Grelet - Block Party place de la Loire" href="http://www.orleans.fr/fileadmin/orleans/MEDIA/diaporama/hiphop2014/IMG_6853.JPG">
                                        <figure>
                                            <img class="img-responsive" alt="Festival Hip Hop" src="img/editorial/b5e5262ea3.jpg">
                                            <figcaption class="sr-only"></figcaption>
                                        </figure>
                                    </a>
                                </li>
                                <li class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
                                    <a class="thumbnail" title="Festival Hip Hop  - © J Grelet - Block Party place de la Loire" href="http://www.orleans.fr/fileadmin/orleans/MEDIA/diaporama/hiphop2014/IMG_6861.JPG">
                                        <figure>
                                            <img class="img-responsive" alt="Festival Hip Hop" src="img/editorial/e0c4f35a5b.jpg">
                                            <figcaption class="sr-only"></figcaption>
                                        </figure>
                                    </a>
                                </li>
                                <li class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
                                    <a class="thumbnail" title="Festival Hip Hop  - © J Grelet - Block Party place de la Loire" href="http://www.orleans.fr/fileadmin/orleans/MEDIA/diaporama/hiphop2014/IMG_6895.JPG">
                                        <figure>
                                            <img class="img-responsive" alt="Festival Hip Hop" src="img/editorial/441a6f0824.jpg">
                                            <figcaption class="sr-only"></figcaption>
                                        </figure>
                                    </a>
                                </li>
                                <li class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
                                    <a class="thumbnail" title="Festival Hip Hop  - © J Grelet - Block Party place de la Loire" href="http://www.orleans.fr/fileadmin/orleans/MEDIA/diaporama/hiphop2014/IMG_6896.JPG">
                                        <figure>
                                            <img class="img-responsive" alt="Festival Hip Hop" src="img/editorial/eb2623948f.jpg">
                                            <figcaption class="sr-only"></figcaption>
                                        </figure>
                                    </a>
                                </li>
                                <li class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
                                    <a class="thumbnail" title="Festival Hip Hop  - © J Grelet - Block Party place de la Loire" href="http://www.orleans.fr/fileadmin/orleans/MEDIA/diaporama/hiphop2014/IMG_6916.JPG">
                                        <figure>
                                            <img class="img-responsive" alt="Festival Hip Hop" src="img/editorial/b3735cc1de.jpg">
                                            <figcaption class="sr-only"></figcaption>
                                        </figure>
                                    </a>
                                </li>
                                <li class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
                                    <a class="thumbnail" title="Festival Hip Hop  - © J Grelet - Block Party place de la Loire" href="http://www.orleans.fr/fileadmin/orleans/MEDIA/diaporama/hiphop2014/IMG_6925.JPG">
                                        <figure>
                                            <img class="img-responsive" alt="Festival Hip Hop" src="img/editorial/2239a70c6b.jpg">
                                            <figcaption class="sr-only"></figcaption>
                                        </figure>
                                    </a>
                                </li>
                                <li class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
                                    <a class="thumbnail" title="Festival Hip Hop  - © J Grelet - Block Party place de la Loire" href="http://www.orleans.fr/fileadmin/orleans/MEDIA/diaporama/hiphop2014/IMG_6993.JPG">
                                        <figure>
                                            <img class="img-responsive" alt="Festival Hip Hop" src="img/editorial/7348beded2.jpg">
                                            <figcaption class="sr-only"></figcaption>
                                        </figure>
                                    </a>
                                </li>
                                <li class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
                                    <a class="thumbnail" title="Festival Hip Hop  - © J Grelet - Block Party place de la Loire" href="http://www.orleans.fr/fileadmin/orleans/MEDIA/diaporama/hiphop2014/IMG_6999.JPG">
                                        <figure>
                                            <img class="img-responsive" alt="Festival Hip Hop" src="img/editorial/b9302ed611.jpg">
                                            <figcaption class="sr-only"></figcaption>
                                        </figure>
                                    </a>
                                </li>
                                <li class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
                                    <a class="thumbnail" title="Festival Hip Hop  - © J Grelet - Block Party place de la Loire" href="http://www.orleans.fr/fileadmin/orleans/MEDIA/diaporama/hiphop2014/IMG_7007.jpg">
                                        <figure>
                                            <img class="img-responsive" alt="Festival Hip Hop" src="img/editorial/6e38d83568.jpg">
                                            <figcaption class="sr-only"></figcaption>
                                        </figure>
                                    </a>
                                </li>
                                <li class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
                                    <a class="thumbnail" title="Festival Hip Hop  - © J Grelet - Block Party place de la Loire" href="http://www.orleans.fr/fileadmin/orleans/MEDIA/diaporama/hiphop2014/IMG_7053.JPG">
                                        <figure>
                                            <img class="img-responsive" alt="Festival Hip Hop" src="img/editorial/0aeb41126c.jpg">
                                            <figcaption class="sr-only"></figcaption>
                                        </figure>
                                    </a>
                                </li>
                                <li class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
                                    <a class="thumbnail" title="Festival Hip Hop  - © J Grelet - Block Party place de la Loire" href="http://www.orleans.fr/fileadmin/orleans/MEDIA/diaporama/hiphop2014/IMG_7066.JPG">
                                        <figure>
                                            <img class="img-responsive" alt="Festival Hip Hop" src="img/editorial/5dd2f8512a.jpg">
                                            <figcaption class="sr-only"></figcaption>
                                        </figure>
                                    </a>
                                </li>
                                <li class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
                                    <a class="thumbnail" title="Festival Hip Hop  - © J Grelet - Block Party place de la Loire" href="http://www.orleans.fr/fileadmin/orleans/MEDIA/diaporama/hiphop2014/IMG_7075.JPG">
                                        <figure>
                                            <img class="img-responsive" alt="Festival Hip Hop" src="img/editorial/193d687a86.jpg">
                                            <figcaption class="sr-only"></figcaption>
                                        </figure>
                                    </a>
                                </li>
                                <li class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
                                    <a class="thumbnail" title="Festival Hip Hop  - © J Grelet - Block Party place de la Loire" href="http://www.orleans.fr/fileadmin/orleans/MEDIA/diaporama/hiphop2014/IMG_7082.JPG">
                                        <figure>
                                            <img class="img-responsive" alt="Festival Hip Hop" src="img/editorial/a6532cfdb1.jpg">
                                            <figcaption class="sr-only"></figcaption>
                                        </figure>
                                    </a>
                                </li>
                                <li class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
                                    <a class="thumbnail" title="Festival Hip Hop  - © J Grelet - Block Party place de la Loire" href="http://www.orleans.fr/fileadmin/orleans/MEDIA/diaporama/hiphop2014/IMG_9144.JPG">
                                        <figure>
                                            <img class="img-responsive" alt="Festival Hip Hop" src="img/editorial/e5126f9026.jpg">
                                            <figcaption class="sr-only"></figcaption>
                                        </figure>
                                    </a>
                                </li>
                                <li class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
                                    <a class="thumbnail" title="Festival Hip Hop  - © J Grelet - Block Party place de la Loire" href="http://www.orleans.fr/fileadmin/orleans/MEDIA/diaporama/hiphop2014/IMG_9164.JPG">
                                        <figure>
                                            <img class="img-responsive" alt="Festival Hip Hop" src="img/editorial/c790ebb1dd.jpg">
                                            <figcaption class="sr-only"></figcaption>
                                        </figure>
                                    </a>
                                </li>
                                <li class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
                                    <a class="thumbnail" title="Festival Hip Hop  - © J Grelet - Block Party place de la Loire" href="http://www.orleans.fr/fileadmin/orleans/MEDIA/diaporama/hiphop2014/IMG_9176.JPG">
                                        <figure>
                                            <img class="img-responsive" alt="Festival Hip Hop" src="img/editorial/b639536269.jpg">
                                            <figcaption class="sr-only"></figcaption>
                                        </figure>
                                    </a>
                                </li>
                                <li class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
                                    <a class="thumbnail" title="Festival Hip Hop  - © J Grelet - Block Party place de la Loire" href="http://www.orleans.fr/fileadmin/orleans/MEDIA/diaporama/hiphop2014/IMG_9241.jpg">
                                        <figure>
                                            <img class="img-responsive" alt="Festival Hip Hop" src="img/editorial/d268eb52b9.jpg">
                                            <figcaption class="sr-only"></figcaption>
                                        </figure>
                                    </a>
                                </li>
                                <li class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
                                    <a class="thumbnail" title="Festival Hip Hop  - © J Grelet - Block Party place de la Loire" href="http://www.orleans.fr/fileadmin/orleans/MEDIA/diaporama/hiphop2014/IMG_9266.JPG">
                                        <figure>
                                            <img class="img-responsive" alt="Festival Hip Hop" src="img/editorial/8ab13a452f.jpg">
                                            <figcaption class="sr-only"></figcaption>
                                        </figure>
                                    </a>
                                </li>
                                <li class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
                                    <a class="thumbnail" title="Festival Hip Hop  - © J Grelet - Block Party place de la Loire" href="http://www.orleans.fr/fileadmin/orleans/MEDIA/diaporama/hiphop2014/IMG_9294.JPG">
                                        <figure>
                                            <img class="img-responsive" alt="Festival Hip Hop" src="img/editorial/b6ba423f37.jpg">
                                            <figcaption class="sr-only"></figcaption>
                                        </figure>
                                    </a>
                                </li>
                                <li class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
                                    <a class="thumbnail" title="Festival Hip Hop  - © J Grelet - Block Party place de la Loire" href="http://www.orleans.fr/fileadmin/orleans/MEDIA/diaporama/hiphop2014/IMG_9309.JPG">
                                        <figure>
                                            <img class="img-responsive" alt="Festival Hip Hop" src="img/editorial/fcdb4b8add.jpg">
                                            <figcaption class="sr-only"></figcaption>
                                        </figure>
                                    </a>
                                </li>
                                <li class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
                                    <a class="thumbnail" title="Festival Hip Hop  - © J Grelet - Block Party place de la Loire" href="http://www.orleans.fr/fileadmin/orleans/MEDIA/diaporama/hiphop2014/IMG_9340.JPG">
                                        <figure>
                                            <img class="img-responsive" alt="Festival Hip Hop" src="img/editorial/32a3161ef1.jpg">
                                            <figcaption class="sr-only"></figcaption>
                                        </figure>
                                    </a>
                                </li>
                                <li class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
                                    <a class="thumbnail" title="Festival Hip Hop  - © J Grelet - Block Party place de la Loire" href="http://www.orleans.fr/fileadmin/orleans/MEDIA/diaporama/hiphop2014/IMG_9353.JPG">
                                        <figure>
                                            <img class="img-responsive" alt="Festival Hip Hop" src="img/editorial/44f389edbf.jpg">
                                            <figcaption class="sr-only"></figcaption>
                                        </figure>
                                    </a>
                                </li>
                                <li class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
                                    <a class="thumbnail" title="Festival Hip Hop  - © J Grelet - Flash Mob, place sainte Croix" href="http://www.orleans.fr/fileadmin/orleans/MEDIA/diaporama/hiphop2014/IMG_9371.JPG">
                                        <figure>
                                            <img class="img-responsive" alt="Festival Hip Hop" src="img/editorial/f6d433b77e.jpg">
                                            <figcaption class="sr-only"></figcaption>
                                        </figure>
                                    </a>
                                </li>
                                <li class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
                                    <a class="thumbnail" title="Festival Hip Hop  - © J Grelet - Flash Mob, place sainte Croix" href="http://www.orleans.fr/fileadmin/orleans/MEDIA/diaporama/hiphop2014/IMG_9473.JPG">
                                        <figure>
                                            <img class="img-responsive" alt="Festival Hip Hop" src="img/editorial/cab32ad4bf.jpg">
                                            <figcaption class="sr-only"></figcaption>
                                        </figure>
                                    </a>
                                </li>
                                <li class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
                                    <a class="thumbnail" title="Festival Hip Hop  - © J Grelet - Flash Mob, place sainte Croix" href="http://www.orleans.fr/fileadmin/orleans/MEDIA/diaporama/hiphop2014/IMG_9496.JPG">
                                        <figure>
                                            <img class="img-responsive" alt="Festival Hip Hop" src="img/editorial/484adb5979.jpg">
                                            <figcaption class="sr-only"></figcaption>
                                        </figure>
                                    </a>
                                </li>
                                <li class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
                                    <a class="thumbnail" title="Festival Hip Hop  - © J Grelet - Flash Mob, place sainte Croix" href="http://www.orleans.fr/fileadmin/orleans/MEDIA/diaporama/hiphop2014/IMG_9507.JPG">
                                        <figure>
                                            <img class="img-responsive" alt="Festival Hip Hop" src="img/editorial/50b9fa6014.jpg">
                                            <figcaption class="sr-only"></figcaption>
                                        </figure>
                                    </a>
                                </li>
                                <li class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
                                    <a class="thumbnail" title="Festival Hip Hop  - © J Grelet - Flash Mob, place sainte Croix" href="http://www.orleans.fr/fileadmin/orleans/MEDIA/diaporama/hiphop2014/IMG_9521.JPG">
                                        <figure>
                                            <img class="img-responsive" alt="Festival Hip Hop" src="img/editorial/9caa43819d.jpg">
                                            <figcaption class="sr-only"></figcaption>
                                        </figure>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </aside>
                    <div class="visible-xs text-center">
                        <?php include( "blocs/social.php"); ?>
                    </div>
            </div>
            <div class="col-md-4 col-lg-3">
                <?php include( "blocs/editorial-filter.php"); ?>
            </div>
                </article>
        </div>
    </div>
    <?php include( 'blocs/footer.php'); ?>
    <?php include( 'blocs/scripts.php'); ?>
</body>

</html>

