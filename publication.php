<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Le site d'Orléans et son AgglO - Liste d'événements</title>
<?php include( 'blocs/styles.php') ?>
</head>
<body>
<?php include( 'blocs/header.php') ?>
<div class="container publication">
  <div class="row">
    <div class="col-md-8 col-lg-9">
      <ol class="breadcrumb hidden-xs">
        <li><a href="index.php">Accueil</a> </li>
        <li><a href="actu-list.php">Actualités</a> </li>
        <li class="active">Le Fab Lab orléanais</li>
      </ol>
      <section>
        <header class="header-article">
          <div class="row">
            <div class="col-sm-8">
              <h1>Publications</h1>
            </div>
            <div class="col-sm-4 hidden-xs">
              <?php include( "blocs/social.php"); ?>
            </div>
          </div>
          <div class="ads-com-carousel-slide">
            <div id="carousel-publication" class="carousel slide carousel-publication" data-ride="carousel">
              <!-- Indicators -->
              <ol class="carousel-indicators">
                <li data-target="#carousel-publication" data-slide-to="0" class="active"></li>
                <li data-target="#carousel-publication" data-slide-to="1"></li>
                <li data-target="#carousel-publication" data-slide-to="2"></li>
                <li data-target="#carousel-publication" data-slide-to="3"></li>
              </ol>
              <div class="carousel-inner" role="listbox">
                <div class="item active">
                  <div class="col-sm-4 picture">
                    <figure> <img class="img-responsive " src="img/hommage.jpg" alt="A Pleine Voix">
                      <figcaption class="sr-only"></figcaption>
                    </figure>
                  </div>
                  <div class="col-sm-8 carousel-text">
                    <h2><a href="#">A Pleine Voix</a></h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus aliquam eget tortor ac sollicitudin. Praesent eget accumsan sapien, vel pulvinar ante ipsum dolor sit amet, consectetur adipiscing elit. Phasellus aliquam eget tortor ac sollicitudin. Praesent eget accumsan sapien, vel pulvinar ante.</p>
                    <p><a href="#" class="btn btn-primary"><span class="fa fa-eye"></span> Consulter</a></p>
                  </div>
                </div>
                <div class="item">
                  <div class="col-sm-4">
                    <figure> <img class="img-responsive " src="img/marche.jpg" alt="A Pleine Voix">
                      <figcaption class="sr-only">Le marché de Noel des étoiles</figcaption>
                    </figure>
                  </div>
                  <div class="col-sm-8 carousel-text">
                    <h2><a href="#">Le marché de Noel des étoiles</a></h2>
                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</p>
                    <p><a href="#" class="btn btn-primary"><span class="fa fa-eye"></span> Consulter</a></p>
                  </div>
                </div>
                <div class="item">
                  <div class="col-sm-4">
                    <figure> <img class="img-responsive " src="img/marche.jpg" alt="A Pleine Voix">
                      <figcaption class="sr-only">Le marché de Noel des étoiles</figcaption>
                    </figure>
                  </div>
                  <div class="col-sm-8 carousel-text">
                    <h2><a href="#">Le marché de Noel des étoiles</a></h2>
                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</p>
                    <p><a href="#" class="btn btn-primary"><span class="fa fa-eye"></span> Consulter</a></p>
                  </div>
                </div>
                <div class="item">
                  <div class="col-sm-4">
                    <figure> <img class="img-responsive " src="img/marche.jpg" alt="A Pleine Voix">
                      <figcaption class="sr-only">Le marché de Noel des étoiles</figcaption>
                    </figure>
                  </div>
                  <div class="col-sm-8 carousel-text">
                    <h2><a href="#">Le marché de Noel des étoiles</a></h2>
                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</p>
                    <p><a href="#" class="btn btn-primary"><span class="fa fa-eye"></span> Consulter</a></p>
                  </div>
                </div>
                <!-- Controls -->
                <a class="left carousel-control" href="#carousel-publication" role="button" data-slide="prev"> <span class="fa fa-chevron-left"></span></a> <a class="right carousel-control" href="#carousel-publication" role="button" data-slide="next"> <span class="fa fa-chevron-right"></span></a> </div>
            </div>
          </div>
        </header>
        <div class="row liste-item">



            <div class="col-sm-6 col-md-3 ">
                <article class="publication">
                    <div class="visuel"> <a href="#">
                            <figure> <img class="img-responsive" src=img/marche.jpg alt="">
                                <figcaption class="sr-only"></figcaption>
                            </figure>
                        </a> </div>
                    <div class="caption">
                        <h3><a href="#">Braderie d'hiver</a></h3>
                        <p>La Grande Braderie vous donne envie de démarrer la rentrée du bon pied !</p>
                        <a href="#" target="_blank" class="btn-publication"><span class="fa fa-download"></span> Télécharger</a> </div>
                </article>
            </div>
            <div class="col-sm-6 col-md-3 ">
                <article class="publication">
                    <div class="visuel"> <a href="#">
                            <figure> <img class="img-responsive" src="img/hommage.jpg" alt="">
                                <figcaption class="sr-only"></figcaption>
                            </figure>
                        </a> </div>
                    <div class="caption">
                        <h3><a href="#">Braderie d'hiver</a></h3>
                        <p>La Grande Braderie vous donne envie de démarrer la rentrée du bon pied !</p>
                        <a href="#" target="_blank" class="btn-publication"><span class="fa fa-download"></span> Télécharger</a> </div>
                </article>
            </div>
            <div class="col-sm-6 col-md-3 ">
                <article class="publication">
                    <div class="visuel"> <a href="#">
                            <figure> <img class="img-responsive" src="img/marche.jpg" alt="">
                                <figcaption class="sr-only"></figcaption>
                            </figure>
                        </a> </div>
                    <div class="caption">
                        <h3><a href="#">Braderie d'hiver</a></h3>
                        <p>La Grande Braderie vous donne envie de démarrer la rentrée du bon pied !</p>
                        <a href="#" target="_blank" class="btn-publication"><span class="fa fa-download"></span> Télécharger</a> </div>
                </article>
            </div>
            <div class="col-sm-6 col-md-3 ">
                <article class="publication">
                    <div class="visuel"> <a href="#">
                            <figure> <img class="img-responsive" src="img/marche.jpg" alt="">
                                <figcaption class="sr-only"></figcaption>
                            </figure>
                        </a> </div>
                    <div class="caption">
                        <h3><a href="#">Braderie d'hiver</a></h3>
                        <p>La Grande Braderie vous donne envie de démarrer la rentrée du bon pied !</p>
                        <a href="#" target="_blank" class="btn-publication"><span class="fa fa-download"></span> Télécharger</a> </div>
                </article>
            </div>
            <div class="col-sm-6 col-md-3 ">
                <article class="publication">
                    <div class="visuel"> <a href="#">
                            <figure> <img class="img-responsive" src="img/marche.jpg" alt="">
                                <figcaption class="sr-only"></figcaption>
                            </figure>
                        </a> </div>
                    <div class="caption">
                        <h3><a href="#">Braderie d'hiver</a></h3>
                        <p>La Grande Braderie vous donne envie de démarrer la rentrée du bon pied !</p>
                        <a href="#" target="_blank" class="btn-publication"><span class="fa fa-download"></span> Télécharger</a> </div>
                </article>
            </div>
            <div class="col-sm-6 col-md-3 ">
                <article class="publication">
                    <div class="visuel"> <a href="#">
                            <figure> <img class="img-responsive" src="img/marche.jpg" alt="">
                                <figcaption class="sr-only"></figcaption>
                            </figure>
                        </a> </div>
                    <div class="caption">
                        <h3><a href="#">Braderie d'hiver</a></h3>
                        <p>La Grande Braderie vous donne envie de démarrer la rentrée du bon pied !</p>
                        <a href="#" target="_blank" class="btn-publication"><span class="fa fa-download"></span> Télécharger</a> </div>
                </article>
            </div>
            <div class="col-sm-6 col-md-3 ">
                <article class="publication">
                    <div class="visuel"> <a href="#">
                            <figure> <img class="img-responsive" src="img/marche.jpg" alt="">
                                <figcaption class="sr-only"></figcaption>
                            </figure>
                        </a> </div>
                    <div class="caption">
                        <h3><a href="#">Braderie d'hiver</a></h3>
                        <p>La Grande Braderie vous donne envie de démarrer la rentrée du bon pied !</p>
                        <a href="#" target="_blank" class="btn-publication"><span class="fa fa-download"></span> Télécharger</a> </div>
                </article>
            </div>



          <div class="col-sm-6 col-md-4 ">
            <article class="publication">
              <div class="visuel"> <a href="#">
                <figure> <img class="img-responsive" src="img/braderie.jpg" alt="">
                  <figcaption class="sr-only"></figcaption>
                </figure>
                </a> </div>
              <div class="caption">
                <h3><a href="#">Braderie d'hiver</a></h3>
                <p>La Grande Braderie vous donne envie de démarrer la rentrée du bon pied !</p>
                <a href="#" target="_blank" class="btn-publication"><span class="fa fa-download"></span> Télécharger</a> </div>
            </article>
          </div>

          <div class="col-sm-6 col-md-4 ">
            <article class="publication">
              <div class="visuel"> <a href="#">
                <figure> <img class="img-responsive" src="img/pleine-voix.jpg" alt="">
                  <figcaption class="sr-only"></figcaption>
                </figure>
                </a> </div>
              <div class="caption">
                <h3><a href="#">A Pleine Voix</a></h3>
                <p>Un programme riche et varié.</p>
                <a href="#" target="_blank" class="btn-publication"><span class="fa fa-download"></span> Télécharger</a> </div>
            </article>
          </div>
          <div class="col-sm-6 col-md-4 ">
            <article class="publication">
              <div class="visuel"> <a href="#">
                <figure> <img class="img-responsive" src="img/festival.jpg" alt="">
                  <figcaption class="sr-only"></figcaption>
                </figure>
                </a> </div>
              <div class="caption">
                <h3><a href="#">Festival de Loire</a></h3>
                <p>Programme d'animation d'été 2015 : animations des quais, expositions culturelles,14 juillet, Fête des Duits...</p>
                <a href="#" target="_blank" class="btn-publication"><span class="fa fa-download"></span> Télécharger</a> <br>
              </div>
            </article>
          </div>
          <div class="col-sm-6 col-md-4 ">
            <article class="publication">
              <div class="visuel"> <a href="#">
                <figure> <img class="img-responsive" src="img/braderie.jpg" alt="">
                  <figcaption class="sr-only"></figcaption>
                </figure>
                </a> </div>
              <div class="caption">
                <h3><a href="#">Braderie d'hiver</a></h3>
                <p>La Grande Braderie vous donne envie de démarrer la rentrée du bon pied !</p>
                <a href="#" target="_blank" class="btn-publication"><span class="fa fa-download"></span> Télécharger</a> </div>
            </article>
          </div>
          <div class="col-sm-6 col-md-4 ">
            <article class="publication">
              <div class="visuel"> <a href="#">
                <figure> <img class="img-responsive" src="img/pleine-voix.jpg" alt="">
                  <figcaption class="sr-only"></figcaption>
                </figure>
                </a> </div>
              <div class="caption">
                <h3><a href="#">A Pleine Voix</a></h3>
                <p>Un programme riche et varié.</p>
                <a href="#" target="_blank" class="btn-publication"><span class="fa fa-download"></span> Télécharger</a> </div>
            </article>
          </div>
          <div class="col-sm-6 col-md-4 ">
            <article class="publication">
              <div class="visuel"> <a href="#">
                <figure> <img class="img-responsive" src="img/festival.jpg" alt="">
                  <figcaption class="sr-only"></figcaption>
                </figure>
                </a> </div>
              <div class="caption">
                <h3><a href="#">Festival de Loire</a></h3>
                <p>Programme d'animation d'été 2015 : animations des quais, expositions culturelles,14 juillet, Fête des Duits...</p>
                <a href="#" target="_blank" class="btn-publication"><span class="fa fa-download"></span> Télécharger</a> <br>
              </div>
            </article>
          </div>
          <div class="col-sm-6 col-md-4 ">
            <article class="publication">
              <div class="visuel"> <a href="#">
                <figure> <img class="img-responsive" src="img/braderie.jpg" alt="">
                  <figcaption class="sr-only"></figcaption>
                </figure>
                </a> </div>
              <div class="caption">
                <h3><a href="#">Braderie d'hiver</a></h3>
                <p>La Grande Braderie vous donne envie de démarrer la rentrée du bon pied !</p>
                <a href="#" target="_blank" class="btn-publication"><span class="fa fa-download"></span> Télécharger</a> </div>
            </article>
          </div>
          <div class="col-sm-6 col-md-4 ">
            <article class="publication">
              <div class="visuel"> <a href="#">
                <figure> <img class="img-responsive" src="img/pleine-voix.jpg" alt="">
                  <figcaption class="sr-only"></figcaption>
                </figure>
                </a> </div>
              <div class="caption">
                <h3><a href="#">A Pleine Voix</a></h3>
                <p>Un programme riche et varié.</p>
                <a href="#" target="_blank" class="btn-publication right"><span class="fa fa-book"></span> Consulter</a>
                <a href="#" target="_blank" class="btn-publication"><span class="fa fa-download"></span> Télécharger</a>
              </div>
            </article>
          </div>
          <div class="col-sm-6 col-md-4 ">
            <article class="publication">
              <div class="visuel"> <a href="#">
                <figure> <img class="img-responsive" src="img/festival.jpg" alt="">
                  <figcaption class="sr-only"></figcaption>
                </figure>
                </a> </div>
              <div class="caption">
                <h3><a href="#">Festival de Loire</a></h3>
                <p>Programme d'animation d'été 2015 : animations des quais, expositions culturelles,14 juillet, Fête des Duits...</p>
                <a href="#" target="_blank" class="btn-publication"><span class="fa fa-download"></span> Télécharger</a> <br>
              </div>
            </article>
          </div>
          <div class="col-sm-6 col-md-4 ">
            <article class="publication">
              <div class="visuel"> <a href="#">
                <figure> <img class="img-responsive" src="img/braderie.jpg" alt="">
                  <figcaption class="sr-only"></figcaption>
                </figure>
                </a> </div>
              <div class="caption">
                <h3><a href="#">Braderie d'hiver</a></h3>
                <p>La Grande Braderie vous donne envie de démarrer la rentrée du bon pied !</p>
                <a href="#" target="_blank" class="btn-publication"><span class="fa fa-download"></span> Télécharger</a> </div>
            </article>
          </div>
          <div class="col-sm-6 col-md-4 ">
            <article class="publication">
              <div class="visuel"> <a href="#">
                <figure> <img class="img-responsive" src="img/pleine-voix.jpg" alt="">
                  <figcaption class="sr-only"></figcaption>
                </figure>
                </a> </div>
              <div class="caption">
                <h3><a href="#">A Pleine Voix</a></h3>
                <p>Un programme riche et varié.</p>
                <a href="#" target="_blank" class="btn-publication"><span class="fa fa-download"></span> Télécharger</a> </div>
            </article>
          </div>
          <div class="col-sm-6 col-md-4 ">
            <article class="publication">
              <div class="visuel"> <a href="#">
                <figure> <img class="img-responsive" src="img/festival.jpg" alt="">
                  <figcaption class="sr-only"></figcaption>
                </figure>
                </a> </div>
              <div class="caption">
                <h3><a href="#">Festival de Loire</a></h3>
                <p>Programme d'animation d'été 2015 : animations des quais, expositions culturelles,14 juillet, Fête des Duits...</p>
                <a href="#" target="_blank" class="btn-publication"><span class="fa fa-download"></span> Télécharger</a> <br>
              </div>
            </article>
          </div>
        </div>
        <nav class="text-center">
          <h2 class="sr-only">Pagination</h2>
          <?php include( "blocs/pagination.php") ?>
        </nav>
      </section>
    </div>
    <div class="col-md-4 col-lg-3">
      <?php include( 'blocs/publication-filter.php'); ?>
    </div>
  </div>
</div>
<?php include( 'blocs/footer.php'); ?>
<?php include( 'blocs/scripts.php'); ?>
</body>
</html>
