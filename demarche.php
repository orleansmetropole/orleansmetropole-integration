<?php require "portail/citoyen/config.php" ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/html">

<head>
    <meta charset="utf-8">
    <title>Le site d'Orléans et son AgglO - Accueil</title>
    <?php include( 'blocs/styles.php') ?>

<body>
<?php include( 'blocs/header.php') ?>
<div class="container demarche-summary">

    <ol class="breadcrumb hidden-xs">
        <li><a href="#">Orléans métropole</a>
        </li>
        <li class="active">Démarches</li>
    </ol>

    <header class="accroche header-article">
        <div class="row">
            <div class="col-sm-8">
                <h1>Toutes les démarches</h1>
            </div>
            <div class="col-sm-4 hidden-xs">
                <div class="dropdown-social pull-right hidden-xs">
                    <button class="share dropdown-toggle" type="button" id="dropdownMenuSocial1" data-toggle="dropdown" aria-expanded="true">
                        Partager
                        <span class="fa fa-share-alt btn btn-primary rounded-sm"></span>
                    </button>
                    <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenuSocial1">
                        <li><a href="#"><span class="fa fa-facebook"></span> Facebook</a></li>
                        <li><a href="#"><span class="fa fa-twitter"></span> Twitter</a></li>
                        <li><a href="#"><span class="fa fa-linkedin"></span> LinkedIn</a></li>
                        <li><a href="#"><span class="fa fa-google"></span> Google +</a></li>
                    </ul>
                </div>
                <div class="social-xs text-center visible-xs">
                    <ul class="list-inline list-unstyled">
                        <li><a href="#"><span class="fa fa-facebook btn btn-primary rounded"></span></a></li>
                        <li><a href="#"><span class="fa fa-twitter btn btn-primary rounded"></span></a></li>
                        <li><a href="#"><span class="fa fa-linkedin btn btn-primary rounded"></span></a></li>
                        <li><a href="#"><span class="fa fa-google btn btn-primary rounded"></span></a></li>
                    </ul>
                </div>
            </div>
        </div>
        <p class="texte-accroche">Retrouvez ici l'ensemble des demandes et démarches que vous pouvez effectuer en ligne actuellement.</p>
    </header>

    <?php if ($header == 1) : ?>
    <div class="block-special">
        <div class="row">
            <div class="col-sm-3">
                <h2>Code de suivi</h2>
            </div>
            <div class="col-sm-5">
                <p>Un code de suivi — indiqué en haut des formulaires — peut être associé à vos demandes. Il vous facilite les échanges avec les services et permet aussi de reprendre une démarche interrompue. Pour retrouver une demande disposant d'un code de suivi, indiquez ce dernier ci-dessous :</p>
                <form class="form-horizontal text-center">
                    <div class="form-group input-group">
                        <input type="text" class="form-control input-lg" id="inputCode" placeholder="Code">
                        <span class="input-group-btn">
                            <button type="submit" class="btn btn-lg btn-primary align-center"><span class="fa fa-search"></span></button>
                        </span>
                    </div>
                </form>
                <hr>
            </div>
            <div class="col-md-4 text-center">
                <a href="#">
                    <span class="fa fa-user fa-3x fa-border"></span>
                    <h3>Mon compte</h3>
                    <p class="small">Conçu pour simplifier votre quotidien, Mon Compte vous permet de réaliser vos démarches en ligne 24h/24, 7 jours/7. Vous pourrez également y suivre leur avancement.</p>
                </a>
            </div>
        </div>
    </div>
    <?php endif; ?>

    <?php if ($header == 2) : ?>
        <?php
            $shortcuts = [
                'account' => [
                    'label' => 'Mon compte',
                    'icon'  => 'user',
                    'desc'  => 'Conçu pour simplifier votre quotidien1',
                    'url'   => 'https://moncompte.orleans-metropole.fr/'
                ],
                'famille' => [
                    'label' => 'Portail famille',
                    'icon'  => 'child',
                    'desc'  => 'Gérer votre famille',
                    'url'   => 'https://www.espace-citoyens.net/orleans/espace-citoyens/CompteCitoyen'
                ],
                'guide' => [
                    'label' => 'Guide des démarches',
                    'icon'  => 'book',
                    'desc'  => 'Rechercher des informations sur les démarches',
                    'url'   => 'http://www.orleans-metropole.fr/125/guide-des-demarches.htm#!/Particuliers'
                ]
            ]
        ?>
        <div class="block-special">
            <div class="row">
                <div class="col-sm-5">
                    <div class="row">
                        <h2>Accès rapides</h2>
                        <div class="row">
                            <?php foreach ($shortcuts as $k => $config) : ?>
                                <div class="col-xs-4 text-center quicklink">
                                    <a href="<?php echo $config['url'] ?>">
                                        <span class="fa fa-<?php echo $config['icon'] ?> fa-3x rounded"></span>
                                        <p><?php echo $config['label'] ?></p>
<!--                                        <p class="small">--><?php //echo $config['desc'] ?><!--</p>-->
                                    </a>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-sm-offset-1">
                    <h2>Code de suivi</h2>
                    <p>Un code de suivi — indiqué en haut des formulaires — peut être associé à vos demandes. Il vous facilite les échanges avec les services et permet aussi de reprendre une démarche interrompue.</p>
                    <form class="form-horizontal text-center">
                        <div class="form-group input-group">
                            <input type="text" class="form-control input-lg" id="inputCode" placeholder="Code">
                            <span class="input-group-btn">
                            <button type="submit" class="btn btn-lg btn-primary align-center"><span class="fa fa-search"></span></button>
                        </span>
                        </div>
                    </form>
                    <hr>
                </div>
            </div>
        </div>
    <?php endif; ?>

    <div class="row">
      <?php $i = 0; foreach ($menus as $menu) : $i++;?>
          <article class="col-sm-6 col-md-4 col-lg-4 ">
              <h3 class="bg-primary"><span class="fa fa-<?php echo $menu['icon'] ?>"></span> <?php echo $menu['label'] ?></h3>
              <div class="list-group" data-list-limit="5">
                <?php foreach ($menu['items'] as $item) : ?>
                    <a href="<?php echo $item['url'] ?>" class="list-group-item" <?php if($item['connected']) : ?><?php endif; ?>>
                      <?php echo $item['label'] ?>
                      <?php if($item['connected']) : ?>
                      <span class="badge">
                          <span class="fa fa-user"></span>
                      </span>
                      <?php endif; ?>
                      <?php if (!empty($item['description'])) : ?>
                          <p class="list-group-item-text small text-muted"><?php echo $item['description'] ?></p>
                      <?php endif; ?>
                    </a>
                <?php endforeach; ?>
              </div>
          </article>
        <?php if ($i%3 === 0) : ?><div class="clearfix" style="margin-bottom: 30px"></div><?php endif; ?>
      <?php endforeach; ?>

    </div>

</div>
<?php include( 'blocs/footer.php'); ?>
<?php include( 'blocs/scripts.php'); ?>
</body>

</html>



