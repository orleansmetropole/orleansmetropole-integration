<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/html">

<head>
    <meta charset="utf-8">
    <title>Le site d'Orléans et son AgglO - Accueil</title>
    <?php include( 'blocs/styles.php') ?>

<body>
<?php include( 'blocs/header.php') ?>
<div class="container">

    <div class="row">
        <section>
            <ol class="breadcrumb hidden-xs">
                <li><a href="index.php">Accueil</a>
                </li>
                <li><a href="#">Conseil municipal</a>
                </li>
                <li><a href="#">Comptes rendus</a>
                </li>
                <li class="active">Archives</li>
            </ol>
            <h1>Archives des comptes rendus</h1>

<!--  # Emmet code #  div.row>div.col-md-3*13>h2{20$$}+ul.list-unstyled>(li>a[href="#"]>+i.fa.fa-file-pdf-o+{ Compte rendu du mois $})*10-->

            <div class="row">
                <div class="col-md-3">
                    <h2>2001</h2>
                    <ul class="list-unstyled">
                        <li><a href="#"><i class="fa fa-file-pdf-o"></i> Compte rendu du mois 1</a></li>
                        <li><a href="#"><i class="fa fa-file-pdf-o"></i> Compte rendu du mois 2</a></li>
                        <li><a href="#"><i class="fa fa-file-pdf-o"></i> Compte rendu du mois 3</a></li>
                        <li><a href="#"><i class="fa fa-file-pdf-o"></i> Compte rendu du mois 4</a></li>
                        <li><a href="#"><i class="fa fa-file-pdf-o"></i> Compte rendu du mois 5</a></li>
                        <li><a href="#"><i class="fa fa-file-pdf-o"></i> Compte rendu du mois 6</a></li>
                        <li><a href="#"><i class="fa fa-file-pdf-o"></i> Compte rendu du mois 7</a></li>
                        <li><a href="#"><i class="fa fa-file-pdf-o"></i> Compte rendu du mois 8</a></li>
                        <li><a href="#"><i class="fa fa-file-pdf-o"></i> Compte rendu du mois 9</a></li>
                        <li><a href="#"><i class="fa fa-file-pdf-o"></i> Compte rendu du mois 10</a></li>
                    </ul>
                </div>
                <div class="col-md-3">
                    <h2>2002</h2>
                    <ul class="list-unstyled">
                        <li><a href="#"><i class="fa fa-file-pdf-o"></i> Compte rendu du mois 1</a></li>
                        <li><a href="#"><i class="fa fa-file-pdf-o"></i> Compte rendu du mois 2</a></li>
                        <li><a href="#"><i class="fa fa-file-pdf-o"></i> Compte rendu du mois 3</a></li>
                        <li><a href="#"><i class="fa fa-file-pdf-o"></i> Compte rendu du mois 4</a></li>
                        <li><a href="#"><i class="fa fa-file-pdf-o"></i> Compte rendu du mois 5</a></li>
                        <li><a href="#"><i class="fa fa-file-pdf-o"></i> Compte rendu du mois 6</a></li>
                        <li><a href="#"><i class="fa fa-file-pdf-o"></i> Compte rendu du mois 7</a></li>
                        <li><a href="#"><i class="fa fa-file-pdf-o"></i> Compte rendu du mois 8</a></li>
                        <li><a href="#"><i class="fa fa-file-pdf-o"></i> Compte rendu du mois 9</a></li>
                        <li><a href="#"><i class="fa fa-file-pdf-o"></i> Compte rendu du mois 10</a></li>
                    </ul>
                </div>
                <div class="col-md-3">
                    <h2>2003</h2>
                    <ul class="list-unstyled">
                        <li><a href="#"><i class="fa fa-file-pdf-o"></i> Compte rendu du mois 1</a></li>
                        <li><a href="#"><i class="fa fa-file-pdf-o"></i> Compte rendu du mois 2</a></li>
                        <li><a href="#"><i class="fa fa-file-pdf-o"></i> Compte rendu du mois 3</a></li>
                        <li><a href="#"><i class="fa fa-file-pdf-o"></i> Compte rendu du mois 4</a></li>
                        <li><a href="#"><i class="fa fa-file-pdf-o"></i> Compte rendu du mois 5</a></li>
                        <li><a href="#"><i class="fa fa-file-pdf-o"></i> Compte rendu du mois 6</a></li>
                        <li><a href="#"><i class="fa fa-file-pdf-o"></i> Compte rendu du mois 7</a></li>
                        <li><a href="#"><i class="fa fa-file-pdf-o"></i> Compte rendu du mois 8</a></li>
                        <li><a href="#"><i class="fa fa-file-pdf-o"></i> Compte rendu du mois 9</a></li>
                        <li><a href="#"><i class="fa fa-file-pdf-o"></i> Compte rendu du mois 10</a></li>
                    </ul>
                </div>
                <div class="col-md-3">
                    <h2>2004</h2>
                    <ul class="list-unstyled">
                        <li><a href="#"><i class="fa fa-file-pdf-o"></i> Compte rendu du mois 1</a></li>
                        <li><a href="#"><i class="fa fa-file-pdf-o"></i> Compte rendu du mois 2</a></li>
                        <li><a href="#"><i class="fa fa-file-pdf-o"></i> Compte rendu du mois 3</a></li>
                        <li><a href="#"><i class="fa fa-file-pdf-o"></i> Compte rendu du mois 4</a></li>
                        <li><a href="#"><i class="fa fa-file-pdf-o"></i> Compte rendu du mois 5</a></li>
                        <li><a href="#"><i class="fa fa-file-pdf-o"></i> Compte rendu du mois 6</a></li>
                        <li><a href="#"><i class="fa fa-file-pdf-o"></i> Compte rendu du mois 7</a></li>
                        <li><a href="#"><i class="fa fa-file-pdf-o"></i> Compte rendu du mois 8</a></li>
                        <li><a href="#"><i class="fa fa-file-pdf-o"></i> Compte rendu du mois 9</a></li>
                        <li><a href="#"><i class="fa fa-file-pdf-o"></i> Compte rendu du mois 10</a></li>
                    </ul>
                </div>
                <div class="col-md-3">
                    <h2>2005</h2>
                    <ul class="list-unstyled">
                        <li><a href="#"><i class="fa fa-file-pdf-o"></i> Compte rendu du mois 1</a></li>
                        <li><a href="#"><i class="fa fa-file-pdf-o"></i> Compte rendu du mois 2</a></li>
                        <li><a href="#"><i class="fa fa-file-pdf-o"></i> Compte rendu du mois 3</a></li>
                        <li><a href="#"><i class="fa fa-file-pdf-o"></i> Compte rendu du mois 4</a></li>
                        <li><a href="#"><i class="fa fa-file-pdf-o"></i> Compte rendu du mois 5</a></li>
                        <li><a href="#"><i class="fa fa-file-pdf-o"></i> Compte rendu du mois 6</a></li>
                        <li><a href="#"><i class="fa fa-file-pdf-o"></i> Compte rendu du mois 7</a></li>
                        <li><a href="#"><i class="fa fa-file-pdf-o"></i> Compte rendu du mois 8</a></li>
                        <li><a href="#"><i class="fa fa-file-pdf-o"></i> Compte rendu du mois 9</a></li>
                        <li><a href="#"><i class="fa fa-file-pdf-o"></i> Compte rendu du mois 10</a></li>
                    </ul>
                </div>
                <div class="col-md-3">
                    <h2>2006</h2>
                    <ul class="list-unstyled">
                        <li><a href="#"><i class="fa fa-file-pdf-o"></i> Compte rendu du mois 1</a></li>
                        <li><a href="#"><i class="fa fa-file-pdf-o"></i> Compte rendu du mois 2</a></li>
                        <li><a href="#"><i class="fa fa-file-pdf-o"></i> Compte rendu du mois 3</a></li>
                        <li><a href="#"><i class="fa fa-file-pdf-o"></i> Compte rendu du mois 4</a></li>
                        <li><a href="#"><i class="fa fa-file-pdf-o"></i> Compte rendu du mois 5</a></li>
                        <li><a href="#"><i class="fa fa-file-pdf-o"></i> Compte rendu du mois 6</a></li>
                        <li><a href="#"><i class="fa fa-file-pdf-o"></i> Compte rendu du mois 7</a></li>
                        <li><a href="#"><i class="fa fa-file-pdf-o"></i> Compte rendu du mois 8</a></li>
                        <li><a href="#"><i class="fa fa-file-pdf-o"></i> Compte rendu du mois 9</a></li>
                        <li><a href="#"><i class="fa fa-file-pdf-o"></i> Compte rendu du mois 10</a></li>
                    </ul>
                </div>
                <div class="col-md-3">
                    <h2>2007</h2>
                    <ul class="list-unstyled">
                        <li><a href="#"><i class="fa fa-file-pdf-o"></i> Compte rendu du mois 1</a></li>
                        <li><a href="#"><i class="fa fa-file-pdf-o"></i> Compte rendu du mois 2</a></li>
                        <li><a href="#"><i class="fa fa-file-pdf-o"></i> Compte rendu du mois 3</a></li>
                        <li><a href="#"><i class="fa fa-file-pdf-o"></i> Compte rendu du mois 4</a></li>
                        <li><a href="#"><i class="fa fa-file-pdf-o"></i> Compte rendu du mois 5</a></li>
                        <li><a href="#"><i class="fa fa-file-pdf-o"></i> Compte rendu du mois 6</a></li>
                        <li><a href="#"><i class="fa fa-file-pdf-o"></i> Compte rendu du mois 7</a></li>
                        <li><a href="#"><i class="fa fa-file-pdf-o"></i> Compte rendu du mois 8</a></li>
                        <li><a href="#"><i class="fa fa-file-pdf-o"></i> Compte rendu du mois 9</a></li>
                        <li><a href="#"><i class="fa fa-file-pdf-o"></i> Compte rendu du mois 10</a></li>
                    </ul>
                </div>
                <div class="col-md-3">
                    <h2>2008</h2>
                    <ul class="list-unstyled">
                        <li><a href="#"><i class="fa fa-file-pdf-o"></i> Compte rendu du mois 1</a></li>
                        <li><a href="#"><i class="fa fa-file-pdf-o"></i> Compte rendu du mois 2</a></li>
                        <li><a href="#"><i class="fa fa-file-pdf-o"></i> Compte rendu du mois 3</a></li>
                        <li><a href="#"><i class="fa fa-file-pdf-o"></i> Compte rendu du mois 4</a></li>
                        <li><a href="#"><i class="fa fa-file-pdf-o"></i> Compte rendu du mois 5</a></li>
                        <li><a href="#"><i class="fa fa-file-pdf-o"></i> Compte rendu du mois 6</a></li>
                        <li><a href="#"><i class="fa fa-file-pdf-o"></i> Compte rendu du mois 7</a></li>
                        <li><a href="#"><i class="fa fa-file-pdf-o"></i> Compte rendu du mois 8</a></li>
                        <li><a href="#"><i class="fa fa-file-pdf-o"></i> Compte rendu du mois 9</a></li>
                        <li><a href="#"><i class="fa fa-file-pdf-o"></i> Compte rendu du mois 10</a></li>
                    </ul>
                </div>
                <div class="col-md-3">
                    <h2>2009</h2>
                    <ul class="list-unstyled">
                        <li><a href="#"><i class="fa fa-file-pdf-o"></i> Compte rendu du mois 1</a></li>
                        <li><a href="#"><i class="fa fa-file-pdf-o"></i> Compte rendu du mois 2</a></li>
                        <li><a href="#"><i class="fa fa-file-pdf-o"></i> Compte rendu du mois 3</a></li>
                        <li><a href="#"><i class="fa fa-file-pdf-o"></i> Compte rendu du mois 4</a></li>
                        <li><a href="#"><i class="fa fa-file-pdf-o"></i> Compte rendu du mois 5</a></li>
                        <li><a href="#"><i class="fa fa-file-pdf-o"></i> Compte rendu du mois 6</a></li>
                        <li><a href="#"><i class="fa fa-file-pdf-o"></i> Compte rendu du mois 7</a></li>
                        <li><a href="#"><i class="fa fa-file-pdf-o"></i> Compte rendu du mois 8</a></li>
                        <li><a href="#"><i class="fa fa-file-pdf-o"></i> Compte rendu du mois 9</a></li>
                        <li><a href="#"><i class="fa fa-file-pdf-o"></i> Compte rendu du mois 10</a></li>
                    </ul>
                </div>
                <div class="col-md-3">
                    <h2>2010</h2>
                    <ul class="list-unstyled">
                        <li><a href="#"><i class="fa fa-file-pdf-o"></i> Compte rendu du mois 1</a></li>
                        <li><a href="#"><i class="fa fa-file-pdf-o"></i> Compte rendu du mois 2</a></li>
                        <li><a href="#"><i class="fa fa-file-pdf-o"></i> Compte rendu du mois 3</a></li>
                        <li><a href="#"><i class="fa fa-file-pdf-o"></i> Compte rendu du mois 4</a></li>
                        <li><a href="#"><i class="fa fa-file-pdf-o"></i> Compte rendu du mois 5</a></li>
                        <li><a href="#"><i class="fa fa-file-pdf-o"></i> Compte rendu du mois 6</a></li>
                        <li><a href="#"><i class="fa fa-file-pdf-o"></i> Compte rendu du mois 7</a></li>
                        <li><a href="#"><i class="fa fa-file-pdf-o"></i> Compte rendu du mois 8</a></li>
                        <li><a href="#"><i class="fa fa-file-pdf-o"></i> Compte rendu du mois 9</a></li>
                        <li><a href="#"><i class="fa fa-file-pdf-o"></i> Compte rendu du mois 10</a></li>
                    </ul>
                </div>
                <div class="col-md-3">
                    <h2>2011</h2>
                    <ul class="list-unstyled">
                        <li><a href="#"><i class="fa fa-file-pdf-o"></i> Compte rendu du mois 1</a></li>
                        <li><a href="#"><i class="fa fa-file-pdf-o"></i> Compte rendu du mois 2</a></li>
                        <li><a href="#"><i class="fa fa-file-pdf-o"></i> Compte rendu du mois 3</a></li>
                        <li><a href="#"><i class="fa fa-file-pdf-o"></i> Compte rendu du mois 4</a></li>
                        <li><a href="#"><i class="fa fa-file-pdf-o"></i> Compte rendu du mois 5</a></li>
                        <li><a href="#"><i class="fa fa-file-pdf-o"></i> Compte rendu du mois 6</a></li>
                        <li><a href="#"><i class="fa fa-file-pdf-o"></i> Compte rendu du mois 7</a></li>
                        <li><a href="#"><i class="fa fa-file-pdf-o"></i> Compte rendu du mois 8</a></li>
                        <li><a href="#"><i class="fa fa-file-pdf-o"></i> Compte rendu du mois 9</a></li>
                        <li><a href="#"><i class="fa fa-file-pdf-o"></i> Compte rendu du mois 10</a></li>
                    </ul>
                </div>
                <div class="col-md-3">
                    <h2>2012</h2>
                    <ul class="list-unstyled">
                        <li><a href="#"><i class="fa fa-file-pdf-o"></i> Compte rendu du mois 1</a></li>
                        <li><a href="#"><i class="fa fa-file-pdf-o"></i> Compte rendu du mois 2</a></li>
                        <li><a href="#"><i class="fa fa-file-pdf-o"></i> Compte rendu du mois 3</a></li>
                        <li><a href="#"><i class="fa fa-file-pdf-o"></i> Compte rendu du mois 4</a></li>
                        <li><a href="#"><i class="fa fa-file-pdf-o"></i> Compte rendu du mois 5</a></li>
                        <li><a href="#"><i class="fa fa-file-pdf-o"></i> Compte rendu du mois 6</a></li>
                        <li><a href="#"><i class="fa fa-file-pdf-o"></i> Compte rendu du mois 7</a></li>
                        <li><a href="#"><i class="fa fa-file-pdf-o"></i> Compte rendu du mois 8</a></li>
                        <li><a href="#"><i class="fa fa-file-pdf-o"></i> Compte rendu du mois 9</a></li>
                        <li><a href="#"><i class="fa fa-file-pdf-o"></i> Compte rendu du mois 10</a></li>
                    </ul>
                </div>
                <div class="col-md-3">
                    <h2>2013</h2>
                    <ul class="list-unstyled">
                        <li><a href="#"><i class="fa fa-file-pdf-o"></i> Compte rendu du mois 1</a></li>
                        <li><a href="#"><i class="fa fa-file-pdf-o"></i> Compte rendu du mois 2</a></li>
                        <li><a href="#"><i class="fa fa-file-pdf-o"></i> Compte rendu du mois 3</a></li>
                        <li><a href="#"><i class="fa fa-file-pdf-o"></i> Compte rendu du mois 4</a></li>
                        <li><a href="#"><i class="fa fa-file-pdf-o"></i> Compte rendu du mois 5</a></li>
                        <li><a href="#"><i class="fa fa-file-pdf-o"></i> Compte rendu du mois 6</a></li>
                        <li><a href="#"><i class="fa fa-file-pdf-o"></i> Compte rendu du mois 7</a></li>
                        <li><a href="#"><i class="fa fa-file-pdf-o"></i> Compte rendu du mois 8</a></li>
                        <li><a href="#"><i class="fa fa-file-pdf-o"></i> Compte rendu du mois 9</a></li>
                        <li><a href="#"><i class="fa fa-file-pdf-o"></i> Compte rendu du mois 10</a></li>
                    </ul>
                </div>
            </div>


        </section>
    </div>
</div>
<?php include( 'blocs/footer.php'); ?>
<?php include( 'blocs/scripts.php'); ?>
</body>

</html>



