<?php
/**
 * Created by PhpStorm.
 * User: geoffroycochard
 * Date: 21/10/2019
 * Time: 15:23
 */

function debug($datas, $die = false) {
  echo '<pre>';
  var_dump($datas);
  echo '</pre>';
  if ($die) die();
}

$curl = curl_init();

$orig = 'demarches-orleans.test.entrouvert.org';
$key = 'c97904572f0883d36e02c1ec375ec22e632d10325820fac9520d3bd0acd5b006';
$url = 'https://demarches-orleans.test.entrouvert.org/api/code/%s';
$code = 'HHBNCQTC';

/**
 * @param string $url
 * @param string $orig
 * @param string $key
 * @return string
 * @throws Exception
 */
function sign_url(string $url, string $orig, string $key) {
  $parsed_url = parse_url($url);
  $timestamp =  gmstrftime("%Y-%m-%dT%H:%M:%SZ");
  $nonce = bin2hex(random_bytes(16));
  $new_query = '';
  if (isset($parsed_url['query'])) {
    $new_query .= $parsed_url['query'] . '&';
  }
  $new_query .= http_build_query(array(
    'algo' => 'sha256',
    'timestamp' => $timestamp,
    'nonce' => $nonce,
    'orig' => $orig));
  $signature = base64_encode(hash_hmac('sha256', $new_query, $key));
  $new_query .= '&' . http_build_query(array('signature' => $signature));
  $scheme   = isset($parsed_url['scheme']) ? $parsed_url['scheme'] . '://' : '';
  $host     = isset($parsed_url['host']) ? $parsed_url['host'] : '';
  $port     = isset($parsed_url['port']) ? ':' . $parsed_url['port'] : '';
  $user     = isset($parsed_url['user']) ? $parsed_url['user'] : '';
  $pass     = isset($parsed_url['pass']) ? ':' . $parsed_url['pass']  : '';
  $pass     = ($user || $pass) ? "$pass@" : '';
  $path     = isset($parsed_url['path']) ? $parsed_url['path'] : '';
  $fragment = isset($parsed_url['fragment']) ? '#' . $parsed_url['fragment'] : '';
  return "$scheme$user$pass$host$port$path?$new_query$fragment";
}

# usage:
$url = sign_url(sprintf($url, $code), $orig, $key);
debug($url);

$headers = array(
  'Content-Type: application/json',
  'Accept: application/json'
);

curl_setopt($curl, CURLOPT_HEADER, $headers);

curl_setopt($curl, CURLOPT_URL, $url);
curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

$result = curl_exec($curl);
debug($result);
curl_close($curl);