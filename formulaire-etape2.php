<!DOCTYPE html>
<html>

<head>
<meta charset="utf-8">
<title>Le site d'Orléans et son AgglO - Formulaire</title>
<? include('blocs/styles.php') ?>

    <body>
        <? include( 'blocs/header.php') ?>
        <div class="container formulaire">
            <ol class="breadcrumb hidden-xs">
                <li><a href="index.php">Accueil</a>
                </li>
                <li><a href="#">Formulaire de contacte</a>
                </li>
                <li><a href="#">Intervention </a>
                </li>
                <li class="active">Demande d'arrêté de circulation</li>
            </ol>
            <section>
                <header>
                    <h1>Demande d'arrêté de circulation</h1>
                    <ul class="list-unstyled etape">
                        <li class="inactive"><a href="formulaire-etape1.php"><span class="step">1</span> Demandeur</a>
                        </li>
                        <br class="visible-sm visible-xs">
                        <li class="active "><span class="step">2</span> Informations sur les travaux</li>
                        <br class="visible-sm visible-xs">
                        <li class="inactive "><a href="formulaire-etape3.php"><span class="step">3</span> Mesures de circulation sollicitées</a>
                        </li>
                        <br class="visible-sm visible-xs">
                        <li class="inactive "><a href="formulaire-etape4.php"><span class="step">4</span> Validation</a>
                        </li>
                    </ul>
                </header>
                <h2 class="sr-only">Formulaire de demande - Deuxième étape</h2>
                <form class="form-horizontal" method="post" enctype="multipart/form-data" action="http://www.orleans.fr/depot-de-page/demarches-mairie/formulaire-de-contact/intervention/demande-darrete-de-circulation-stationnement-travaux-demenagement.htm" id="myForm2782">
                    <h3 class="sous-titre">Informations sur les travaux</h3>	
                    <div class="form-group">
                        <label class="control-label col-md-4">N° de dossier ville d’Orléans</label>
                        <div class="col-md-4">
                            <input class="form-control" placeholder="N° de dossier ville d’Orléans">
                        </div>
                    </div>
                    <div class="require">
                        <div class="form-group">
                            <label class="control-label col-md-4">*Lieu des travaux</label>
                            <div class="col-md-4">
                                <input class="form-control" placeholder="Lieu des travaux">
                                <p class="help-block">Indiquez ici le(s) nom(s) de rue(s) concernée(s), en précisant les limites éventuelles de tronçons, en les répétants soit par le nom des rues des intersections, ou par des adresses postales en utilisant au besoin les points cardinaux. Préciser si nécessaire avec un plan ou un croquis.</p>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4">Croquis</label>
                        <div class="col-md-4">
                            <input type="file" id="exampleInputFile">
                            <p class="help-block">(3000 Ko max,jpg,png,doc,pdf)</p>
                        </div>
                    </div>
                    <div class="require">
                        <div class="form-group">
                            <label class="control-label col-md-4">*Date de début</label>
                            <div class="col-md-4">
                                <div class="input-group date">
                                    <input class="form-control" placeholder="jj/mm/aaaa">
                                    <span class="input-group-addon">
								<span class="fa fa-calendar"></span>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="require">
                        <div class="form-group">
                            <label class="control-label col-md-4">*Date de fin</label>
                            <div class="col-md-4">
                                <div class="input-group date">
                                    <input class="form-control" placeholder="jj/mm/aaaa">
                                    <span class="input-group-addon">
								<span class="fa fa-calendar"></span>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="require hidden">
                        <div class="form-group">
                            <label class="control-label col-md-4">*Date de début</label>
                            <div class="raw">
                                <div class="col-md-1">
                                    <select class="form-control">
                                        <option value="" selected disabled>Jour</option>
                                        <option value="">1</option>
                                        <option value="">2</option>
                                        <option value="">3</option>
                                        <option value="">4</option>
                                        <option value="">5</option>
                                        <option value="">6</option>
                                        <option value="">7</option>
                                        <option value="">8</option>
                                        <option value="">9</option>
                                        <option value="">10</option>
                                        <option value="">11</option>
                                        <option value="">12</option>
                                        <option value="">13</option>
                                        <option value="">14</option>
                                        <option value="">15</option>
                                        <option value="">16</option>
                                        <option value="">17</option>
                                        <option value="">18</option>
                                        <option value="">19</option>
                                        <option value="">20</option>
                                        <option value="">21</option>
                                        <option value="">22</option>
                                        <option value="">23</option>
                                        <option value="">24</option>
                                        <option value="">25</option>
                                        <option value="">26</option>
                                        <option value="">27</option>
                                        <option value="">28</option>
                                        <option value="">29</option>
                                        <option value="">30</option>
                                        <option value="">31</option>
                                    </select>
                                </div>
                                <div class="col-md-1">
                                    <select class="form-control">
                                        <option value="" selected disabled>Mois</option>
                                        <option value="">Janvier</option>
                                        <option value="">Février</option>
                                        <option value="">Mars</option>
                                        <option value="">Avril</option>
                                        <option value="">Mai</option>
                                        <option value="">Juin</option>
                                        <option value="">Juillet</option>
                                        <option value="">Août</option>
                                        <option value="">Septembre</option>
                                        <option value="">Octobre</option>
                                        <option value="">Novembre</option>
                                        <option value="">Décembre</option>
                                    </select>
                                </div>
                                <div class="col-md-2">
                                    <select class="form-control">
                                        <option value="" selected disabled>Année</option>
                                        <option value="">2015</option>
                                        <option value="">2016</option>
                                        <option value="">2017</option>
                                    </select>
                                </div>
                            </div>
                            <p class="help-block">15 jours avant le début des travaux</p>
                        </div>
                    </div>
                    <div class="require hidden">
                        <div class="form-group">
                            <label class="control-label col-md-4">*Date de fin</label>
                            <div class="raw">
                                <div class="col-md-1">
                                    <select class="form-control">
                                        <option value="" selected disabled>Jour</option>
                                        <option value="">1</option>
                                        <option value="">2</option>
                                        <option value="">3</option>
                                        <option value="">4</option>
                                        <option value="">5</option>
                                        <option value="">6</option>
                                        <option value="">7</option>
                                        <option value="">8</option>
                                        <option value="">9</option>
                                        <option value="">10</option>
                                        <option value="">11</option>
                                        <option value="">12</option>
                                        <option value="">13</option>
                                        <option value="">14</option>
                                        <option value="">15</option>
                                        <option value="">16</option>
                                        <option value="">17</option>
                                        <option value="">18</option>
                                        <option value="">19</option>
                                        <option value="">20</option>
                                        <option value="">21</option>
                                        <option value="">22</option>
                                        <option value="">23</option>
                                        <option value="">24</option>
                                        <option value="">25</option>
                                        <option value="">26</option>
                                        <option value="">27</option>
                                        <option value="">28</option>
                                        <option value="">29</option>
                                        <option value="">30</option>
                                        <option value="">31</option>
                                    </select>
                                </div>
                                <div class="col-md-1">
                                    <select class="form-control">
                                        <option value="" selected disabled>Mois</option>
                                        <option value="">Janvier</option>
                                        <option value="">Février</option>
                                        <option value="">Mars</option>
                                        <option value="">Avril</option>
                                        <option value="">Mai</option>
                                        <option value="">Juin</option>
                                        <option value="">Juillet</option>
                                        <option value="">Août</option>
                                        <option value="">Septembre</option>
                                        <option value="">Octobre</option>
                                        <option value="">Novembre</option>
                                        <option value="">Décembre</option>
                                    </select>
                                </div>
                                <div class="col-md-2">
                                    <select class="form-control">
                                        <option value="" selected disabled>Année</option>
                                        <option value="">2015</option>
                                        <option value="">2016</option>
                                        <option value="">2017</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="require">
                        <div class="form-group">
                            <label class="control-label col-md-4">*Durée effective des travaux</label>
                            <div class="col-md-4">
                                <input class="form-control" placeholder="Durée effective des travaux">
                            </div>
                        </div>
                    </div>
                    <div class="require">
                        <div class="form-group">
                            <label class="control-label col-md-4">*Chantier nocturne</label>
                            <div class="col-md-4">
                                <div class="row">
                                    <div class="col-md-2 radio">
                                        <label>
                                            <input type="radio" value="oui" name="tx_artificaform_pi1[field2]">oui
                                        </label>
                                        <br>
                                        <label>
                                            <input type="radio" value="non" name="tx_artificaform_pi1[field2]">non
                                        </label>
                                    </div>
                                    <div class="col-md-10">
                                        <p class="help-block">entre 20h00 et 7h00</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="require">
                        <div class="form-group">
                            <label class="control-label col-md-4">*Durée effective des travaux</label>
                            <div class="col-md-4">
                                <input class="form-control" placeholder="Durée effective des travaux">
                            </div>
                        </div>
                    </div>
                    <div class="require">
                        <div class="form-group">
                            <label class="control-label col-md-4">*Nature des travaux</label>
                            <div class="raw checkbox">
                                <div class="col-md-2">
                                    <label>
                                        <input type="checkbox" value="">eau
                                    </label>
                                    <br>
                                    <label>
                                        <input type="checkbox" value="">assainissement
                                    </label>
                                    <br>
                                    <label>
                                        <input type="checkbox" value="">gaz
                                    </label>
                                    <br>
                                    <label>
                                        <input type="checkbox" value="">électricité
                                    </label>
                                    <br>
                                    <label>
                                        <input type="checkbox" value="">téléphone
                                    </label>
                                    <br>
                                    <label>
                                        <input type="checkbox" value="">réseau câblé
                                    </label>
                                    <br>
                                    <label>
                                        <input type="checkbox" value="">éclairage public
                                    </label>
                                </div>
                                <div class="col-md-2">
                                    <label>
                                        <input type="checkbox" value="">réfection de chaussée
                                    </label>
                                    <br>
                                    <label>
                                        <input type="checkbox" value="">réfection de trottoir
                                    </label>
                                    <br>
                                    <label>
                                        <input type="checkbox" value="">espaces verts
                                    </label>
                                    <br>
                                    <label>
                                        <input type="checkbox" value="">déménagement
                                    </label>
                                    <br>
                                    <label>
                                        <input type="checkbox" value="">nettoyage de vitres
                                    </label>
                                    <br>
                                    <label>
                                        <input type="checkbox" value="">autres
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4">Autres à préciser</label>
                        <div class="col-md-4">
                            <input class="form-control" placeholder="Autres">
                        </div>
                    </div>
                    <p class="text-right obligatoires">*Champs obligatoires</p>
                    <p class="text-center"><a href="formulaire-etape3.php" class="btn btn-primary"><span class="fa fa-arrow-right"></span> &Eacute;tape suivante</a>
                    </p>
                    <br>
                </form>
            </section>
        </div>
        <? include( 'blocs/footer.php'); ?>
        <? include( 'blocs/scripts.php'); ?>
    </body>

</html>



