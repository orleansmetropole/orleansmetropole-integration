<div class='to-top affix affix-bottom'><a href="#" class="btn btn-primary"><span class="fa fa-chevron-up"></span> Haut de page</a></div>
<footer>
  <div class="text-center">
  <hr  />
    <h2>Plan d'Orléans et son AgglO</h2>
    <img src="img/map.jpg" class="img-responsive visible-lg visible-md" alt="Plan de la ville">
    <p><a href="#" class="btn btn-primary"><span class="fa fa-map-marker"></span> Accéder au plan interactif</a></p>
  </div>
  <div class="container">
    <hr>
    <div class="row">
      <div class="col-md-4">
        <h3 class="underline">Mairie d'Orléans</h3>
        <p>1 place de l'Étape<br>
          45040 Orléans Cedex 1<br>
          <span class="fa fa-phone"></span> 02 38 79 22 22</p>
        <div id="content14">
          <h4>Horaires de la Mairie :</h4>
          <ul>
            <li>lundi au jeudi : 8h30 - 17h30</li>
            <li> vendredi : 8h30 - 17h00</li>
            <li>Samedi : 9h00 - 12h00 <br/>
              (uniquement les affaires administratives)</li>
          </ul>
          <br/>
          <h4>Horaires de la communauté d'agglomération :</h4>
          <ul>
            <li>lundi au vendredi : 8h00 - 18h00</li>
          </ul>
        </div>
      </div>
      <div class="col-md-8">
        <h3>Les mairies de proximité</h3>
        <div class="row">
          <div class="col-md-6">
            <ul class="list-unstyled row">
              <li class="col-sm-6">
                <h4>Nord</h4>
                <p> 11, rue Charles le chauve<br>
                  <span class="fa fa-phone"></span> 02 38 43 94 44</p>
              </li>
              <li class="col-sm-6">
                <h4>Ouest</h4>
                <p> 99, faubourg Madeleine<br>
                  <span class="fa fa-phone"></span> 02 38 72 56 13</p>
              </li>
            </ul>
            <ul class="list-unstyled row">
              <li class="col-sm-6">
                <h4>Est</h4>
                <p> 1, place Mozart<br>
                  <span class="fa fa-phone"></span> 02 38 68 43 03</p>
              </li>
              <li class="col-sm-6">
                <h4>Centre ville</h4>
                <p> 5, place de la république<br>
                  <span class="fa fa-phone"></span> 02 38 68 31 60</p>
              </li>
            </ul>
            <ul class="list-unstyled row">
              <li class="col-sm-6">
                <h4>Saint Marceau</h4>
                <p> 57, rue de la mouillère<br>
                  <span class="fa fa-phone"></span> 02 38 56 54 68</p>
              </li>
              <li class="col-sm-6">
                <h4>La Source</h4>
                <p> 4, place Choiseul<br>
                  <span class="fa fa-phone"></span> 02 38 68 44 00</p>
              </li>
            </ul>
          </div>
          <div class="col-md-6">
            <div id="content16">
              <h4>Horaires </h4>
              <div>
                <ul>
                  <li>lundi : 14h00 - 17h00</li>
                  <li> marid au vendredi : 
                    8h30 - 12h30, 14h00 - 17h00</li>
                  <li> samedi : 9h00 - 12h00</li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <hr>
    <div class="row">
      <div class="text-center text-capitalize">
        <ul class="list-inline">
          <li><a href="/contact.htm" class="title"><span class="fa fa-envelope"></span> contact</a></li>
          <li><a href="/outils-et-services/wifi-orleans.htm" class="title"><span class="fa fa-newspaper-o"></span> Espace presse</a></li>
          <li><a href="/outils-et-services/mentions-legales.htm" class="title">mentions légales</a></li>
        </ul>
      </div>
    </div>
   
  </div>
</footer>