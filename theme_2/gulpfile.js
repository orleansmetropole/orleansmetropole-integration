const { src, dest, watch, parallel, series } = require('gulp');
const less = require('gulp-less');
const rename = require('gulp-rename');
const postcss = require('gulp-postcss');
const autoprefixer = require('autoprefixer');
const cssnano = require('cssnano');
const browserSync = require('browser-sync').create();
const sourcemaps = require('gulp-sourcemaps');
const concat = require('gulp-concat');

// Static server
function serve() {

    // Serve files from the root of this project
    browserSync.init({
        proxy: "127.0.0.1:4000"
    });

    // add browserSync.reload to the tasks array to make
    // all browsers reload after tasks are complete.
    watch('less/**/*.less', series('css'));
}

function css() {
    return src('less/bootstrap.less')
        .pipe(sourcemaps.init())
        .pipe(less())
        .pipe(sourcemaps.write())
        .pipe(rename('main.css'))
        .pipe(dest('dist/css'))
        .pipe(browserSync.stream());
}

function cssProduction() {
    var plugins = [
        autoprefixer(),
        cssnano()
    ];
    return src('less/bootstrap.less')
        .pipe(less())
        .pipe(postcss(plugins))
        .pipe(rename('main.min.css'))
        .pipe(dest('dist/css'));
}

function js() {
    return src([
        'node_modules/jquery/dist/jquery.min.js',
        'node_modules/bootstrap/dist/js/bootstrap.min.js',
        // 'js/transition.js',
        // 'js/alert.js',
        // 'js/button.js',
        //'js/carousel.js',
        // 'js/collapse.js',
        // 'js/dropdown.js',
        // 'js/modal.js',
        // 'js/tooltip.js',
        // 'js/popover.js',
        // 'js/scrollspy.js',
        // 'js/tab.js',
        // 'js/affix.js',
        // 'js/ads-carousel.js',
        'js/ads-map-menu.js',
        // 'js/ads-touchswipe.min.js',
        'node_modules/blueimp-gallery/js/jquery.blueimp-gallery.min.js',
        'node_modules/blueimp-bootstrap-image-gallery/js/bootstrap-image-gallery.min.js',
        'node_modules/cookieconsent/build/cookieconsent.min.js',
        'js/ads-orleans.js'
    ], { sourcemaps: true })
        .pipe(concat('main.js'))
        .pipe(dest('dist/js', { sourcemaps: true }))
}

function watchLess() {
    return watch('less/**/*.less', series('css'));
}

exports.serve = series(serve);
exports.watch = series(watchLess);
exports.css = series(css);
exports.js = series(js);
exports.production = series(cssProduction);