<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>Le site d'Orléans et son AgglO - La Fab Lab orléanais</title>
    <?php include( 'blocs/styles.php') ?>
</head>

<body>
    <?php include( 'blocs/header.php') ?>
    <div class="container actu">
        <div class="row">
            <div class="col-md-8 col-lg-9">
                <ol class="breadcrumb hidden-xs">
                    <li><a href="index.php">Accueil</a> 
                    </li>
                    <li><a href="actu-list.php">Actualités</a> 
                    </li>
                    <li class="active">Le Fab Lab orléanais</li>
                </ol>
                <article>
                    <header class="header-article accroche">
                        <div class="row">
                            <div class="col-sm-8">
                                <h1>Le Fab Lab orléanais</h1>
                                <span class="date">Publiée le <time datetime="2015-04-09">Jeudi 9 Avril 2015</time></span> 
                            </div>
                            <div class="col-sm-4 hidden-xs">
                                <?php include( "blocs/social.php"); ?>
                            </div>
                        </div>
                        <p class="texte-accroche">Depuis le mois d'octobre, le premier Fab Lab <i>(laboratoire de fabrication)</i> du département a ouvert ses portes à La Source.</p>
                        <div class="visuel"><span class="badge">Éducation </span>
                            <figure>
                                <img src="img/actu/machine.jpg" class="img-responsive" alt="Le Fab Lab orléanais">
                                <figcaption class="sr-only">Le Fab Lab orléanais</figcaption>
                            </figure>
                        </div>
                    </header>
                    <div class="rteContent">
                        <p>Au 4e étage de l’école d’ingénieurs Polytech, à La Source, au coeur de locaux flambant neufs, de drôles de machines ont fait leur apparition depuis le mois d’octobre. Entre la salle de réunion ultra-connectée, les bureaux soigneusement équipés et les écrans d’ordinateurs alignés, une impressionnante « découpe et gravure laser » répond en rythme aux saccades des imprimantes 3D, faisant face à une fraiseuse de précision dernier cri.</p>
                        <div class="visuel">
                            <figure>
                                <img src="img/actu/benjamin.jpg" class="img-responsive" alt="Benjamin et Filipe">
                                <figcaption class="legend">Benjamin, formateur et animateur, et Filipe, directeur du FabLab, lors d'un atelier de fabrication de drônes.</figcaption>
                            </figure>
                        </div>
                        <p>Bienvenue au <a class="" target="_blank" href="http://www.fablab-orleanais.fr/">Fab Lab orléanais</a>, le premier « laboratoire de fabrication » (de l’anglais Fabrication Laboratory) du Loiret. <em>« Plus précisément, un atelier de fabrication numérique, destiné à donner les capacités à un groupe, à une communauté, d’innover avec des moyens simples »</em>, décrit Filipe Franco, ingénieur à la solide expérience dans l’industrie et les bureaux d’étude, et aujourd’hui directeur de la structure. <em>« Une plateforme dédiée à la réalisation de prototypes, qui permet de lever les freins à la créativité en proposant des services à moindre coût, des conseils et des synergies. »</em> 
                        </p>
                        <div class="visuel">
                            <figure>
                                <img src="img/actu/decoupe.jpg" class="img-responsive" alt="">
                                <figcaption class="legend">Une découpe laser dernier cri est à disposition des utilisateurs.</figcaption>
                            </figure>
                        </div>
                        <p>Aujourd’hui, par exemple, c’est Natacha, jeune designer orléanaise à la tête de sa propre marque d’accessoires de mode (And So Voilà), qui vient concrétiser le fruit de son imagination débordante. Au programme : des écrins personnalisés pour ses foulards en soie, des pièces uniques au packaging personnalisé, qui lui permettront de promouvoir sa marque. <em>« Ici, c’est mon atelier, résume-t-elle en observant la découpe laser donner vie à son dessin. Un atelier recherche et développement, où j’utilise tout autant les machines que l’expertise du directeur. » </em> 
                        </p>
                        <div class="visuel">
                            <figure>
                                <img src="img/actu/natacha.jpg" class="img-responsive" alt="">
                                <figcaption class="legend">Natacha, jeune designer, créatrice de “And So Voilà”, conçoit des écrins pour ses foulards en soie.</figcaption>
                            </figure>
                        </div>
                        <p>Dans la pièce voisine, autour d’un boîtier jaune gravé d’une abeille, c’est plutôt vers Benjamin, formateur et animateur de l’atelier électronique et informatique, qu’un autre jeune entrepreneur s’est tourné. Bertrand, ancien responsable de développement digital, est en plein lancement de Bee angels, des ruches connectées associées au label « Abeille », une solution clés en main destinée aux collectivités, associations, scolaires, apiculteurs amateurs et professionnels, pour protéger les abeilles, en voie d’extinction, tout en créant des emplois. <em>« La ruche connectée renferme des capteurs de température, de géolocalisation, de taux d’humidité, d’orientation, qui renvoient toutes ces informations à une base de données accessibles par ordinateur, smartphone ou tablette. »</em> Ainsi, il est possible de surveiller en direct la santé des essaims et de suivre leur production.</p>
                        <div class="visuel">
                            <figure>
                                <img src="img/actu/bertrand.jpg" class="img-responsive" alt="">
                                <figcaption class="legend">Bertrand et ses ruches connectées associées au label "Abeilles"</figcaption>
                            </figure>
                        </div>
                        <cite>« Avec le Fab Lab, on a démontré que le projet fonctionnait. On a matérialisé les capteurs dans un boîtier, fabriqué une ruche grâce à des plans disponibles en open source et me voilà prêt à faire des démonstrations ! » Beaucoup plus vendeur, en effet, qu’une présentation Powerpoint !</cite>
                        <p>Utile aux particuliers, la structure peut l’être tout autant aux entreprises. Gaëlle et Anne, de la société Panibois, ont poussé les portes des locaux pour construire une pièce qu’elles ont imaginée. Un cône d’empilage pour placer des pièces sur la chaîne de production, et améliorer la qualité du travail et le confort de l’opérateur de ligne.</p>
                        <div class="visuel">
                            <figure>
                                <img src="img/actu/gaelle.jpg" class="img-responsive" alt="">
                                <figcaption class="legend">Gaëlle, de la société Panibois confectionne une pièce qu'elle a imaginée pour une ligne de production.</figcaption>
                            </figure>
                        </div>
                        <cite>« Si nous avions dû le faire faire, cela nous aurait coûté beaucoup plus cher et nous aurait pris beaucoup plus de temps. Et puis en plus, là, il y a le côté ‘fait soi-même’, très valorisant. ». Ça vous plaît ? C’est moi qui l’ai fait !</cite> 
                    </div>
                    <div class="visible-xs">
                        <?php include( "blocs/social.php"); ?>
                    </div>
                </article>
            </div>
            <div class="col-md-4 col-lg-3">
                <?php include( "blocs/agenda-filter.php") ?>
            </div>
        </div>
    </div>
    <?php include( 'blocs/footer.php'); ?>
    <?php include( 'blocs/scripts.php'); ?>
</body>

</html>
