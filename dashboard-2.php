<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>Le site d'Orléans et son AgglO - Accueil</title>
    <?php include( 'blocs/styles.php') ?>

    <body>
        <?php include( 'blocs/header-dashboard.php') ?>
        <div class="container dashboard">
            <section>
                <div class="row">
                    <div class="col-lg-9">
                        <ol class="breadcrumb hidden-xs">
                            <li><a href="#">Compte citoyen</a>
                            </li>
                            <li class="active">Mon dossier</li>
                        </ol>
                        <div class="header-article">
                            <h1>Compte citoyen Orléans</h1>
                            <span class="date">Bonjour M. HEZARD Sébastien</span>
                        </div>
                        <div class="accroche">
                            <p class="texte-accroche">Vous êtes actuellement rélié au <b>dossier famille n°23929</b>. Si vous désirez ajouter une information ou corriger une erreur, utilisez les <a href="#"><u>téléservices prévus pour les modifications</u></a>.</p>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <div class="pull-left"><h3 class="facture">Factures en cours <span class="highlight">(3)</span></h3></div>
                                <div class="panel-actions pull-right">
                                    <h4><a href="#" class=""><span class="visible-xs">+</span><span class="hidden-xs">Toutes les factures</span></a></h4>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="panel-body">
                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Montant facture</th>
                                            <th>Restant à payer</th>
                                            <th>&Eacute;mise le</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td><a href="#">1502020933</a>
                                            </td>
                                            <td>134.56 €</td>
                                            <td><b>134.56 €</b>
                                            </td>
                                            <td>5 mai 2015</td>
                                            <td>
                                                <a href="#" class="payer"> Voir et payer</a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><a href="#">1502020932</a>
                                            </td>
                                            <td>110.99 €</td>
                                            <td><b>85.54 €</b>
                                            </td>
                                            <td>2 mai 2015</td>
                                            <td>
                                                <a href="#" class="payer"> Voir et payer</a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><a href="#">1502020931</a>
                                            </td>
                                            <td>34.56 €</td>
                                            <td><b>34.56 €</b>
                                            </td>
                                            <td>1 mai 2015</td>
                                            <td>
                                                <a href="#" class="payer"> Voir et payer</a>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="panel panel-default ">
                            <div class="panel-heading">
                                <ul class="nav nav-pills" id="myTab">
                                    <li role="presentation" class="active"><a data-toggle="tab" href="#encours"><h3 class="facture">Demandes en cours</h3></a>
                                    </li>
                                    <li role="presentation"><a data-toggle="tab" href="#cloturee"><h3 class="facture">Demandes clôturées</h3></a>
                                    </li>
                                </ul>
                                <div class="panel-actions pull-right">
                                    <h4><a href="#" class=""><span class="visible-xs">+</span><span class="hidden-xs">Toutes les demandes</span></a></h4>
                                </div>
                            </div>
                            <div class="panel-body">
                                <div class="tab-content" id="myTabContent">
                                    <div id="encours" class="tab-pane fade in active">
                                        <h3>Demandes en cours</h3>
                                    </div>
                                    <div id="cloturee" class="tab-pane fade in">
                                        <h3>Demandes cloturées</h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <nav class="quicklink text-center">
                            <h2 class="sr-only">Accès rapides</h2>
                            <div class="well dashboard">
                                <ul class="list-unstyled row ">
                                    <li class="col-xs-6 col-sm-4 col-lg-6"> <a href="" class="btn btn-default "><span class="fa fa-euro rounded"></span><br>
										Mes<br class="hidden-sm">
										factures</a>
                                    </li>
                                    <li class="col-xs-6 col-sm-4 col-lg-6"> <a href="" class="btn btn-default"><span class="fa fa-envelope rounded"></span><br>
										Nous<br class="hidden-sm">
										contacter</a>
                                    </li>
                                    <li class="col-xs-12 col-sm-4 col-lg-12">
                                        <div class="dropup">
                                            <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuCompte" data-toggle="dropdown" aria-expanded="true">
                                                <span class="fa  fa-user inverse rounded"></span>
                                                <br>Mon compte
                                            </button>
                                            <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenuCompte">
                                                <li role="presentation"><a role="menuitem" tabindex="-1" href="#"><span class="fa  fa-pencil"></span> Modifier mes informations</a>
                                                </li>
                                                <li role="presentation"><a role="menuitem" tabindex="-1" href="#"><span class="fa  fa-close"></span> Se déconnecter</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </nav>
                        <div class="well sidebar">
                            <div class="row entete">
                                <h3>Les responsables</h3>
                            </div>
                            <ul class="list-unstyled">
                                <li>
                                    <h3>Catherine BENCHOUA</h3>
                                    <p class="adresse"> <strong>Téléphone</strong>
                                        <br>06 83 18 46 85 <i>(port.)</i>
                                        <br>01 45 49 72 93 <i>(prof.)</i>
                                    </p>
                                    <p class="adresse">Allocataire n°1316424, CAF du Loiret, Régime Général</p>
                                    <h3>Stéphane BENCHOUA</h3>
                                    <p class="adresse"> <strong>Téléphone</strong>
                                        <br>06 84 80 17 24 <i>(port.)</i>
                                    </p>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </section>
        </div>
        <?php include( 'blocs/footer.php'); ?>
        <?php include( 'blocs/scripts.php'); ?>
    </body>

</html>
