<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>Le site d'Orléans et son AgglO - Page satellite</title>
    <?php include( 'blocs/styles.php') ?>

    <body>
        <?php include( 'blocs/header-satellite.php') ?>
        <div class="container satellite">
            <img src="img/jazz-eveche.jpg" class="img-responsive couverture" alt="jazz">
            <div class="row">
                <section>
                    <div class="col-lg-9">
                        <div id="carousel-home" class="carousel carousel-home slide carousel-fade" data-ride="carousel">
                            <h1 class="sr-only">A la une</h1>
                            <!-- Indicators -->
                            <ol class="carousel-indicators">
                                <li data-target="#carousel-home" data-slide-to="0" class="active"></li>
                                <li data-target="#carousel-home" data-slide-to="1"></li>
                                <li data-target="#carousel-home" data-slide-to="2"></li>
                            </ol>
                            <!-- Wrapper for slides -->
                            <div class="carousel-inner">
                                <section class="item active">
                                    <div class="bg"></div>
                                    <a href="#">
                                        <figure>
                                            <img src="img/jazz2.jpg" class="img-responsive" alt="Orléans Jazz">
                                            <figcaption class="sr-only">Orléans Jazz</figcaption>
                                        </figure>
                                    </a>
                                    <div class="carousel-caption"> <span class="badge">Culture</span>
                                        <h3> <a href="#">Orléans Jazz</a></h3>
                                        <p>Jazz à l'évêché : 4 journées de concerts gratuits !</p>
                                    </div>
                                </section>
                                <section class="item">
                                    <div class="bg"></div>
                                    <a href="#">
                                        <figure>
                                            <img src="img/ecole.jpg" class="img-responsive" alt="Ecole">
                                            <figcaption class="sr-only">Année scolaire 2015-2016 : inscriptions</figcaption>
                                        </figure>
                                    </a>
                                    <div class="carousel-caption"> <span class="badge">Éducation</span>
                                        <h3><a href="#">Année scolaire 2015-2016 : inscriptions</a></h3>
                                        <p>Inscriptions du lundi 12 janvier au vendredi 6 mars 2015</p>
                                    </div>
                                </section>
                                <section class="item">
                                    <div class="bg"></div>
                                    <a href="#">
                                        <figure>
                                            <img src="img/festival-de-loire.jpg" class="img-responsive" alt="Festival de Loire">
                                            <figcaption class="sr-only">Festival de Loire 2015 : cap à l’Est !</figcaption>
                                        </figure>
                                    </a>
                                    <div class="carousel-caption"> <span class="badge">Festival de Loire</span>
                                        <h3><a href="#">Festival de Loire 2015 : cap à l’Est !</a></h3>
                                        <p>Le Festival de Loire vous fera découvrir les 23 au 27 septembre la Pologne.</p>
                                    </div>
                                </section>
                            </div>
                            <!-- Controls -->
                            <a class="left carousel-control" href="#carousel-home" role="button" data-slide="prev"> <span class="fa fa-chevron-left"></span> 
                            </a>
                            <a class="right carousel-control" href="#carousel-home" role="button" data-slide="next"> <span class="fa fa-chevron-right"></span> 
                            </a>
                        </div>
                    </div>
                    <aside class="col-lg-3">
                        <div class="well sidebar">
                            <div class="row entete">
                                <h3>Infos Pratiques</h3>
                            </div>
                            <ul class="list-unstyled">
                                <li>
                                    <p class="adresse"> <strong>Adresse</strong>
                                        <br>2, Place de l'Etape 45000 Orléans</p>
                                    <p class="adresse"> <strong>Téléphone</strong>
                                        <br>02 38 24 05 05</p>
                                    <p><a href="http://www.tourisme-orleans.com" target="blanck"><strong><span class="fa fa-link"></span> Tourisme Orléans</strong></a>
                                    </p>
                                    <div class="embed-responsive embed-responsive-4by3">
                                        <iframe class="embed-responsive-item" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d2674.159147779366!2d1.903644!3d47.91395800000001!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47e4fb28e6f11bbf%3A0xf3466a6788c59944!2sAds-com!5e0!3m2!1sfr!2sfr!4v1428439739506">
                                        </iframe>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </aside>
                </section>
            </div>
            <section>
            <h1>Prochains concerts</h1>
            <div class="row liste-item">
                <article class="col-sm-6 col-md-4">
                    <div class="visuel">
                        <a href="#">
                            <img class="img-responsive" src="img/satelitte/51shots.jpg" alt="">
                        </a>
                    </div>
                    <div class="caption sat">
                        <h3><a href="#">51 Shots</a></h3>
                        <span class="date">Le <time datetime="">Jeudi 18 Juin 2015 - 19h00</time></span>
                    </div>
                </article>
                <article class="col-sm-6 col-md-4">
                    <div class="visuel">
                        <a href="#">
                            <figure>
                                <img class="img-responsive" src="img/satelitte/andre.jpg" alt="">
                                <figcaption class="sr-only">André Manoukian 4tet</figcaption>
                            </figure>
                        </a>
                    </div>
                    <div class="caption sat">
                        <h3><a href="#">André Manoukian 4tet</a></h3>
                        <span class="date">Le <time datetime="">Jeudi 18 Juin 2015 - 20h30</time></span>
                    </div>
                </article>
                <article class="col-sm-6 col-md-4">
                    <div class="visuel">
                        <a href="#">
                            <figure>
                                <img class="img-responsive" src="img/satelitte/jazz.jpg" alt="">
                                <figcaption class="sr-only">Ateliers Jazz de Baptiste Dubreuil</figcaption>
                            </figure>
                        </a>
                    </div>
                    <div class="caption sat">
                        <h3><a href="#">Ateliers Jazz de Baptiste Dubreuil</a></h3>
                        <span class="date">Le <time datetime="">Jeudi 18 Juin 2015 - 22h00</time></span>
                    </div>
                </article>
                <article class="col-sm-6 col-md-4">
                    <div class="visuel">
                        <a href="#">
                            <figure>
                                <img class="img-responsive" src="img/satelitte/jazz.jpg" alt="">
                                <figcaption class="sr-only">« Ca va Jazzer »</figcaption>
                            </figure>
                        </a>
                    </div>
                    <div class="caption sat">
                        <h3><a href="#">« Ca va Jazzer »</a></h3>
                        <span class="date">Le <time datetime="">Vendredi 19 Juin 2015 - 22h00</time></span>
                    </div>
                </article>
                <article class="col-sm-6 col-md-4">
                    <div class="visuel">
                        <a href="#">
                            <figure>
                                <img class="img-responsive" src="img/satelitte/celesta.jpg" alt="">
                                <figcaption class="sr-only">Orphéon Célesta « La Préhistoire du Jazz »</figcaption>
                            </figure>
                        </a>
                    </div>
                    <div class="caption sat">
                        <h3><a href="#">Orphéon Célesta « La Préhistoire du Jazz »</a></h3>
                        <span class="date">Le <time datetime="">Vendredi 19 Juin 2015 - 12h305</span>
                    </div>
                </article>
                <article class="col-sm-6 col-md-4">
                    <div class="visuel">
                        <a href="#">
                            <figure>
                                <img class="img-responsive" src="img/satelitte/meloblast.jpg" alt="">
                                <figcaption class="sr-only">Méloblast</figcaption>
                            </figure>
                        </a>
                    </div>
                    <div class="caption sat">
                        <h3><a href="#">Méloblast</a></h3>
                        <span class="date">Le <time datetime="">Vendredi 19 Juin 2015 - 19h00</time></span>
                    </div>
                </article>
                <article class="col-sm-6 col-md-4">
                    <div class="visuel">
                        <a href="#">
                            <figure>
                                <img class="img-responsive" src="img/satelitte/cuisine.jpg" alt="">
                                <figcaption class="sr-only">Orphéon Célesta « Cuisine au Jazz »</figcaption>
                            </figure>
                        </a>
                    </div>
                    <div class="caption sat">
                        <h3><a href="#">Orphéon Célesta « Cuisine au Jazz »</a></h3>
                        <span class="date">Le <time datetime="">Vendredi 19 Juin 2015 - 20h30</time></span>
                    </div>
                </article>
                <article class="col-sm-6 col-md-4">
                    <div class="visuel">
                        <a href="#">
                            <figure>
                                <img class="img-responsive" src="img/satelitte/clarinology.jpg" alt="">
                                <figcaption class="sr-only">Clarinology jazz ensemble</figcaption>
                            </figure> 
                        </a>
                    </div>
                    <div class="caption sat">
                        <h3><a href="#">Clarinology jazz ensemble</a></h3>
                        <span class="date">Le <time datetime="">Samedi 20 Juin 2015 - 12h30</time></span>
                    </div>
                </article>
                <article class="col-sm-6 col-md-4">
                    <div class="visuel">
                        <a href="#">
                            <figure>
                                <img class="img-responsive" src="img/satelitte/fd.jpg" alt="">
                                <figcaption class="sr-only">FD Parcours Libre</figcaption>
                            </figure> 
                        </a>
                    </div>
                    <div class="caption sat">
                        <h3><a href="#">FD Parcours Libre</a></h3>
                        <span class="date">Le <time datetime="">Samedi 20 Juin 2015 - 19h00</time></span>
                    </div>
                </article>
            </div>
			<nav class="text-center">
				<?php include( "blocs/pagination.php") ?>
			</nav>
       </section>
        </div>
        </div>
        <?php include( 'blocs/footer.php'); ?>
        <?php include( 'blocs/scripts.php'); ?>
    </body>

</html>
