<!DOCTYPE html>
<html>

<head>
<meta charset="utf-8">
<title>Le site d'Orléans et son AgglO - Formulaire</title>
<? include('blocs/styles.php') ?>

    <body>
        <? include( 'blocs/header.php') ?>
        <div class="container formulaire">
            <ol class="breadcrumb hidden-xs">
                <li><a href="index.php">Accueil</a>
                </li>
                <li><a href="#">Formulaire de contacte</a>
                </li>
                <li><a href="#">Intervention </a>
                </li>
                <li class="active">Demande d'arrêté de circulation</li>
            </ol>
            <section>
                <header>
                    <h1>Demande d'arrêté de circulation</h1>
                    <ul class="list-inline list-unstyled etape">
                        <li class="inactive"><a href="formulaire-etape1.php"><span class="step">1</span> Demandeur</a>
                        </li>
                        <br class="visible-sm visible-xs">
                        <li class="inactive "><a href="formulaire-etape2.php"><span class="step">2</span> Informations sur les travaux</a>
                        </li>
                        <br class="visible-sm visible-xs">
                        <li class="inactive "><a href="formulaire-etape3.php"><span class="step">3</span> Mesures de circulation sollicitées</a>
                        </li>
                        <br class="visible-sm visible-xs">
                        <li class="active "><span class="step">4</span> Validation</li>
                    </ul>
                </header>
                <h2 class="sr-only">Formulaire de demande - Quatrième étape</h2>
                <form class="form-horizontal" method="post">
                    <h3 class="sous-titre">Validation</h3>
                    <p class="obligatoires">Les demandes sont à adresser au minimum 15 jours ouvrables avant l'intervention.</p>
                    <p class="text-center">Pour valider le formulaire, saisissez les <strong>4 derniers caractères</strong> de la série.</p>
                    <ul class="list-inline list-unstyled text-center">
                        <li>Q</li>
                        <li>N</li>
                        <li>L</li>
                        <li>K</li>
                        <li>6</li>
                        <li>M</li>
                        <li>X</li>
                        <li>S</li>
                    </ul>
                    <div class="require">
                        <div class="form-group">
                            <label class="control-label col-md-4">*La clé de validation</label>
                            <div class="col-md-4">
                                <input class="form-control" placeholder="La clé de validation">
                            </div>
                        </div>
                    </div>
                    <p class="text-right obligatoires">*Champs obligatoires</p>
                    <p class="text-center"><a href="#" class="btn btn-primary btn-lg"><span class="fa fa-send"></span> Valider</a>
                    </p>
                    <br>
                </form>
            </section>
        </div>
        <? include( 'blocs/footer.php'); ?>
        <? include( 'blocs/scripts.php'); ?>
    </body>

</html>



