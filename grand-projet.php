<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <title>Le site d'Orléans et son AgglO - Liste d'événements</title>
  <?php include( 'blocs/styles.php') ?>
  </style>
  
</head>

<body>
    <?php include( 'blocs/header.php') ?>


	<div class="container">



		<ol class="breadcrumb hidden-xs">


			<li><a href="http://preprod.orleans.fr/2/accueil.htm">orleans-agglo.fr</a></li>



			<li><a href="http://preprod.orleans.fr/1435/le-labo.htm">Le Lab'O</a></li>


		</ol>



		<header class="accroche header-article">
			<div class="row">
				<div class="col-sm-8">

					<h1>Le Lab'O</h1>

				</div>
				<div class="col-sm-4 hidden-xs">
					<div class="dropdown-social pull-right hidden-xs">
						<button class="share dropdown-toggle" type="button" id="dropdownMenuSocial1" data-toggle="dropdown" aria-expanded="true">
							Partager
							<span class="fa fa-share-alt btn btn-primary rounded-sm"></span>
						</button>
						<ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenuSocial1">
							<li><a href="http://www.facebook.com/share.php?u="><span class="fa fa-facebook"></span> Facebook</a></li>
							<li><a href="https://twitter.com/share?url="><span class="fa fa-twitter"></span> Twitter</a></li>
							<li><a href="http://www.linkedin.com/shareArticle?mini=true&amp;url="><span class="fa fa-linkedin"></span> LinkedIn</a></li>
							<li><a href="https://plus.google.com/share?url="><span class="fa fa-google"></span> Google +</a></li>
						</ul>
					</div>
					<div class="social-xs text-center visible-xs">
						<ul class="list-inline list-unstyled">
							<li><a href="http://www.facebook.com/share.php?u="><span class="fa fa-facebook btn btn-primary rounded"></span></a></li>
							<li><a href="https://twitter.com/share?url="><span class="fa fa-twitter btn btn-primary rounded"></span></a></li>
							<li><a href="http://www.linkedin.com/shareArticle?mini=true&amp;url="><span class="fa fa-linkedin btn btn-primary rounded"></span></a></li>
							<li><a href="https://plus.google.com/share?url="><span class="fa fa-google btn btn-primary rounded"></span></a></li>
						</ul>
					</div>
				</div>
			</div>


			<p class="texte-accroche">L’incubateur orléanais de la French Tech Loire Valley accueille ces premières startups au printemps. Son rôle&nbsp;: faciliter la croissance de ces jeunes pousses du numérique, jusqu’à leur envol.</p>



		</header>
		<div id="c3980" class="csc-space-after-100">



			<div id="carousel-3980" class="carousel slide carousel-zoom" data-interval="5000" data-wrap="1" data-ride="carousel" style="touch-action: pan-y; -webkit-user-select: none; -webkit-user-drag: none; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
				<div class="carousel-inner" role="listbox">



					<div class="item carousel-item-type carousel-item-type-textandimage" data-itemno="0" style="




                        background-color: #333333;
                    ">





						<div class="valign">
							<div class="carousel-caption" style="color: #FFFFFF;">
								<h3>Ouverture en avril 2016</h3>
								<p>Les deux incubateurs numériques de la French Tech Loire Valley&nbsp;: le Lab’O, à Orléans, et Mame, à Tours, ouvriront simultanément en avril 2016.</p>
							</div>
							<div class="carousel-image vcontainer">



								<img class="img-responsive" src="http://preprod.orleans.fr/fileadmin/_processed_/csm_labo-accueil_8e9a4855cf.jpg" width="1140" height="600" alt="">



							</div>
						</div>





					</div>



					<div class="item carousel-item-type carousel-item-type-textandimage" data-itemno="1" style="




                        background-color: #333333;
                    ">





						<div class="valign">
							<div class="carousel-caption" style="color: #FFFFFF;">
								<h3>Convention avec Orange</h3>
								<p>Le 28 janvier 2016, signature d’une convention de partenariat entre l’AgglO Orléans Val de Loire, Tour(s)plus et Orange, au Fablab d’Orléans. Objectif&nbsp;: contribuer à l’émergence et au développement de startups locales.</p>
							</div>
							<div class="carousel-image vcontainer">



								<img class="img-responsive" src="http://preprod.orleans.fr/fileadmin/_processed_/csm_partenariat-orange003_3bf767e3b5.jpg" width="1140" height="600" alt="">



							</div>
						</div>





					</div>



					<div class="item carousel-item-type carousel-item-type-textandimage active" data-itemno="2" style="



                                background-image: url('/fileadmin/orleans/MEDIA/editorial/economie_emploi/french-tech/french-tech-vegas.jpg');
                                filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='./fileadmin/orleans/MEDIA/editorial/economie_emploi/french-tech/french-tech-vegas.jpg',
                                sizingMethod='scale');
                                -ms-filter:
                                &quot;progid:DXImageTransform.Microsoft.AlphaImageLoader(src='/fileadmin/orleans/MEDIA/editorial/economie_emploi/french-tech/french-tech-vegas.jpg',
                                sizingMethod='scale')&quot;;



                        background-color: #333333;
                    ">





						<div class="valign">
							<div class="carousel-caption" style="color: #FFFFFF;">
								<h3>À nous Las Vegas&nbsp;!</h3>
								<p>Cinq entreprises ont affiché les couleurs de la French Tech Loire Valley au Consumer Electronics Show (CES) de Las Vegas, du 6 au 9 janvier 2016&nbsp;: Design Screen HD, My Serious Game, Zefal, Avidsen et Altyor.</p>
							</div>
							<div class="carousel-image vcontainer">



								<img class="img-responsive" src="http://preprod.orleans.fr/fileadmin/_processed_/csm_french-tech-vegas_0c62f4dc3e.jpg" width="1140" height="600" alt="">



							</div>
						</div>





					</div>



					<div class="item carousel-item-type carousel-item-type-textandimage" data-itemno="3" style="




                        background-color: #333333;
                    ">





						<div class="valign">
							<div class="carousel-caption" style="color: #FFFFFF;">
								<h3>French Tech Loire Valley, territoire d'innovation</h3>
								<p>La secrétaire d’Etat au Numérique, Axelle Lemaire prend connaissance du projet French Tech Loire Valley en juin 2015. Les deux grandes villes de la Vallée de la Loire veulent inciter les créateurs à partir à l’assaut des nouveaux horizons de l’économie digitale.</p>
							</div>
							<div class="carousel-image vcontainer">



								<img class="img-responsive" src="http://preprod.orleans.fr/fileadmin/_processed_/csm_visite-ministre003_01_a97eb3253d.jpg" width="1140" height="600" alt="">



							</div>
						</div>





					</div>

				</div>

				<ol class="carousel-indicators">

					<li data-target="#carousel-3980" data-slide-to="0" class=""></li>

					<li data-target="#carousel-3980" data-slide-to="1" class=""></li>

					<li data-target="#carousel-3980" data-slide-to="2" class="active"></li>

					<li data-target="#carousel-3980" data-slide-to="3" class=""></li>

				</ol>
				<a data-slide="prev" role="button" class="left carousel-control" href="#carousel-3980">
					<span class="glyphicon glyphicon-chevron-left"></span>
					<span class="sr-only">Previous</span>
				</a>
				<a data-slide="next" role="button" class="right carousel-control" href="#carousel-3980">
					<span class="glyphicon glyphicon-chevron-right"></span>
					<span class="sr-only">Next</span>
				</a>

			</div>


		</div><div id="c3981" class="csc-space-after-55"><div class="row"><div class="  col-md-6  "><div id="c3998"><div class="texticon texticon-top"><div class="texticon-icon texticon-size-default texticon-type-circle"><span class="glyphicon glyphicon-tree-deciduous" style="color: #FFFFFF;background-color: #1f3f4b;"></span></div><div class="texticon-content"><h2 class="text-center first-headline">Un écosystème fertile</h2><p>«&nbsp;Lieu totem&nbsp;» de la French Tech Loire Valley - avec le site Mame, de Tours -, le Lab’O a pris ses quartiers en bordure de Loire, à une enjambée du centre-ville d’Orléans.</p></div></div></div><div id="c4015"><p class="text-center"><a href="http://preprod.orleans.fr/?id=1435#4004" class="btn btn-warning" data-toggle="modal" data-target="#modal-ecosystem">Lire la suite...</a></p>

						<div class="modal fade" tabindex="-1" role="dialog" id="modal-ecosystem">
							<div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
										<h2 class="modal-title">Un écosystème fertile</h2>
									</div>
									<div class="modal-body">
										<p>L’incubateur (ou couveuse d’entreprise) vise à créer un écosystème fertile, propice à l’entreprenariat, au développement d’entreprises innovantes, générateur de richesses et d’emplois pour le territoire. Cet écosystème dispose, d’ailleurs, d’autres points d’ancrage comme le centre de recherche Helios du groupe LVMH, le Datacenter d’Hitachi, le FabLab, et bientôt, le quartier d’affaires Interives et AgreenTech Valley (vallée numérique du végétal).</p>
										<h3>Quelque chose qui « pulse » </h3>
										<p>Son originalité réside aussi dans le fonctionnement, collaboratif mais respectueux de la logique d’entreprise. <cite>« L’idée est de créer une communauté réunissant des entrepreneurs individuels et des sociétés de croissance, mais aussi des associations et de la formation dans le domaine du numérique, explique Olivier Carré, député-maire d’Orléans. Quelque chose qui « pulse », un lieu de création et de ressources pour tous ceux qui s’y installeront et pour l’ensemble des entreprises du territoire. »</cite></p>


									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
									</div>
								</div>
							</div>
						</div></div></div><div class="  col-md-6  "><div id="c3988"><div class="texticon texticon-top"><div class="texticon-icon texticon-size-default texticon-type-circle"><span class="glyphicon glyphicon-ok" style="color: #FFFFFF;background-color: #1f3f4b;"></span></div><div class="texticon-content"><h2 class="text-center first-headline">5 axes d’excellence</h2><p>Le numérique est appelé à dynamiser les écosystèmes des filières existantes sur le territoire, entendez par là, les secteurs d’excellence qui comptent des sociétés bien assises et exportatrices&nbsp;</p></div></div></div><div id="c4016"><p class="text-center"><a href="http://preprod.orleans.fr/?id=1435#4006" class="btn btn-warning" data-toggle="modal" data-target="#modal-excellence">Lire la suite...</a></p>

						<div class="modal fade" tabindex="-1" role="dialog" id="modal-excellence">
							<div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
										<h2 class="modal-title">5 axes d’excellence</h2>
									</div>
									<div class="modal-body">

										<p>Pour le territoire, il s’agit de :</p>
										<ul><li>végétal et agriculture</li>
											<li>logistique et e-commerce
											</li><li>éco-technologie
											</li><li>objets connectés
											</li><li>santé et beauté</li></ul>
										<p>Le cercle vertueux promu par la French Tech et favorisé au sein du Lab’O consiste à connecter des jeunes pousses fourmillant d’idées à des entreprises du même territoire en quête de nouveaux débouchés pour, à l’arrivée, valoriser et exporter le savoir-faire à l’international.</p>


									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
									</div>
								</div>
							</div>
						</div></div></div></div></div><div id="c3999" class="csc-space-after-55"><div class="row"><div class="  col-md-6  "><div id="c3982"><div class="texticon texticon-top"><div class="texticon-icon texticon-size-default texticon-type-circle"><span class="glyphicon glyphicon-map-marker" style="color: #FFFFFF;background-color: #1f3f4b;"></span></div><div class="texticon-content"><h2 class="text-center first-headline">Un lieu totem</h2><p>L’incubateur numérique porte le nom de Lab’O, en référence au passé du site, autrefois occupé par Sandoz puis Famar, et plus largement à l’histoire industrielle orléanaise marquée par la présence d’entreprises pharmaceutiques</p></div></div></div><div id="c4017"><p class="text-center"><a href="http://preprod.orleans.fr/?id=1435#4006" class="btn btn-warning" data-toggle="modal" data-target="#modal-totem">Lire la suite...</a></p>

						<div class="modal fade" tabindex="-1" role="dialog" id="modal-totem">
							<div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
										<h2 class="modal-title">Lieu totem</h2>
									</div>
									<div class="modal-body">

										<p>Aménagé par l’Agglomération Orléans Val de Loire, le site sera en capacité d’accueillir d’ici à 5 ans une centaine d’entreprises liées au numérique et à l’innovation. Les premières – sélection sur dossier et devant jury - vont s’installer au printemps. L’articulation de l’ancienne usine pharmaceutique repose sur le bâtiment historique Tschumi (8 000 m2) et d’un 2e bâtiment à vocation industrielle (1 000 m2). Le reste a été démoli pour paysager le site et créer des zones de stationnement résidents/visiteurs.</p>
										<h3>Ouvert et collaboratif</h3>
										<p>L’aménagement du bâtiment Tschumi mise sur l’ouverture. Aucun obstacle ne doit stopper ou refroidir le bouillonnement des idées, ni le fonctionnement en mode collaboratif.</p>
										<ul><li>Au rez-de-chaussée : large hall d’accueil, espaces showroom-animation et co-working, ainsi qu’une école de code ouverte à tous
											</li><li>1er étage : espaces de travail collaboratifs pour les startups, bureaux et salles de réunion adaptables
											</li><li>2e étage : salles de réunion et espace détente
											</li><li>3e étage : salle de 100 m2, configurée pour accueillir conférences, cocktails…, avec vue imprenable sur la Loire et la Cathédrale.
											</li></ul>
										<p>Le tout, bien sûr, équipé des outils (visioconférence…) et réseaux (Wi-Fi…) nécessaires au bon fonctionnement.</p>


									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
									</div>
								</div>
							</div>
						</div></div></div><div class="  col-md-6  "><div id="c3983"><div class="texticon texticon-top"><div class="texticon-icon texticon-size-default texticon-type-circle"><span class="glyphicon glyphicon-resize-small" style="color: #FFFFFF;background-color: #1f3f4b;"></span></div><div class="texticon-content"><h2 class="text-center first-headline">Des services sur mesure</h2><p>L’agencement du site s’accompagne d’une offre de services adaptée aux besoins des jeunes pousses, de l’accompagnement à l’incubation, du développement à l’accélération</p></div></div></div><div id="c4018"><p class="text-center"><a href="http://preprod.orleans.fr/?id=1435#4007" class="btn btn-warning" data-toggle="modal" data-target="#modal-services">Lire la suite...</a></p>

						<div class="modal fade" tabindex="-1" role="dialog" id="modal-services">
							<div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
										<h2 class="modal-title">Services sur mesure</h2>
									</div>
									<div class="modal-body">

										<p>L’objectif est de sécuriser le plus possible leur parcours et de favoriser les échanges entre elles. Le catalogue de services a été élaboré et testé au préalable par les acteurs de l’écosystème. Il propose, en accès libre ou payant, des ateliers pour croiser les idées, de l’accompagnement juridique, des conférences, un espace showroom, de la formation… </p>
										<h3>Fonds d’investissement unique</h3>
										<p>Pour répondre à la problématique du financement, la French Tech Loire Valley mise sur un fonds d’investissement régional chargé de délivrer des aides aux entreprises ayant besoin de fonds de roulement. <cite>« Ce fonds d’amorçage peut permettre, avec l’appui des collectivités, de créer un effet de levier, explique Olivier Carré, député-maire d’Orléans, pour obtenir les financements qui existent mais que peu de « villes de province » arrivent à mobiliser. »</cite></p>


									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
									</div>
								</div>
							</div>
						</div></div></div></div></div><div id="c3993"><div class="image-center image-above"><div class="image-wrap"><div class="image-center-outer"><div class="image-center-inner"><div class="image-row"><div class="image-column col-md-4 col-sm-4 col-xs-4"><figure class="image">                <img src="http://preprod.orleans.fr/fileadmin/_processed_/csm_labo002_d496c7c939.jpg" class="newLazyload img-responsive" bigger="/fileadmin/_processed_/csm_labo002_fdf7e501df.jpg" large="/fileadmin/_processed_/csm_labo002_005155678f.jpg" medium="/fileadmin/_processed_/csm_labo002_cca85fb920.jpg" small="/fileadmin/_processed_/csm_labo002_5fd9ab2a9b.jpg" alt="" title="Le Labo, incubateur numérique à Orléans" style="opacity: 1;">
										<noscript>
											&lt;img src="http://preprod.orleans.fr/fileadmin/_processed_/csm_labo002_d496c7c939.jpg" alt="" title="Le Labo, incubateur numérique à Orléans" /&gt;
										</noscript></figure></div>
								<div class="image-column col-md-4 col-sm-4 col-xs-4"><figure class="image">                <img src="http://preprod.orleans.fr/fileadmin/_processed_/csm_french-tech002_822c4a4134.jpg" class="newLazyload img-responsive" bigger="/fileadmin/_processed_/csm_french-tech002_631a275baf.jpg" large="/fileadmin/_processed_/csm_french-tech002_20d0a85d76.jpg" medium="/fileadmin/_processed_/csm_french-tech002_3f5ee2aeb1.jpg" small="/fileadmin/_processed_/csm_french-tech002_00f967cef1.jpg" alt="" title="Le Labo, incubateur numérique à Orléans" style="opacity: 1;">
										<noscript>
											&lt;img src="http://preprod.orleans.fr/fileadmin/_processed_/csm_french-tech002_822c4a4134.jpg" alt="" title="Le Labo, incubateur numérique à Orléans" /&gt;
										</noscript></figure></div>
								<div class="image-column col-md-4 col-sm-4 col-xs-4"><figure class="image">                <img src="http://preprod.orleans.fr/fileadmin/_processed_/csm_labo001_9b7a3483c1.jpg" class="newLazyload img-responsive" bigger="/fileadmin/_processed_/csm_labo001_0b17ffbb52.jpg" large="/fileadmin/_processed_/csm_labo001_e8325ee956.jpg" medium="/fileadmin/_processed_/csm_labo001_73f75cf139.jpg" small="/fileadmin/_processed_/csm_labo001_97e996d90c.jpg" alt="" title="Le Labo, incubateur numérique à Orléans" style="opacity: 1;">
										<noscript>
											&lt;img src="http://preprod.orleans.fr/fileadmin/_processed_/csm_labo001_9b7a3483c1.jpg" alt="" title="Le Labo, incubateur numérique à Orléans" /&gt;
										</noscript></figure></div></div>
							<div class="image-row"><div class="image-column col-md-4 col-sm-4 col-xs-4"><figure class="image">                <img src="http://preprod.orleans.fr/fileadmin/_processed_/csm_visite-labo_1bcd10b34d.jpg" class="newLazyload img-responsive" bigger="/fileadmin/_processed_/csm_visite-labo_0291449908.jpg" large="/fileadmin/_processed_/csm_visite-labo_8ab8aec5cf.jpg" medium="/fileadmin/_processed_/csm_visite-labo_77316add0e.jpg" small="/fileadmin/_processed_/csm_visite-labo_86df7fe5fe.jpg" alt="" title="Le Labo, incubateur numérique à Orléans, visite des habitants" style="opacity: 1;">
										<noscript>
											&lt;img src="http://preprod.orleans.fr/fileadmin/_processed_/csm_visite-labo_1bcd10b34d.jpg" alt="" title="Le Labo, incubateur numérique à Orléans, visite des habitants" /&gt;
										</noscript></figure></div>
								<div class="image-column col-md-4 col-sm-4 col-xs-4"><figure class="image">                <img src="http://preprod.orleans.fr/fileadmin/_processed_/csm_french-tech-barcamp_26daa28a1f.jpg" class="newLazyload img-responsive" bigger="/fileadmin/_processed_/csm_french-tech-barcamp_adbdcde4b1.jpg" large="/fileadmin/_processed_/csm_french-tech-barcamp_46a94f2ba5.jpg" medium="/fileadmin/_processed_/csm_french-tech-barcamp_20945ac45e.jpg" small="/fileadmin/_processed_/csm_french-tech-barcamp_dee71db47b.jpg" alt="" title="Barcamp French Tech Loire Valley à Orléans" style="opacity: 1;">
										<noscript>
											&lt;img src="http://preprod.orleans.fr/fileadmin/_processed_/csm_french-tech-barcamp_26daa28a1f.jpg" alt="" title="Barcamp French Tech Loire Valley à Orléans" /&gt;
										</noscript></figure></div>
								<div class="image-column col-md-4 col-sm-4 col-xs-4"><figure class="image">                <img src="http://preprod.orleans.fr/fileadmin/_processed_/csm_labo003_9a6fd79e72.jpg" class="newLazyload img-responsive" bigger="/fileadmin/_processed_/csm_labo003_2b29a7501f.jpg" large="/fileadmin/_processed_/csm_labo003_56d76a54f7.jpg" medium="/fileadmin/_processed_/csm_labo003_0313d9d883.jpg" small="/fileadmin/_processed_/csm_labo003_e630619f08.jpg" alt="" title="Le Labo, incubateur numérique à Orléans" style="opacity: 1;">
										<noscript>
											&lt;img src="http://preprod.orleans.fr/fileadmin/_processed_/csm_labo003_9a6fd79e72.jpg" alt="" title="Le Labo, incubateur numérique à Orléans" /&gt;
										</noscript></figure></div></div></div></div></div></div></div><div id="c4000"><h2>FRENCH TECH LOIRE VALLEY,  <small>un tremplin numérique</small></h2></div><aside class="block-special"><h2></h2><div class="row"><div class="  col-md-6  "><p>En amont comme en aval de la Loire, un écosystème diversifié s’est organisé et développé, riche d’incubateurs comme le Lab’O (Orléans) et Mame (Tours), d’un tissu fourni de PME innovantes, d’entreprises leaders dans leur secteur et exportatrices, de laboratoires de recherche, de formations supérieures et d’événements. Avec un objectif ambitieux&nbsp;: ressourcer l’économie traditionnelle et permettre aux startups du numérique d’émerger au plan international.
					</p>
					<p>Le positionnement stratégique de la French Tech Loire Valley est unique en son genre en France&nbsp;: devenir un «&nbsp;industry lab&nbsp;», le territoire-test des innovations grandeur nature et des expérimentations d’usages numériques nouveaux.</p><p>Avec un vivier numérique de 3 400 entreprises, 6 domaines d’excellence allant de l’agriculture 3.0 aux objets connectés, une mobilisation sans précédent de tous les acteurs de l’innovation, la French Tech Loire Valley offre un potentiel de développement pour le numérique en France. Les agglomérations d’Orléans et de Tours ont choisi de croiser leurs complémentarités et de porter ensemble le projet «&nbsp;French Tech Loire Valley&nbsp;».&nbsp;</p><ul><li><a href="http://frenchtech-loirevalley.com/" target="_blank">Site officiel</a></li><li><a href="http://preprod.orleans.fr/http:///351-4963/actualite/amenagement-du-labo.htm?&amp;no_cache=1">Actualité</a></li></ul></div><div class="  col-md-6  "><div id="c4009"><div class="image-center image-above csc-textpic-border"><div class="image-wrap"><div class="image-center-outer"><div class="image-center-inner"><figure class="thumbnail">                <img src="http://preprod.orleans.fr/fileadmin/_processed_/csm_logo-french-tech-loire-valley_01_009628c349.jpg" class="newLazyload img-responsive" bigger="/fileadmin/_processed_/csm_logo-french-tech-loire-valley_01_8721b287e7.jpg" large="/fileadmin/_processed_/csm_logo-french-tech-loire-valley_01_6bd98cc22a.jpg" medium="/fileadmin/_processed_/csm_logo-french-tech-loire-valley_01_dd3efc57e7.jpg" small="/fileadmin/_processed_/csm_logo-french-tech-loire-valley_01_e4a431ce1b.jpg" alt="" title="FRENCH TECH LOIRE VALLEY" style="opacity: 1;">
											<noscript>
												&lt;img src="http://preprod.orleans.fr/fileadmin/_processed_/csm_logo-french-tech-loire-valley_01_009628c349.jpg" alt="" title="FRENCH TECH LOIRE VALLEY" /&gt;
											</noscript></figure></div></div></div></div></div></div></div></aside><div id="c4019"><h2>LES RENDEZ-VOUS DU NUMÉRIQUE&nbsp;EN 2016</h2>


			<div class="panel-group" id="accordion-4019" role="tablist" aria-multiselectable="true">

				<div class="panel panel-default">
					<div class="panel-heading" role="tab">
						<h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion-4019" href="#accordion-4019-8">
								Global Service Jam
							</a>
						</h4>
					</div>
					<div id="accordion-4019-8" class="panel-collapse collapse in" role="tabpanel">
						<div class="panel-body">
							<p>Le plus grand événement de design de services au monde, organisé dans plus d’une centaine de villes simultanément.</p><ul><li>Du 27 février au 1er mars, au 108, à Orléans</li> <li><a href="http://www.orleansservicejam.fr" target="_blank">www.orleansservicejam.fr</a></li></ul>
						</div>
					</div>
				</div>

				<div class="panel panel-default">
					<div class="panel-heading" role="tab">
						<h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion-4019" href="#accordion-4019-9">
								Start-up week-end
							</a>
						</h4>
					</div>
					<div id="accordion-4019-9" class="panel-collapse collapse" role="tabpanel">
						<div class="panel-body">
							<p>ou comment créer sa startup en 54h&nbsp;!</p><ul><li>Du 20 au 22 mai, à Orléans</li> <li><a href="http://www.orleans.startupweekend.org" target="_blank">www.orleans.startupweekend.org</a></li></ul><ul><li>Aux 3e et 4e trimestres, à Tours</li> <li><a href="http://www.tours.startupweekend.org" target="_blank">www.tours.startupweekend.org</a></li></ul>
						</div>
					</div>
				</div>

				<div class="panel panel-default">
					<div class="panel-heading" role="tab">
						<h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion-4019" href="#accordion-4019-10">
								Dreamhack
							</a>
						</h4>
					</div>
					<div id="accordion-4019-10" class="panel-collapse collapse" role="tabpanel">
						<div class="panel-body">
							<p>Le plus grand festival digital du monde, doublé cette année d’un DreamHackaton, une session de programmation informatique collaborative</p><ul><li>Du 14 au 16 mai, à Tours</li> <li>www.dreamhack.fr</li></ul>
						</div>
					</div>
				</div>

				<div class="panel panel-default">
					<div class="panel-heading" role="tab">
						<h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion-4019" href="#accordion-4019-11">
								Hackathon citoyen
							</a>
						</h4>
					</div>
					<div id="accordion-4019-11" class="panel-collapse collapse" role="tabpanel">
						<div class="panel-body">
							<p>Organisé par Palo Altours avec Coopaxis et 8 associations.<br>2e semestre 2016, à Tours
							</p>
							<p><a href="http://hackathon-citoyen.org/" target="_blank">http://hackathon-citoyen.org</a>
							</p>
							<p>&nbsp;</p>
							<p>&nbsp;</p>
						</div>
					</div>
				</div>

				<div class="panel panel-default">
					<div class="panel-heading" role="tab">
						<h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion-4019" href="#accordion-4019-12">
								Conférence Tedx
							</a>
						</h4>
					</div>
					<div id="accordion-4019-12" class="panel-collapse collapse" role="tabpanel">
						<div class="panel-body">
							<p>"Technology, entertainement, design" visent à diffuser des idées méritant d’être partagées et ayant pour but de “changer” le monde</p><ul><li>Le 3 juin à Tours et le 17 juin à Orléans</li> <li><a href="http://www.tedxorleans.com" target="_blank">www.tedxorleans.com</a> </li></ul>
						</div>
					</div>
				</div>

			</div>


		</div>
	</div>


    <?php include( 'blocs/footer.php'); ?>
    <?php include( 'blocs/scripts.php'); ?>

</body>

</html>

