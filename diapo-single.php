<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <title>Le site d'Orléans et son AgglO - Liste d'événements</title>
  <?php include( 'blocs/styles.php') ?>
</head>

<body>
    <?php include( 'blocs/header.php') ?>
    <div class="container diapo">
        <div class="row">
            <div class="col-md-8 col-lg-9">
                <ol class="breadcrumb hidden-xs">
                    <li><a href="index.php">Accueil</a>
                    </li>
                    <li><a href="#">Vidéos</a>
                    </li>
                    <li><a href="diapo-list.php">Photos</a>
                    </li>
                    <li class="active">Hommage à Jean Zay</li>
                </ol>
                <article>
                    <header class="header-article accroche">
                        <div class="row ">
                            <div class="col-sm-8">
                                <h1>Hommage à Jean Zay</h1>
                                <span class="date">Publiée le Jeudi 9 Avril 2015</span>
                            </div>
                            <div class="col-sm-4 hidden-xs">
                                <?php include( "blocs/social.php"); ?>
                            </div>
                        </div>
                        <p class="texte-accroche diapo">Une cérémonie solennelle d’hommage est organisée le 18 mai 2015. Une marche républicaine, avec des étapes sur des sites symboliques, a ainsi été imaginée. Un temps de rassemblement prenant et intense auquel chacun a été convié.</p>
                    </header>


                    <div class="wrapSpecial">
                        <div class="gallery">

                            <h2 class="contentTitle">retro en images</h2>



                            <ul class="list-unstyled row">


                                <li class="list-1 col-sm-6 col-md-4 col-lg-3">


                                    <a href="http://preprod.orleans.fr/fileadmin/orleans/MEDIA/diaporama/jane-2015/IMG_5114.JPG" title="journée d'accueil des nouveaux étudiants  - © A Lasnier" data-toggle="modal" data-target="#myModal0" class="thumbnail" data-gallery="">
                                        <figure>
                                            <img class="img-responsive" alt="journée d'accueil des nouveaux étudiants" src="http://preprod.orleans.fr/fileadmin/_processed_/csm_IMG_5114_2236dae2e8.jpg" width="264" height="176" title="journée d'accueil des nouveaux étudiants">
                                            <figcaption class="sr-only">A Lasnier</figcaption>
                                        </figure>
                                    </a>


                                </li>



                                <li class="list-2 col-sm-6 col-md-4 col-lg-3">


                                    <a href="http://preprod.orleans.fr/fileadmin/orleans/MEDIA/diaporama/jane-2015/IMG_5137.JPG" title="journée d'accueil des nouveaux étudiants  - © A Lasnier" data-toggle="modal" data-target="#myModal1" class="thumbnail" data-gallery="">
                                        <figure>
                                            <img class="img-responsive" alt="journée d'accueil des nouveaux étudiants" src="http://preprod.orleans.fr/fileadmin/_processed_/csm_IMG_5137_50ab67a946.jpg" width="264" height="176" title="journée d'accueil des nouveaux étudiants">
                                            <figcaption class="sr-only">A Lasnier</figcaption>
                                        </figure>
                                    </a>


                                </li>



                                <li class="list-3 col-sm-6 col-md-4 col-lg-3">


                                    <a href="http://preprod.orleans.fr/fileadmin/orleans/MEDIA/diaporama/jane-2015/IMG_5157.JPG" title="journée d'accueil des nouveaux étudiants  - © A Lasnier" data-toggle="modal" data-target="#myModal2" class="thumbnail" data-gallery="">
                                        <figure>
                                            <img class="img-responsive" alt="journée d'accueil des nouveaux étudiants" src="http://preprod.orleans.fr/fileadmin/_processed_/csm_IMG_5157_6a56dbb914.jpg" width="264" height="176" title="journée d'accueil des nouveaux étudiants">
                                            <figcaption class="sr-only">A Lasnier</figcaption>
                                        </figure>
                                    </a>


                                </li>



                                <li class="list-1 col-sm-6 col-md-4 col-lg-3">


                                    <a href="http://preprod.orleans.fr/fileadmin/orleans/MEDIA/diaporama/jane-2015/IMG_5185.JPG" title="journée d'accueil des nouveaux étudiants -  Soufiane SANKHON adjoint au sport et Youssoufi TOURÉ, président de l'université  - © A Lasnier" data-toggle="modal" data-target="#myModal3" class="thumbnail" data-gallery="">
                                        <figure>
                                            <img class="img-responsive" alt="journée d'accueil des nouveaux étudiants -  Soufiane SANKHON adjoint au sport et Youssoufi TOURÉ, président de l'université" src="http://preprod.orleans.fr/fileadmin/_processed_/csm_IMG_5185_8df8e18ad7.jpg" width="264" height="176" title="journée d'accueil des nouveaux étudiants -  Soufiane SANKHON adjoint au sport et Youssoufi TOURÉ, président de l'université">
                                            <figcaption class="sr-only">A Lasnier</figcaption>
                                        </figure>
                                    </a>


                                </li>



                                <li class="list-2 col-sm-6 col-md-4 col-lg-3">


                                    <a href="http://preprod.orleans.fr/fileadmin/orleans/MEDIA/diaporama/jane-2015/IMG_5223.JPG" title="journée d'accueil des nouveaux étudiants  - © A Lasnier" data-toggle="modal" data-target="#myModal4" class="thumbnail" data-gallery="">
                                        <figure>
                                            <img class="img-responsive" alt="journée d'accueil des nouveaux étudiants" src="http://preprod.orleans.fr/fileadmin/_processed_/csm_IMG_5223_f446fd192a.jpg" width="264" height="176" title="journée d'accueil des nouveaux étudiants">
                                            <figcaption class="sr-only">A Lasnier</figcaption>
                                        </figure>
                                    </a>


                                </li>



                                <li class="list-3 col-sm-6 col-md-4 col-lg-3">


                                    <a href="http://preprod.orleans.fr/fileadmin/orleans/MEDIA/diaporama/jane-2015/IMG_5245.JPG" title="journée d'accueil des nouveaux étudiants  - © A Lasnier" data-toggle="modal" data-target="#myModal5" class="thumbnail" data-gallery="">
                                        <figure>
                                            <img class="img-responsive" alt="journée d'accueil des nouveaux étudiants" src="http://preprod.orleans.fr/fileadmin/_processed_/csm_IMG_5245_ec0ba01a6b.jpg" width="264" height="176" title="journée d'accueil des nouveaux étudiants">
                                            <figcaption class="sr-only">A Lasnier</figcaption>
                                        </figure>
                                    </a>


                                </li>



                                <li class="list-1 col-sm-6 col-md-4 col-lg-3">


                                    <a href="http://preprod.orleans.fr/fileadmin/orleans/MEDIA/diaporama/jane-2015/IMG_5257.JPG" title="journée d'accueil des nouveaux étudiants  - © A Lasnier" data-toggle="modal" data-target="#myModal6" class="thumbnail" data-gallery="">
                                        <figure>
                                            <img class="img-responsive" alt="journée d'accueil des nouveaux étudiants" src="http://preprod.orleans.fr/fileadmin/_processed_/csm_IMG_5257_e13648181f.jpg" width="264" height="176" title="journée d'accueil des nouveaux étudiants">
                                            <figcaption class="sr-only">A Lasnier</figcaption>
                                        </figure>
                                    </a>


                                </li>



                                <li class="list-2 col-sm-6 col-md-4 col-lg-3">


                                    <a href="http://preprod.orleans.fr/fileadmin/orleans/MEDIA/diaporama/jane-2015/IMG_7845.JPG" title="journée d'accueil des nouveaux étudiants  - © A Lasnier" data-toggle="modal" data-target="#myModal7" class="thumbnail" data-gallery="">
                                        <figure>
                                            <img class="img-responsive" alt="journée d'accueil des nouveaux étudiants" src="http://preprod.orleans.fr/fileadmin/_processed_/csm_IMG_7845_981db4259f.jpg" width="264" height="176" title="journée d'accueil des nouveaux étudiants">
                                            <figcaption class="sr-only">A Lasnier</figcaption>
                                        </figure>
                                    </a>


                                </li>



                                <li class="list-3 col-sm-6 col-md-4 col-lg-3">


                                    <a href="http://preprod.orleans.fr/fileadmin/orleans/MEDIA/diaporama/jane-2015/IMG_7854.JPG" title="journée d'accueil des nouveaux étudiants  - © A Lasnier" data-toggle="modal" data-target="#myModal8" class="thumbnail" data-gallery="">
                                        <figure>
                                            <img class="img-responsive" alt="journée d'accueil des nouveaux étudiants" src="http://preprod.orleans.fr/fileadmin/_processed_/csm_IMG_7854_6af6b2dc4e.jpg" width="264" height="176" title="journée d'accueil des nouveaux étudiants">
                                            <figcaption class="sr-only">A Lasnier</figcaption>
                                        </figure>
                                    </a>


                                </li>



                                <li class="list-1 col-sm-6 col-md-4 col-lg-3">


                                    <a href="http://preprod.orleans.fr/fileadmin/orleans/MEDIA/diaporama/jane-2015/IMG_7874.JPG" title="journée d'accueil des nouveaux étudiants  - © A Lasnier" data-toggle="modal" data-target="#myModal9" class="thumbnail" data-gallery="">
                                        <figure>
                                            <img class="img-responsive" alt="journée d'accueil des nouveaux étudiants" src="http://preprod.orleans.fr/fileadmin/_processed_/csm_IMG_7874_4cab0633ed.jpg" width="264" height="176" title="journée d'accueil des nouveaux étudiants">
                                            <figcaption class="sr-only">A Lasnier</figcaption>
                                        </figure>
                                    </a>


                                </li>



                                <li class="list-2 col-sm-6 col-md-4 col-lg-3">


                                    <a href="http://preprod.orleans.fr/fileadmin/orleans/MEDIA/diaporama/jane-2015/IMG_7897.JPG" title="journée d'accueil des nouveaux étudiants  - © A Lasnier" data-toggle="modal" data-target="#myModal10" class="thumbnail" data-gallery="">
                                        <figure>
                                            <img class="img-responsive" alt="journée d'accueil des nouveaux étudiants" src="http://preprod.orleans.fr/fileadmin/_processed_/csm_IMG_7897_44155a2a6f.jpg" width="264" height="176" title="journée d'accueil des nouveaux étudiants">
                                            <figcaption class="sr-only">A Lasnier</figcaption>
                                        </figure>
                                    </a>


                                </li>



                                <li class="list-3 col-sm-6 col-md-4 col-lg-3">


                                    <a href="http://preprod.orleans.fr/fileadmin/orleans/MEDIA/diaporama/jane-2015/IMG_7903.JPG" title="journée d'accueil des nouveaux étudiants  - © A Lasnier" data-toggle="modal" data-target="#myModal11" class="thumbnail" data-gallery="">
                                        <figure>
                                            <img class="img-responsive" alt="journée d'accueil des nouveaux étudiants" src="http://preprod.orleans.fr/fileadmin/_processed_/csm_IMG_7903_f09213efc7.jpg" width="264" height="176" title="journée d'accueil des nouveaux étudiants">
                                            <figcaption class="sr-only">A Lasnier</figcaption>
                                        </figure>
                                    </a>


                                </li>



                                <li class="list-1 col-sm-6 col-md-4 col-lg-3">


                                    <a href="http://preprod.orleans.fr/fileadmin/orleans/MEDIA/diaporama/jane-2015/IMG_7918.JPG" title="journée d'accueil des nouveaux étudiants  - © A Lasnier" data-toggle="modal" data-target="#myModal12" class="thumbnail" data-gallery="">
                                        <figure>
                                            <img class="img-responsive" alt="journée d'accueil des nouveaux étudiants" src="http://preprod.orleans.fr/fileadmin/_processed_/csm_IMG_7918_b49b33b0c1.jpg" width="264" height="176" title="journée d'accueil des nouveaux étudiants">
                                            <figcaption class="sr-only">A Lasnier</figcaption>
                                        </figure>
                                    </a>


                                </li>



                                <li class="list-2 col-sm-6 col-md-4 col-lg-3">


                                    <a href="http://preprod.orleans.fr/fileadmin/orleans/MEDIA/diaporama/jane-2015/IMG_7945.JPG" title="journée d'accueil des nouveaux étudiants  - © A Lasnier" data-toggle="modal" data-target="#myModal13" class="thumbnail" data-gallery="">
                                        <figure>
                                            <img class="img-responsive" alt="journée d'accueil des nouveaux étudiants" src="http://preprod.orleans.fr/fileadmin/_processed_/csm_IMG_7945_fe7796261f.jpg" width="264" height="176" title="journée d'accueil des nouveaux étudiants">
                                            <figcaption class="sr-only">A Lasnier</figcaption>
                                        </figure>
                                    </a>


                                </li>



                                <li class="list-3 col-sm-6 col-md-4 col-lg-3">


                                    <a href="http://preprod.orleans.fr/fileadmin/orleans/MEDIA/diaporama/jane-2015/IMG_8006.jpg" title="journée d'accueil des nouveaux étudiants  - © A Lasnier" data-toggle="modal" data-target="#myModal14" class="thumbnail" data-gallery="">
                                        <figure>
                                            <img class="img-responsive" alt="journée d'accueil des nouveaux étudiants" src="http://preprod.orleans.fr/fileadmin/_processed_/csm_IMG_8006_04608cbc77.jpg" width="264" height="176" title="journée d'accueil des nouveaux étudiants">
                                            <figcaption class="sr-only">A Lasnier</figcaption>
                                        </figure>
                                    </a>


                                </li>



                                <li class="list-1 col-sm-6 col-md-4 col-lg-3">


                                    <a href="http://preprod.orleans.fr/fileadmin/orleans/MEDIA/diaporama/jane-2015/IMG_8035.JPG" title="journée d'accueil des nouveaux étudiants  - © A Lasnier" data-toggle="modal" data-target="#myModal15" class="thumbnail" data-gallery="">
                                        <figure>
                                            <img class="img-responsive" alt="journée d'accueil des nouveaux étudiants" src="http://preprod.orleans.fr/fileadmin/_processed_/csm_IMG_8035_dfba3cdedb.jpg" width="264" height="176" title="journée d'accueil des nouveaux étudiants">
                                            <figcaption class="sr-only">A Lasnier</figcaption>
                                        </figure>
                                    </a>


                                </li>


                            </ul>

                        </div>
                    </div>


                    <div class="row">
                        <div class="col-sm-6 col-md-4">
                            <a href="#" data-toggle="modal" data-target="#myModal1" class="thumbnail">
                                <img src="http://www.orleans.fr/fileadmin/orleans/MEDIA/diaporama/jean_zay_hommage/IMG_5030.JPG" alt="">
                            </a>
                        </div>
                        <div class="col-sm-6 col-md-4">
                            <a href="#" data-toggle="modal" data-target="#myModal2" class="thumbnail">
                                <img src="http://www.orleans.fr/fileadmin/orleans/MEDIA/diaporama/jean_zay_hommage/IMG_5045.jpg" alt="">
                            </a>
                        </div>
                        <div class="col-sm-6 col-md-4">
                            <a href="#" data-toggle="modal" data-target="#myModal3" class="thumbnail">
                                <img src="http://www.orleans.fr/fileadmin/orleans/MEDIA/diaporama/jean_zay_hommage/IMG_5059.jpg" alt="">
                            </a>
                        </div>
                        <div class="col-sm-6 col-md-4">
                            <a href="#" data-toggle="modal" data-target="#myModal4" class="thumbnail">
                                <img src="http://www.orleans.fr/fileadmin/orleans/MEDIA/diaporama/jean_zay_hommage/IMG_5097.jpg" alt="">
                            </a>
                        </div>
                        <div class="col-sm-6 col-md-4">
                            <a href="#" data-toggle="modal" data-target="#myModal5" class="thumbnail">
                                <img src="http://www.orleans.fr/fileadmin/orleans/MEDIA/diaporama/jean_zay_hommage/IMG_5168.jpg" alt="">
                            </a>
                        </div>
                        <div class="col-sm-6 col-md-4">
                            <a href="#" data-toggle="modal" data-target="#myModal6" class="thumbnail">
                                <img src="http://www.orleans.fr/fileadmin/orleans/MEDIA/diaporama/jean_zay_hommage/IMG_5102.jpg" alt="">
                            </a>
                        </div>
                    </div>
                    <div class="modal fade" id="myModal1" tabindex="-1">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button><h2 class="modal-title">Titre du diaporama</h2>
                                </div>
                                <div class="modal-body">
                                    <figure>
                                        <img src="http://www.orleans.fr/fileadmin/orleans/MEDIA/diaporama/jean_zay_hommage/IMG_5030.JPG" class="img-responsive" alt="">
                                        <figcaption class="sr-only">Hommage à Jean Zay</figcaption>
                                    </figure>
                                </div>
                                <div class="modal-footer">
                                        <div class="col-xs-2 text-left"><a href="#" title="Précédent" class="btn btn-primary rounded"><span class="fa fa-chevron-left"></span></a></div>
                                        <div class="col-xs-8">
                                            <p class="text-center">Hommage à Jean Zay</p>
                                        </div>
                                        <div class="col-xs-2 text-right"><a href="#" title="Suivant" class="btn btn-primary rounded"><span class="fa fa-chevron-right"></span></a></div>
                                    </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal fade" id="myModal2" tabindex="-1">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button><h2 class="modal-title">Titre du diaporama</h2>
                                </div>
                                <div class="modal-body">
                                    <figure>
                                        <img src="http://www.orleans.fr/fileadmin/orleans/MEDIA/diaporama/jean_zay_hommage/IMG_5045.jpg" class="img-responsive" alt="">
                                        <figcaption class="sr-only">Hommage à Jean Zay</figcaption>
                                    </figure>
                                </div>
                                <div class="modal-footer">
                                        <div class="col-xs-2 text-left"><a href="#" title="Précédent" class="btn btn-primary rounded"><span class="fa fa-chevron-left"></span></a></div>
                                        <div class="col-xs-8">
                                            <p class="text-center">Hommage à Jean Zay</p>
                                        </div>
                                        <div class="col-xs-2 text-right"><a href="#" title="Suivant" class="btn btn-primary rounded"><span class="fa fa-chevron-right"></span></a></div>
                                    </div>
                                </div>
                        </div>
                    </div>
                    <div class="modal fade" id="myModal3" tabindex="-1">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button><h2 class="modal-title">Titre du diaporama</h2>
                                </div>
                                <div class="modal-body">
                                    <figure>
                                        <img src="http://www.orleans.fr/fileadmin/orleans/MEDIA/diaporama/jean_zay_hommage/IMG_5059.jpg" class="img-responsive" alt="">
                                        <figcaption class="sr-only">Hommage à Jean Zay</figcaption>
                                    </figure>
                                </div>
                                <div class="modal-footer">
                                    <div class="row">
                                        <div class="col-xs-2 text-left"><a href="#" title="Précédent" class="btn btn-primary rounded"><span class="fa fa-chevron-left"></span></a></div>
                                        <div class="col-xs-8">
                                            <p class="text-center">Hommage à Jean Zay</p>
                                        </div>
                                        <div class="col-xs-2 text-right"><a href="#" title="Suivant" class="btn btn-primary rounded"><span class="fa fa-chevron-right"></span></a></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal fade" id="myModal4" tabindex="-1">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button><h2 class="modal-title">Titre du diaporama</h2>
                                </div>
                                <div class="modal-body">
                                    <figure>
                                        <img src="http://www.orleans.fr/fileadmin/orleans/MEDIA/diaporama/jean_zay_hommage/IMG_5097.jpg" class="img-responsive" alt="">
                                        <figcaption class="sr-only">Hommage à Jean Zay</figcaption>
                                    </figure>
                                </div>
                                <div class="modal-footer">
                                    <div class="row">
                                        <div class="col-xs-2 text-left"><a href="#" title="Précédent" class="btn btn-primary rounded"><span class="fa fa-chevron-left"></span></a></div>
                                        <div class="col-xs-8">
                                            <p class="text-center">Hommage à Jean Zay</p>
                                        </div>
                                        <div class="col-xs-2 text-right"><a href="#" title="Suivant" class="btn btn-primary rounded"><span class="fa fa-chevron-right"></span></a></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal fade" id="myModal5" tabindex="-1">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button><h2 class="modal-title">Titre du diaporama</h2>
                                </div>
                                <div class="modal-body">
                                    <figure>
                                        <img src="http://www.orleans.fr/fileadmin/orleans/MEDIA/diaporama/jean_zay_hommage/IMG_5059.jpg" class="img-responsive" alt="">
                                        <figcaption class="sr-only">Hommage à Jean Zay</figcaption>
                                    </figure>
                                </div>
                                <div class="modal-footer">
                                    <div class="row">
                                        <div class="col-xs-2 text-left"><a href="#" title="Précédent" class="btn btn-primary rounded"><span class="fa fa-chevron-left"></span></a></div>
                                        <div class="col-xs-8">
                                            <p class="text-center">Hommage à Jean Zay</p>
                                        </div>
                                        <div class="col-xs-2 text-right"><a href="#" title="Suivant" class="btn btn-primary rounded"><span class="fa fa-chevron-right"></span></a></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal fade" id="myModal6" tabindex="-1">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button><h2 class="modal-title">Titre du diaporama</h2>
                                </div>
                                <div class="modal-body">
                                    <figure>
                                        <img src="http://www.orleans.fr/fileadmin/orleans/MEDIA/diaporama/jean_zay_hommage/IMG_5102.jpg" class="img-responsive" alt="">
                                        <figcaption class="sr-only">Hommage à Jean Zay</figcaption>
                                    </figure>
                                </div>
                                <div class="modal-footer">
                                    <div class="row">
                                        <div class="col-xs-2 text-left"><a href="#" title="Précédent" class="btn btn-primary rounded"><span class="fa fa-chevron-left"></span></a></div>
                                        <div class="col-xs-8">
                                            <p class="text-center">Hommage à Jean Zay</p>
                                        </div>
                                        <div class="col-xs-2 text-right"><a href="#" title="Suivant" class="btn btn-primary rounded"><span class="fa fa-chevron-right"></span></a></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal fade" id="myModal7" tabindex="-1">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button><h2 class="modal-title">Titre du diaporama</h2>
                                </div>
                                <div class="modal-body">
                                    <figure>
                                        <img src="http://www.orleans.fr/fileadmin/orleans/MEDIA/diaporama/jean_zay_hommage/IMG_5261.JPG" class="img-responsive" alt="">
                                        <figcaption class="sr-only">Hommage à Jean Zay</figcaption>
                                    </figure>
                                </div>
                                <div class="modal-footer">
                                    <div class="row">
                                        <div class="col-xs-2 text-left"><a href="#" title="Précédent" class="btn btn-primary rounded"><span class="fa fa-chevron-left"></span></a>
                                        <div class="col-xs-8">
                                            <p class="text-center">Hommage à Jean Zay</p>
                                        </div>
                                        <div class="col-xs-2 text-right"><a href="#" title="Suivant" class="btn btn-primary rounded"><span class="fa fa-chevron-right"></span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal fade" id="myModal8" tabindex="-1">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button><h2 class="modal-title">Titre du diaporama</h2>
                                </div>
                                <div class="modal-body">
                                    <figure>
                                        <img src="http://www.orleans.fr/fileadmin/orleans/MEDIA/diaporama/jean_zay_hommage/IMG_5299.jpg" class="img-responsive" alt="">
                                        <figcaption class="sr-only">Hommage à Jean Zay</figcaption>
                                    </figure>
                                </div>
                                <div class="modal-footer">
                                    <div class="row">
                                        <div class="col-xs-2 text-left"><a href="#" title="Précédent" class="btn btn-primary rounded"><span class="fa fa-chevron-left"></span></a>
                                        <div class="col-xs-8">
                                            <p class="text-center">Hommage à Jean Zay</p>
                                        </div>
                                        <div class="col-xs-2 text-right"><a href="#" title="Suivant" class="btn btn-primary rounded"><span class="fa fa-chevron-right"></span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="visible-xs text-center">
                        <?php include( "blocs/social.php"); ?>
                    </div>
                </article>
            </div>
            <div class="col-md-4 col-lg-3">
                <?php include( "blocs/diapo-filter.php"); ?>
            </div>
        </div>
    </div>
    </div>
    <?php include( 'blocs/footer.php'); ?>
    <?php include( 'blocs/scripts.php'); ?>
</body>

</html>
