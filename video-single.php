<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Le site d'Orléans et son AgglO - Liste d'événements</title>
<? include( 'blocs/styles.php') ?>
</head>
<body>
<? include( 'blocs/header.php') ?>
<div class="container video">
  <section>
    <div class="row">
      <div class="col-md-8 col-lg-9">
        <ol class="breadcrumb hidden-xs hidden-sm">
          <li><a href="index.php"><span class="fa fa-home"></span></a> </li>
          <li><a href="#">Vidéos</a> </li>
          <li class="active">Vidéos</li>
        </ol>
        <header class="header-article accroche">
          <div class="row">
            <div class="col-sm-8">
              <h1>Hommage à Jean Zay : <br>
discours d'Olivier Carré</h1> <span class="date">Publiée le
              <time datetime="2015-04-09">Jeudi 9 Avril 2015</time>
              </span>
            </div>
            <div class="col-sm-4 hidden-xs">
              <? include( "blocs/social.php"); ?>
            </div>
          </div>
                     <p class="texte-accroche">Depuis le mois d'octobre, le premier Fab Lab <i>(laboratoire de fabrication)</i> du département a ouvert ses portes à La Source.</p>
              

            <div class="embed-responsive embed-responsive-16by9">
              <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/NQFuS5tBVTQ?hl=fr&amp;fs=1&amp;rel=0&amp;wmode=opaque&amp;display=iframe&amp;pautohide=1"></iframe>
            </div>


        </header>
        
        <div class="visible-xs text-center">
          <? include( "blocs/social.php"); ?>
        </div>
      </div>
      <div class="col-md-4 col-lg-3">
        <? include( "blocs/diapo-filter.php"); ?>
      </div>
    </div>
  </section>
</div>
<? include( 'blocs/footer.php'); ?>
<? include( 'blocs/scripts.php'); ?>
</body>
</html>
