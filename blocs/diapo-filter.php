<div class="well sidebar">
  <section>
    <div class="row entete">
      <h3><span class="sr-only">Les diaporamas classées par </span>Type de Ressources</h3>
    </div>
    <ul class="nav nav-pills nav-stacked">
      <li><a href="#">Photos</a></li>
      <li><a href="#">Vidéos</a></li>
    </ul>
  </section>
  <section>
    <div class="row entete">
      <h3><span class="sr-only">Les diaporamas classées par </span>Thèmes</h3>
    </div>
    <ul class="nav nav-pills nav-stacked">
      <li><a href="#">Animations sur quais</a></li>
      <li><a href="#">Culture</a></li>
      <li><a href="#">Divers</a></li>
      <li><a href="#">Environnement</a></li>
      <li><a href="#">Festival de Loire</a></li>
      <li><a href="#">Fêtes de Jeanne d'Arc</a></li>
      <li><a href="#">Orléans Jazz</a></li>
      <li><a href="#">Sport</a></li>
      <li><a href="#">Urbanisme</a></li>
    </ul>
  </section>
</div>
