<ul class="pagination text-center">
	<li>
		<a href="#" aria-label="Previous" class="rounded-sm next-prev">
			<span aria-hidden="true" >&laquo;</span>
		</a>
	</li>
	<li class="active"><a href="#" class="rounded-sm">1</a></li>
	<li><a href="#" class="rounded-sm">2</a></li>
	<li><a href="#" class="rounded-sm">3</a></li>
	<li><a href="#" class="rounded-sm">4</a></li>
	<li><a href="#" class="rounded-sm">5</a></li>
	<li>
		<a href="#" aria-label="Next" class="rounded-sm next-prev">
			<span aria-hidden="true">&raquo;</span>
		</a>
	</li>
</ul>