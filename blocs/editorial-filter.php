<aside class="well sidebar">
  <h2 class="sr-only">En savoir plus</h2>
  <section>
    <div class="row entete">
      <h3>Le programme</h3>
    </div>
    <div>
      <figure> <a href="#"><img class="img-responsive center-block"  alt="Festival Hip Hop 2014" src="img/editorial/hip-hop.jpg"></a>
        <figcaption class="sr-only">Festival hip hop : programme</figcaption>
      </figure>
      <p class="text-center"> <a target="_blank" title="Télécharger le numéro" class="btn btn-default btn-alt" href="#"><span class="fa fa-download"></span> Télécharger le numéro en PDF</a> </p>
      <p class="text-center"> <a title="Feuilleter le numéro" class="btn btn-default btn-alt" href="#"><span class="fa fa-book"></span> Feuilleter en ligne</a> </p>
    </div>
  </section>
  </aside>
  <aside class="well sidebar">
  <section>
    <div class="row entete">
      <h3>Mission Jeunesse</h3>
    </div>
    <div class="list-unstyled">
        <address> <strong>Adresse</strong><br>
          2 bis rue des Anglaises, 45000 Orléans</address>
        <p class="adresse"> <strong>Téléphone</strong><br>
          02 38 79 23 65</p>
        <p><a href="#"><strong><span class="fa fa-send"></span> Contact</strong></a></p>
        <div class="embed-responsive embed-responsive-4by3">
          <iframe class="embed-responsive-item" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2674.6851068695323!2d1.9082420000000035!3d47.90377980000001!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47e4e4d2f6a08bd1%3A0x9587475e34c97997!2s2+Rue+des+Anglaises%2C+45000+Orl%C3%A9ans!5e0!3m2!1sfr!2sfr!4v1438593273909"></iframe>
        </div>
    </div>
  </section>
  </aside>
  <aside class="well sidebar">
  <section>
    <div class="row entete">
      <h3>L'Astrolabe</h3>
    </div>
    <ul class="list-unstyled">
      <li>
        <p class="adresse"> <strong>Adresse</strong><br>
          1, rue Alexandre Avisse, 45000 Orléans</p>
        <p class="adresse"> <strong>Téléphone</strong><br>
          02 38 54 20 06</p>
        <p class="adresse"><strong>Responsable</strong><br>
          <i>Direction :</i> Frédéric Robbe<br>
          <i>Gestion :</i> association Antirouille pour le compte de la ville d'Orléans<br>
          <i>Programmation Antirouille :</i> Matthieu Duffaud</p>
        <p><a href="#"><strong><span class="fa fa-link"></span> Site web</strong></a></p>
        <div class="embed-responsive embed-responsive-4by3">
          <iframe class="embed-responsive-item" src="https://www.google.com/maps/embed?pb=!1m16!1m12!1m3!1d2674.714889848645!2d1.893360749999997!3d47.9032034!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!2m1!1s1%2C+rue+Alexandre+Avisse%2C+45000+Orl%C3%A9ans!5e0!3m2!1sfr!2sfr!4v1438593378424"></iframe>
        </div>
      </li>
    </ul>
  </section>
</aside>
