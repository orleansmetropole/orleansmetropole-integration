<aside class="well sidebar">
  <section>
    <div class="row entete calendar">
      <div class="col-xs-3 col-sm-3 col-md-2"> <a href="#" title="Précédent" class="btn rounded-sm arrow"><span class="fa fa-chevron-left "></span></a> </div>
      <div class="col-xs-6 col-sm-6 col-md-8"> <a href="#">
        <h3><span class="sr-only">L'agenda du mois de : </span>Juillet 2015</h3>
        </a> </div>
      <div class="col-xs-3 col-sm-3 col-md-2 text-right"> <a href="#" title="Suivant" class="btn rounded-sm arrow"><span class="fa fa-chevron-right "></span></a> </div>
    </div>
    <table class="table calendrier">
      <thead>
        <tr class="cal-days">
          <th title="Lundi" class="intitule text-center">L</th>
          <th title="Mardi" class="intitule text-center">M</th>
          <th title="Mercredi" class="intitule text-center">M</th>
          <th title="Jeudi" class="intitule text-center">J</th>
          <th title="vendredi" class="intitule text-center">V</th>
          <th title="Samedi" class="intitule text-center">S</th>
          <th title="Dimanche" class="intitule text-center">D</th>
        </tr>
      </thead>
      <tbody>
        <tr class="cal-content text-center">
          <td class="cal-empty">&nbsp;</td>
          <td class="cal-empty">&nbsp;</td>
          <td class="cal-empty">&nbsp;</td>
          <td class="cal-empty">&nbsp;</td>
          <td class="cal-outdated">1</td>
          <td class="cal-outdated">2</td>
          <td class="cal-outdated event"><a href="/agenda/date/2015/05/08.htm" class="btn btn-primary rounded-sm">3</a></td>
        </tr>
        <tr class="cal-content text-center">
          <td class="cal-outdated">4</td>
          <td class="cal-outdated">5</td>
          <td class="cal-outdated">6</td>
          <td class="cal-outdated">7</td>
          <td class="cal-outdated event"><a href="/agenda/date/2015/05/08.htm" class="btn btn-primary rounded-sm">8</a></td>
          <td class="cal-outdated">9</td>
          <td class="cal-outdated">10</td>
        </tr>
        <tr class="cal-content text-center">
          <td class="cal-outdated">11</td>
          <td class="cal-outdated">12</td>
          <td class="cal-outdated">13</td>
          <td class="cal-outdated">14</td>
          <td class="cal-outdated">15</td>
          <td class="cal-outdated">16</td>
          <td class="cal-outdated">17</td>
        </tr>
        <tr class="cal-content text-center">
          <td class="cal-outdated today event"><a href="/agenda/date/2015/05/18.htm" class="btn btn-primary rounded-sm">18</a></td>
          <td class="cal-outdated">19</td>
          <td class="cal-today">20</td>
          <td class="cal-event">21</td>
          <td class="cal-event">22</td>
          <td class="cal-saturday">23</td>
          <td class="cal-sunday">24</td>
        </tr>
        <tr class="cal-content text-center">
          <td class="cal-event">25</td>
          <td class="cal-event">26</td>
          <td class="cal-event">27</td>
          <td class="cal-event">28</td>
          <td class="cal-event">29</td>
          <td class="cal-saturday event"><a href="/agenda/date/2015/05/30.htm" class="btn btn-primary rounded-sm">30</a></td>
          <td class="cal-sunday">31</td>
        </tr>
      </tbody>
    </table>
    <div class="form-group">
      <div class="radio">
        <label class="label-control">
          <input type="radio" value="today" name="tx_artificaevents_pi1[list_date]" id="tx_artificaevents_pi1_29_list_date_today">
          Aujourd'hui (0) </label>
      </div>
      <div class="radio">
        <label class="label-control">
          <input type="radio" value="week" name="tx_artificaevents_pi1[list_date]" id="tx_artificaevents_pi1_29_list_date_week">
          Cette semaine (1) </label>
      </div>
      <div class="radio">
        <label class="label-control">
          <input type="radio" value="nextMonth" name="tx_artificaevents_pi1[list_date]" id="tx_artificaevents_pi1_29_list_date_nextMonth">
          Mois prochain (5) </label>
      </div>
    </div>
  </section>
  <section>
    <div class="row entete">
      <h3><span class="sr-only">L'agenda classé </span>Par thèmes...</h3>
    </div>
    <div class="form-group">
      <div class="checkbox active">
        <label class="label-control">
          <input type="checkbox" checked="checked" value="12" name="theme1" id="theme1">
          Balade - Découverte - Visite </label>
      </div>
      <div class="checkbox">
        <label class="label-control">
          <input type="checkbox" value="12" name="theme2" id="theme2">
          Conférence - Débat - Rencontre </label>
      </div>
      <div class="checkbox">
        <label class="label-control">
          <input type="checkbox" value="12" name="theme3" id="theme3">
          Exposition </label>
      </div>
      <div class="checkbox active">
        <label class="label-control">
          <input type="checkbox" checked="checked" value="12" name="theme4" id="theme4">
          Fête - Foire - Salon </label>
      </div>
      <div class="checkbox">
        <label class="label-control">
          <input type="checkbox" value="12" name="theme5" id="theme5">
          Jeune Public </label>
      </div>
      <div class="checkbox">
        <label class="label-control">
          <input type="checkbox" value="12" name="theme6" id="theme6">
          Musique </label>
      </div>
      <div class="checkbox">
        <label class="label-control">
          <input type="checkbox" value="12" name="theme7" id="theme7">
          Projection </label>
      </div>
      <div class="checkbox">
        <label class="label-control">
          <input type="checkbox" value="12" name="theme8" id="theme8">
          Réunion Publique </label>
      </div>
      <div class="checkbox">
        <label class="label-control">
          <input type="checkbox" value="12" name="theme9" id="theme9">
          Spectacle Vivant </label>
      </div>
      <div class="checkbox">
        <label class="label-control">
          <input type="checkbox" value="12" name="theme10" id="theme10">
          Sport </label>
      </div>
      <div class="checkbox">
        <label class="label-control">
          <input type="checkbox" value="12" name="theme11" id="theme11">
          Stage - Atelier - Jeu </label>
      </div>
      <p class="text-center"> <a href="" class="btn btn-primary btn-alt btn-lg btn-rechercher"><span class="fa fa-search"></span> Rechercher</a> </p>
    </div>
  </section>
</aside>
