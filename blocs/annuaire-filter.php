<div class="well sidebar-form sidebar">
  <section>
    <div class="row entete">
      <h3>Recherche</h3>
    </div>
    <form method="post" action="#">
      <div class="form-group">
        <label class="control-label">Nom de l'association :</label>
        <input class="form-control" type="text" value="" placeholder="Mots libres">
      </div>
      <div class="form-group">
        <label class="control-label">Vous pouvez préciser une catégorie :</label>
        <select class="form-control">
          <option value="">Catégorie</option>
          <option value="">----------------------------------------</option>
          <option value="86">Anciens combattants</option>
          <option value="80">Culture</option>
          <option value="88">Divers</option>
          <option value="83">Famille</option>
          <option value="90">Formation-Enseignement</option>
          <option value="85">International (culture et action)</option>
          <option value="91">Jeunesse</option>
          <option value="84">Loisirs-Détente-Animation</option>
          <option value="79">Nature-Environnement</option>
          <option value="82">Santé-Handicap</option>
          <option value="87">Société</option>
          <option value="89">Solidarité social</option>
          <option value="81">Sports</option>
        </select>
      </div>
      <p class="text-center"><a href="" class="btn btn-primary btn-alt btn-rechercher btn-lg"><span class="fa fa-search"></span> Rechercher</a> </p>
    </form>
  </section>
    </aside>
  <aside class="well sidebar">
  <section>
    <div class="row entete">
      <h3>Infos pratiques</h3>
    </div>
        <h4>Service vie associative</h4>
    <ul class="list-unstyled">
      <li>
        <address>
        <strong>Adresse</strong> <br>
        Place de l'étape, 45000 Orléans<br />
        <strong>Téléphone</strong> <br>
        02 38 79 21 09
        </address>
        <div class="embed-responsive embed-responsive-4by3">
          <iframe class="embed-responsive-item" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2674.744352203207!2d1.9092835000000123!3d47.9026332!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47e4e4d271920501%3A0x601a4904218ba48a!2s2+Place+de+l&#39;%C3%89tape%2C+45000+Orl%C3%A9ans!5e0!3m2!1sfr!2sfr!4v1438593142399"> </iframe>
        </div>
      </li>
    </ul>
      <h4 class="separateur-sidebar">Téléchargements</h4>
    <ul class="nav nav-pills nav-stacked">
      <li><a target="_blank" href="#">Règlement vie associative <i>(pdf)</i></a> </li>
      <li><a target="_blank" href="#">Subventions 2013 <i>(pdf)</i></a> </li>
      <li><a target="_blank" href="#">Demande de subvention <i>(pdf)</i></a> </li>
    </ul>
        <h4 class="separateur-sidebar">Gérer son association</h4>
    <ul class="list-unstyled">
      <li>
        <p>Vous pouvez <b>modifier ou ajouter</b> des informations d'une association.</p>
        <p class="text-center"><a href="" class="btn btn-primary btn-alt btn-lg"><span class="fa fa-gears"></span> Gérer son association</a> 
      </li>
    </ul>
  </section>
</div>
