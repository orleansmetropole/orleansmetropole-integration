<?php
if(empty($cptDropdownMenuSocialId)) {
	$cptDropdownMenuSocialId=1;
}else {
	$cptDropdownMenuSocialId++;
}
?>
<div class="dropdown-social pull-right hidden-xs">
	<button class="share dropdown-toggle" type="button" id="dropdownMenuSocial<?php echo $cptDropdownMenuSocialId?>" data-toggle="dropdown" aria-expanded="true">
		Partager
		<span class="fa fa-share-alt btn btn-primary rounded-sm"></span>
	</button>
	<ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenuSocial<?php echo $cptDropdownMenuSocialId?>">
		<li><a href="#"><span class="fa fa-facebook"></span> Facebook</a></li>
		<li><a href="#"><span class="fa fa-twitter"></span> Twitter</a></li>
		<li><a href="#"><span class="fa fa-linkedin"></span> LinkedIn</a></li>
		<li><a href="#"><span class="fa fa-google"></span> Google +</a></li>
	</ul>
</div>
<div class="social-xs text-center visible-xs">
	<ul class="list-inline list-unstyled">
		<li><a href="#"><span class="fa fa-facebook btn btn-primary rounded"></span></a></li>
		<li><a href="#"><span class="fa fa-twitter btn btn-primary rounded"></span></a></li>
		<li><a href="#"><span class="fa fa-linkedin btn btn-primary rounded"></span></a></li>
		<li><a href="#"><span class="fa fa-google btn btn-primary rounded"></span></a></li>
	</ul>
</div>