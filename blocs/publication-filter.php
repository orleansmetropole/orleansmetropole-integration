<div class="well sidebar">
<section>
	<div class="row entete">
		<h3><span class="sr-only">Les publications classées </span>Par thème...</h3>
	</div>
	<ul class="nav nav-pills nav-stacked">
		<li><a target="_blank" href="#">Culture</a></li>
		<li><a target="_blank" href="#">Economie</a></li>
		<li><a target="_blank" href="#">Education</a></li>
		<li><a target="_blank" href="#">Environnement</a></li>
		<li><a target="_blank" href="#">Orléans.mag</a></li>
		<li><a target="_blank" href="#">Divers</a></li>	
		<li><a target="_blank" href="#">Séniors</a></li>
		<li><a target="_blank" href="#">Solidarité</a></li>
		<li><a target="_blank" href="#">Sport</a></li>	
		<li><a target="_blank" href="#">Urbanisme</a></li>			
	</ul>
    </section>
</div>