<h1 class="sr-only"><strong>Orléans</strong> & son <strong>AgglO</strong></h1>
<header>
  <nav>
    <h2 class="sr-only">Liens d'évitement</h2>
    <ul class="sr-only">
      <li><a href="/" accesskey="1" tabindex="1">Accueil du site</a></li>
      <li><a href="/actualites.htm#accesContent" accesskey="2" tabindex="2">Aller au contenu</a></li>
      <li><a href="/actualites.htm#navigationNiveau1Ancre" accesskey="3" tabindex="3">Aller a la navigation</a></li>
    </ul>
  </nav>
  <nav class="navbar-default sub-nav hidden-xs hidden-sm">
    <h2 class="sr-only">Menu transverse</h2>
    <div class="container">
      <ul class="nav navbar-nav navbar-right">
        <li><a href="#"><span class="fa fa-envelope" aria-hidden="true"></span> Nous contacter</a> </li>
        <li><a href="#" class="visible-lg visible-md"><span class="fa fa-wheelchair" aria-hidden="true"></span> Accessibilité</a> </li>
      </ul>
    </div>
  </nav>
  <div class="container">
    <div class="navbar-wrapper">
      <div class="row no-gutters">
        <div class="col-md-6 col-sm-7">
          <button class="navbar-toggle collapsed" type="button" data-toggle="collapse" data-target="#navbar-main-collapse" aria-controls="navbar-main-collapse" aria-expanded="false"> <span class="sr-only">Ouvrir le menu</span> <span class="icon-bar icon-bar-1"></span> <span class="icon-bar icon-bar-2"></span> <span class="icon-bar icon-bar-3"></span> </button>
          <div class="brand"><a href="index.php"><strong>Orléans</strong> & son <strong>AgglO</strong></a></div>
        </div>
        <div class="col-md-3 col-sm-5 text-right hidden-xs">
          <ul id="socials" class="list-inline text-uppercase text-right">
            <li class="twitter"><a href="https://twitter.com/OrleansAgglO" title="Suivez-nous sur Twitter" target="_blank" class="btn btn-default rounded-sm"><span class="fa fa-twitter"></span></a> </li>
            <li class="facebook"><a href="https://www.facebook.com/OrleansetsonAgglO" title="Suivez-nous sur Facebook" target="_blank" class="btn btn-default rounded-sm"><span class="fa fa-facebook"></span></a> </li>
            <li class="youtube"><a href="https://www.youtube.com/user/OrleansetsonAgglO" title="Suivez-nous sur Youtube" target="_blank" class="btn btn-default rounded-sm"><span class="fa fa-youtube"></span></a> </li>
            <li class="googleplus"><a href="https://plus.google.com/u/0/103392140269241914617/posts" title="Suivez-nous sur Google+" target="_blank" class="btn btn-default rounded-sm"><span class="fa fa-google-plus"></span></a> </li>
            <li class="visible-xs-inline-block visible-sm-inline-block">
              <button type="button" class="btn btn-primary navbar-search-sm rounded" data-toggle="modal" data-target="#searchModal" id="searchModalLabel"><span class="sr-only">Rechercher sur le site</span><span class="fa fa-search"></span> </button>
            </li>
          </ul>
        </div>
        <div class="col-md-3 text-right hidden-xs">
          <form class="navbar-form hidden-xs hidden-sm">
            <div class="form-group">
              <input type="text" class="form-control" placeholder="Rechercher sur le site...">
            </div>
            <a href="#" class="btn btn-primary rounded-sm "><span class="fa fa-search"></span></a>
          </form>
        </div>
      </div>
    </div>
  </div>
  <div class="container">
    <nav class="navbar-default navbar-inverse mega-menu mega-menu-full">
      <h2 class="sr-only">Menu principal</h2>
      <div class="collapse navbar-collapse" id="navbar-main-collapse">
        <ul class="nav navbar-nav main-nav">
          <li class="dropdown"><a href="index.php"><span class="fa fa-home"></span><span class="hidden-lg hidden-md"> Page d'accueil</span></a></li>
          <li class="dropdown"><a class="dropdown-toggle" href="#" data-toggle="dropdown">Actualités <span class="fa fa-chevron-down"></span> </a> </li>
          <li class="dropdown"><a class="dropdown-toggle" href="#" data-toggle="dropdown">Présentation <span class="fa fa-chevron-down"></span> </a> </li>
          <li class="dropdown"><a class="dropdown-toggle" href="#" data-toggle="dropdown">Programme <span class="fa fa-chevron-down"></span></a> </li>
          <li class="dropdown"><a class="dropdown-toggle" href="#" data-toggle="dropdown">Photos/Vidéos <span class="fa fa-chevron-down"></span> </a> </li>
          <li class="dropdown"><a class="dropdown-toggle" href="#" data-toggle="dropdown">Espace presse <span class="fa fa-chevron-down"></span></a> </li>
          <li class="dropdown"><a class="dropdown-toggle" href="#" data-toggle="dropdown">Je suis <span class="fa fa-chevron-down"></span></a>
            <div class="dropdown-menu">
              <div class="container">
                <div class="col-md-7">
                  <ul class="sub-menu row">
                    <li class="col-md-6"><a href="agenda-list.php">Visiteur</a> </li>
                    <li class="col-md-6"><a href="#">Parent</a> </li>
                    <li class="col-md-6"><a href="#">Senior</a> </li>
                    <li class="col-md-6"><a href="#">Jeune</a> </li>
                    <li class="col-md-6"><a href="#">Association</a> </li>
                    <li class="col-md-6"><a href="#">Journaliste</a> </li>
                    <li class="col-md-6"><a href="#">Entrepreneur</a> </li>
                  </ul>
                </div>
              </div>
            </div>
          </li>
        </ul>
      </div>
      <!-- /.navbar-collapse -->
    </nav>
  </div>
  </div>
  <!-- Modal pour le moteur de recherche en mode SM et XS-->
  <div class="modal fade" id="searchModal" tabindex="-1" role="dialog" aria-labelledby="searchModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Rechecher</h4>
        </div>
        <div class="modal-body">
          <form>
            <div class="form-group">
              <input type="text" class="form-control" placeholder="Rechercher sur le site...">
            </div>
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Annuler</button>
          <button type="button" class="btn btn-primary"><span class="fa fa-search"></span> Lancer la recherche</button>
        </div>
      </div>
    </div>
  </div>
</header>
