<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Le site d'Orléans et son AgglO - Liste d'événements</title>
<?php include( 'blocs/styles.php') ?>
</head>
<body>
<?php include( 'blocs/header.php') ?>
<div class="container edit">
  <div class="row">
    <article>
      <div class="col-md-8 col-lg-9">
        <ol class="breadcrumb hidden-xs">
          <li><a href="index.php">Accueil</a> </li>
          <li><a href="#">Rubrique</a> </li>
          <li class="active">Page de styles</li>
        </ol>
        <header class="accroche header-article">
          <div class="row">
            <div class="col-sm-8">
              <h1>Page de styles</h1>
            </div>
            <div class="col-sm-4 hidden-xs">
              <?php include( "blocs/social.php"); ?>
            </div>
          </div>
        </header>
        <p class="texte-accroche">Apple pie icing liquorice tiramisu. Lollipop wypas chupa chups pie pastry soufflé jujubes tootsie roll. Apple pie carrot cake bear claw soufflé.</p>
        <p>Fruitcake gummi bears sugar plum marzipan fruitcake tiramisu tootsie roll. Donut bonbon topping macaroon chocolate bar caramels oat cake. Apple pie tootsie roll wypas pudding croissant chocolate cake. Applicake croissant lemon drops carrot cake powder bonbon. Halvah donut oat cake cake applicake. Muffin jelly beans lemon drops oat cake macaroon chocolate cake. Soufflé gingerbread chocolate cake cupcake tootsie roll. Jujubes jelly-o caramels chocolate toffee oat cake. Dragée bear claw carrot cake sweet. Gummi bears wypas wafer lollipop.</p>
        <h2>Ceci est un titre de niveau 2</h2>
        <p>Marshmallow candy canes sweet cupcake gummi bears apple pie pudding. Sesame snaps dragée tart tart sugar plum pastry caramels danish. Jujubes croissant sweet. Jelly beans cotton candy caramels donut sugar plum icing pie muffin applicake. Gummies tiramisu gingerbread chupa chups apple pie sesame snaps chocolate cake powder topping. Faworki lollipop biscuit gummi bears candy canes. Applicake sweet roll lollipop wafer topping donut chupa chups gingerbread. Marzipan marshmallow applicake tart. Cake powder croissant pudding gummi bears powder. Jujubes apple pie caramels. Sweet roll oat cake caramels jelly beans tiramisu. Faworki marzipan applicake faworki chocolate bar fruitcake cupcake lemon drops. Cheesecake caramels tiramisu fruitcake cotton candy. Sugar plum lollipop chocolate bar croissant ice cream apple pie halvah dragée.</p>
        <h3>Ceci est un titre de niveau 3</h3>
        <p>Sweet roll soufflé danish sesame snaps pastry applicake macaroon croissant. Faworki dragée icing applicake applicake macaroon marshmallow pie. Biscuit pastry chocolate cheesecake jelly beans powder lemon drops sesame snaps.</p>
        <h4>Ceci est un titre de niveau 4</h4>
        <p>Oat cake tootsie roll faworki. Lemon drops brownie tiramisu pie. Soufflé lemon drops jujubes pudding jelly lollipop carrot cake fruitcake marshmallow. Dragée donut carrot cake tiramisu. Ice cream apple pie soufflé cotton candy chocolate cake. Soufflé marshmallow dragée dragée jelly beans lollipop halvah jelly beans. Croissant powder sweet marzipan cookie halvah halvah. </p>
        <div class="titre">Citations</div>
        <blockquote>Cupcake ipsum dolor sit. Amet sweet roll cheesecake applicake lollipop sweet topping cotton candy. Bear claw icing candy croissant liquorice croissant candy cheesecake wafer. Chocolate cake bonbon cupcake chupa chups muffin lollipop.</blockquote>
        <p><em>Légende : Marshmallow candy canes sweet cupcake gummi bears apple pie pudding</em></p>
        <cite>Liste simple avec un sous-niveau (personnalisation de la couleur de flèche au moment de la découpe)</cite>
        <ul>
          <li>Cupcake ipsum dolor sit amet donut jelly macaroon.</li>
          <li>Candy canes chocolate fruitcake chocolate bar chocolate toffee. Chocolate sweet topping liquorice.</li>
          <li>Gummies biscuit powder candy caramels halvah lollipop wypas. Liquorice jelly beans toffee powder jelly beans pastry pudding dessert chocolate.</li>
          <li>Applicake halvah pudding. Candy canes marzipan croissant.
            <ul>
              <li>Cupcake ipsum dolor sit amet donut jelly macaroon.</li>
              <li>Candy canes chocolate fruitcake chocolate bar chocolate toffee. Chocolate sweet topping liquorice.</li>
              <li>Gummies biscuit powder candy caramels halvah lollipop wypas. Liquorice jelly beans toffee powder jelly beans pastry pudding dessert chocolate.</li>
              <li>Applicake halvah pudding. Candy canes marzipan croissant.</li>
              <li>Sugar plum muffin topping cotton candy wypas.</li>
            </ul>
          </li>
          <li>Sugar plum muffin topping cotton candy wypas.</li>
        </ul>
        <div class="titre">Liste numérotée</div>
        <ol>
          <li> iscuit gummi bears donut. </li>
          <li>Marshmallow jelly-o toffee chocolate cake chocolate cake marzipan wypas marzipan.</li>
          <li>Liquorice faworki jelly apple pie sweet caramels chocolate.</li>
          <li>Wypas brownie liquorice bonbon oat cake applicake candy faworki.</li>
          <li>Tart ice cream macaroon tart powder. Lollipop powder cupcake.</li>
        </ol>
        <div class="alert alert-info">
          <h2><span class="fa fa-warning"></span> Alert</h2>
          <p>Cupcake ipsum dolor sit amet donut jelly macaroon. Candy canes chocolate fruitcake chocolate bar chocolate toffee. </p>
        </div>
        <div class="alert alert-success">
          <h2><span class="fa fa-warning"></span> Alert</h2>
          <p>Cupcake ipsum dolor sit amet donut jelly macaroon. Candy canes chocolate fruitcake chocolate bar chocolate toffee. </p>
        </div>
        <div class="alert alert-warning">
          <h2><span class="fa fa-warning"></span> Alert</h2>
          <p>Cupcake ipsum dolor sit amet donut jelly macaroon. Candy canes chocolate fruitcake chocolate bar chocolate toffee. </p>
        </div>
        <div class="alert alert-danger">
          <h2><span class="fa fa-warning"></span> Alert</h2>
          <p>Cupcake ipsum dolor sit amet donut jelly macaroon. Candy canes chocolate fruitcake chocolate bar chocolate toffee. </p>
        </div>
        <div class="titre">Tableaux</div>
        <table class="table table-striped">
          <thead>
            <tr>
              <th>Colonne 1</th>
              <th>Colonne 2</th>
              <th>Colonne 3</th>
              <th>Colonne 4</th>
              <th>Colonne 5</th>
              <th>Colonne 6</th>
              <th>Colonne 7</th>
              <th>Colonne 8</th>
              <th>Colonne 9</th>
              <th>Colonne 10</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>Contenu </td>
              <td>4</td>
              <td>2</td>
              <td>2</td>
              <td>0</td>
              <td>166</td>
              <td>135</td>
              <td>1.2296</td>
              <td>83</td>
              <td>67.5</td>
            </tr>
            <tr>
              <td>Contenu </td>
              <td>4</td>
              <td>2</td>
              <td>2</td>
              <td>0</td>
              <td>166</td>
              <td>135</td>
              <td>1.2296</td>
              <td>83</td>
              <td>67.5</td>
            </tr>
            <tr>
              <td>Contenu </td>
              <td>4</td>
              <td>2</td>
              <td>2</td>
              <td>0</td>
              <td>166</td>
              <td>135</td>
              <td>1.2296</td>
              <td>83</td>
              <td>67.5</td>
            </tr>
            <tr>
              <td>Contenu </td>
              <td>4</td>
              <td>2</td>
              <td>2</td>
              <td>0</td>
              <td>166</td>
              <td>135</td>
              <td>1.2296</td>
              <td>83</td>
              <td>67.5</td>
            </tr>
            <tr>
              <td>Contenu </td>
              <td>4</td>
              <td>2</td>
              <td>2</td>
              <td>0</td>
              <td>166</td>
              <td>135</td>
              <td>1.2296</td>
              <td>83</td>
              <td>67.5</td>
            </tr>
            <tr>
              <td>Contenu </td>
              <td>4</td>
              <td>2</td>
              <td>2</td>
              <td>0</td>
              <td>166</td>
              <td>135</td>
              <td>1.2296</td>
              <td>83</td>
              <td>67.5</td>
            </tr>
            <tr>
              <td>Contenu </td>
              <td>4</td>
              <td>2</td>
              <td>2</td>
              <td>0</td>
              <td>166</td>
              <td>135</td>
              <td>1.2296</td>
              <td>83</td>
              <td>67.5</td>
            </tr>
            <tr>
              <td>Contenu </td>
              <td>4</td>
              <td>2</td>
              <td>2</td>
              <td>0</td>
              <td>166</td>
              <td>135</td>
              <td>1.2296</td>
              <td>83</td>
              <td>67.5</td>
            </tr>
            <tr>
              <td>Contenu </td>
              <td>4</td>
              <td>2</td>
              <td>2</td>
              <td>0</td>
              <td>166</td>
              <td>135</td>
              <td>1.2296</td>
              <td>83</td>
              <td>67.5</td>
            </tr>
            <tr>
              <td>Contenu </td>
              <td>4</td>
              <td>2</td>
              <td>2</td>
              <td>0</td>
              <td>166</td>
              <td>135</td>
              <td>1.2296</td>
              <td>83</td>
              <td>67.5</td>
            </tr>
          </tbody>
          <tfoot>
            <tr>
              <td>Contenu </td>
              <td>4</td>
              <td>2</td>
              <td>2</td>
              <td>0</td>
              <td>166</td>
              <td>135</td>
              <td>1.2296</td>
              <td>83</td>
              <td>67.5</td>
            </tr>
          </tfoot>
        </table>
        <hr>
        <!-- Standard button -->
        <button type="button" class="btn btn-default">Default</button>
        <!-- Provides extra visual weight and identifies the primary action in a set of buttons -->
        <button type="button" class="btn btn-primary">Primary</button>
        <!-- Indicates a successful or positive action -->
        <button type="button" class="btn btn-success">Success</button>
        <!-- Contextual button for informational alert messages -->
        <button type="button" class="btn btn-info">Info</button>
        <!-- Indicates caution should be taken with this action -->
        <button type="button" class="btn btn-warning">Warning</button>
        <!-- Indicates a dangerous or potentially negative action -->
        <button type="button" class="btn btn-danger">Danger</button>
        <!-- Deemphasize a button by making it look like a link while maintaining button behavior -->
        <button type="button" class="btn btn-link">Link</button>
        <hr>
        <!-- Alternative button -->
        <button type="button" class="btn btn-default btn-alt">Default</button>
        <!-- Provides extra visual weight and identifies the primary action in a set of buttons -->
        <button type="button" class="btn btn-primary btn-alt">Primary</button>
        <!-- Indicates a successful or positive action -->
        <button type="button" class="btn btn-success btn-alt">Success</button>
        <!-- Contextual button for informational alert messages -->
        <button type="button" class="btn btn-info btn-alt">Info</button>
        <!-- Indicates caution should be taken with this action -->
        <button type="button" class="btn btn-warning btn-alt">Warning</button>
        <!-- Indicates a dangerous or potentially negative action -->
        <button type="button" class="btn btn-danger btn-alt">Danger</button>
        <!-- Deemphasize a button by making it look like a link while maintaining button behavior -->
        <button type="button" class="btn btn-link btn-alt">Link</button>
        <div class="visible-xs text-center">
          <?php include( "blocs/social.php"); ?>
        </div>
      </div>
    </article>
    <div class="col-md-4 col-lg-3">
      <?php include( "blocs/editorial-filter.php"); ?>
    </div>
  </div>
</div>
<?php include( 'blocs/footer.php'); ?>
<?php include( 'blocs/scripts.php'); ?>
</body>
</html>
