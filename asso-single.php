<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>Le site d'Orléans et son AgglO - 1, 2 3... Lyrique</title>
    <? include( 'blocs/styles.php') ?>
</head>

<body>
    <? include( 'blocs/header.php') ?>
    <div class="container asso">
        <div class="row">
            <div class="col-md-8 col-lg-9">
                <ol class="breadcrumb hidden-xs">
                    <li><a href="index.php">Accueil</a>
                    </li>
                    <li><a href="#">Associations</a>
                    </li>
                    <li class="active">1, 2, 3... Lyrique</li>
                </ol>
                <article>
                    <header class="header-article accroche">
                        <div class="row">
                            <div class="col-sm-8">
                                <h1>1, 2, 3... Lyrique</h1>
                            </div>
                            <div class="col-sm-4 hidden-xs">
                                <? include( "blocs/social.php"); ?>
                            </div>
                        </div>
                        <p class="texte-accroche">Association de chanteurs lyriques amateurs, motivés par la musique d'ensemble et ayant comme objectif des projets de concerts ou de spectacles musicaux sous la direction de Corinne Sertilanges.</p>
                    </header>
                    <div class="infos">
                        <section class="exhibit">
                            <h2>Infos pratiques</h2>
                            <div class="location row">
                                <div class="col-sm-6">
                                    <div class="embed-responsive embed-responsive-16by9">
                                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2674.625085386902!2d1.8897878999999878!3d47.904941400000006!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47e4e52d6ad4a311%3A0x9ac2e2ab2be3835d!2s29+Rue+Basse+d&#39;Ingre%2C+45000+Orl%C3%A9ans!5e0!3m2!1sfr!2sfr!4v1438164052154"></iframe>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <p><strong>Siège social</strong>
                                        <br>29, rue Basse-d'Ingré 45000 Orléans</p>
                                    <p><a href="#" target="_blank"><span class="fa fa-link"></span> Site Web</a></p>
                                    <p><strong>Téléphone(s)</strong>
                                        <br>02 38 53 22 82
                                        <br>02 38 54 23 20</p>
                                    <p><a href="#"><span class="fa fa-send"></span> Courriel</a></p>    
                                </div>
                            </div>
                        </section>
                    </div>
                </article>
            </div>
            <div class="col-md-4 col-lg-3">
                <? include( 'blocs/annuaire-filter.php'); ?>
            </div>
        </div>
    </div>
    <? include( 'blocs/footer.php'); ?>
    <? include( 'blocs/scripts.php'); ?>
</body>

</html>
