<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>Portail citoyen Orléans - Accueil</title>
  <?php include( 'blocs/styles.php') ?>

<body>
<?php include( 'blocs/header.php') ?>
<div class="container dashboard">
    <section>
        <div class="row">
            <div class="col-lg-9">
                <ol class="breadcrumb hidden-xs">
                    <li><a href="#">Compte citoyen</a>
                    </li>
                    <li class="active">Mon dossier</li>
                </ol>
                <div class="header-article">
                    <h1>Portail de services en ligne</h1>
                    <span class="date">Bonjour M. HEZARD Sébastien</span>
                </div>
                <div class="accroche">
                    <p class="texte-accroche">Vous êtes actuellement rélié au <b>dossier famille n°23929</b>. Si vous désirez ajouter une information ou corriger une erreur, utilisez les <a href="#"><u>téléservices prévus pour les modifications</u></a>.</p>
                </div>

                <div class="row">
                    <article class="col-sm-6 col-md-4 col-lg-4 ">
                        <div class="text-center">
                            <span class="fa fa-child fa-5x fa-border"></span>
                            <h3>Petite enfance</h3>
                        </div>
                        <div class="caption sommaire">
                            <div class="list-group">
                                <a href="#" class="list-group-item">
                                    <h4 class="list-group-item-heading">Demande de place en crèche</h4>
                                    <p class="list-group-item-text small">Explication sur la demande afin de bien contextualiser la demande...</p>
                                </a>
                                <a href="#" class="list-group-item">
                                    <h4 class="list-group-item-heading">Planning accueil urgence</h4>
                                    <p class="list-group-item-text small">Explication sur la demande afin de bien contextualiser la demande...</p>
                                </a>
                            </div>
                        </div>
                    </article>

                    <article class="col-sm-6 col-md-4 col-lg-4 ">
                        <div class="text-center">
                            <span class="fa fa-file-text-o fa-5x fa-border"></span>
                            <h3>Etat civil</h3>
                        </div>
                        <div class="caption sommaire">
                            <div class="list-group">
                                <a href="#" class="list-group-item">
                                    <h4 class="list-group-item-heading">Acte de naissance</h4>
                                    <p class="list-group-item-text small">Explication sur la demande afin de bien contextualiser la demande...</p>
                                </a>
                                <a href="#" class="list-group-item">
                                    <h4 class="list-group-item-heading">Acte de mariage</h4>
                                    <p class="list-group-item-text small">Explication sur la demande afin de bien contextualiser la demande...</p>
                                </a>
                            </div>
                        </div>
                    </article>

                    <article class="col-sm-6 col-md-4 col-lg-4 ">
                        <div class="text-center">
                            <span class="fa fa-envelope fa-5x fa-border"></span>
                            <h3>Nous contacter</h3>
                        </div>
                        <div class="caption sommaire">
                            <div class="list-group">
                                <a href="#" class="list-group-item">
                                    <h4 class="list-group-item-heading">Formulaire de contact</h4>
                                </a>
                            </div>
                        </div>
                    </article>
                </div>

            </div>
            <div class="col-lg-3">
              <?php include('blocs/sidebar.php') ?>
            </div>
        </div>
    </section>
</div>
<?php include( 'blocs/footer.php'); ?>
<?php include( 'blocs/scripts.php'); ?>
</body>

</html>
