<?php

require_once "config.php";
$info = "Gestion du compte";
$is_connected = true;
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>Portail citoyen Orléans - Accueil</title>
  <?php include( 'blocs/styles.php') ?>

<body>
<?php include( 'blocs/header.php') ?>
<div class="container dashboard">
    <section>
        <div class="row">
            <div class="col-lg-9">
                <ol class="breadcrumb hidden-xs">
                    <li><a href="#">Compte citoyen</a>
                    </li>
                    <li class="active">Mon compte</li>
                </ol>
                <div class="accroche">
                    <p class="texte-accroche">
                        Gestion de vos informations personnelles
                    </p>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="panel panel-default">
                            <!-- Default panel contents -->
                            <div class="panel-heading">Informations</div>
<!--                            <div class="panel-body">-->
<!--                                <p>A tout moment vous pouvez modifier vos informations personnelles</p>-->
<!--                            </div>-->
                            <!-- List group -->
                            <ul class="list-group">
                                <li class="list-group-item"><strong>Nom :</strong> Cochard</li>
                                <li class="list-group-item"><strong>Prénom :</strong> Geoffroy</li>
                                <li class="list-group-item"><strong>Couriel :</strong> geoffroycochard@gmail.com</li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="panel panel-default">
                            <!-- Default panel contents -->
                            <div class="panel-heading">Modifier vos informations</div>
                            <!-- List group -->
                            <ul class="list-group">
                                <li class="list-group-item">
                                    <a href="">
                                        <span class="fa fa-envelope"></span> Modifier le courriel associé
                                    </a>
                                </li>
                                <li class="list-group-item">
                                    <a href="">
                                        <span class="fa fa-user"></span> Modifier le mot de passe
                                    </a>
                                </li>
                                <li class="list-group-item">
                                    <a href="">
                                        <span class="fa fa-pencil"></span> Éditer les données du compte
                                    </a>
                                </li>
                                <li class="list-group-item">
                                    <a href="">
                                        <span class="fa fa-trash"></span> Supprimer le compte
                                    </a>
                                </li>
                            </ul>
                        </div>

                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="facture">Factures en cours <span class="highlight">(3)</span></h3>
                        <div class="panel-actions">
                            <h4><a href="#" class=""><span class="visible-xs">+</span><span class="hidden-xs">Toutes les factures</span></a></h4>
                        </div>
                    </div>
                    <div class="panel-body">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Montant facture</th>
                                <th>Restant à payer</th>
                                <th>&Eacute;mise le</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td><a href="#">1502020933</a>
                                </td>
                                <td>134.56 €</td>
                                <td><b>134.56 €</b>
                                </td>
                                <td>5 mai 2015</td>
                                <td>
                                    <a href="#" class="payer"> Voir et payer</a>
                                </td>
                            </tr>
                            <tr>
                                <td><a href="#">1502020932</a>
                                </td>
                                <td>110.99 €</td>
                                <td><b>85.54 €</b>
                                </td>
                                <td>2 mai 2015</td>
                                <td>
                                    <a href="#" class="payer"> Voir et payer</a>
                                </td>
                            </tr>
                            <tr>
                                <td><a href="#">1502020931</a>
                                </td>
                                <td>34.56 €</td>
                                <td><b>34.56 €</b>
                                </td>
                                <td>1 mai 2015</td>
                                <td>
                                    <a href="#" class="payer"> Voir et payer</a>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
            <div class="col-lg-3">
              <?php include('blocs/sidebar.php') ?>
            </div>
        </div>
    </section>
</div>
<?php include( 'blocs/footer.php'); ?>
<?php include( 'blocs/scripts.php'); ?>
</body>

</html>
