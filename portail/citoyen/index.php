<?php

require_once "config.php";
$info = "Dashboard user en mode connecté";
$is_connected = true;
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>Portail citoyen Orléans - Accueil</title>
  <?php include( 'blocs/styles.php') ?>

<body>
<?php include( 'blocs/header.php') ?>
<div class="container dashboard">
    <section>
        <div class="row">
            <div class="col-lg-9">
                <ol class="breadcrumb hidden-xs">
                    <li><a href="#">Compte citoyen</a>
                    </li>
                    <li class="active">Mon dossier</li>
                </ol>
                <div class="accroche">
                    <p class="texte-accroche">
                        Services disponibles.
                    </p>
                </div>

                <div class="row">

                  <?php $i = 0; foreach ($menus as $menu) : $i++;?>
                      <article class="col-sm-6 col-md-4 col-lg-4 ">
                          <div class="text-center">
                              <span class="fa fa-<?php echo $menu['icon'] ?> fa-5x fa-border"></span>
                              <h3><?php echo $menu['label'] ?></h3>
                          </div>
                          <div class="caption sommaire">
                              <div class="list-group">
                                <?php foreach ($menu['items'] as $item) : ?>
                                    <a href="<?php echo $item['url'] ?>" class="list-group-item" <?php if($item['connected']) : ?>style="position: relative;
  background: radial-gradient(circle at 100% 0,  #34697d 27px, transparent 15px);"<?php endif; ?>>
                                      <?php if($item['connected']) : ?>
                                          <span class="fa fa-user fa-inverse"
                                                style="position: absolute; top: 2px; right: 4px;"
                                          ></span>
                                      <?php endif; ?>
                                        <h4 class="list-group-item-heading"><?php echo $item['label'] ?></h4>
                                      <?php if (!empty($item['description'])) : ?>
                                          <p class="list-group-item-text small"><?php echo $item['description'] ?></p>
                                      <?php endif; ?>
                                    </a>
                                <?php endforeach; ?>
                              </div>
                          </div>
                      </article>
                    <?php if ($i%3 === 0) : ?><div class="clearfix" style="margin-bottom: 30px"></div><?php endif; ?>
                  <?php endforeach; ?>

                </div>

            </div>
            <div class="col-lg-3">
                <?php include('blocs/sidebar.php') ?>
            </div>
        </div>
    </section>
</div>
<?php include( 'blocs/footer.php'); ?>
<?php include( 'blocs/scripts.php'); ?>
</body>

</html>
