<h1 class="sr-only"><strong>Portail citoyen</strong> d' <strong>Orléans</strong></h1>
<header>
  <nav>
    <h2 class="sr-only">Liens d'évitement</h2>
    <ul class="sr-only">
      <li><a href="/" accesskey="1" tabindex="1">Accueil du site</a></li>
      <li><a href="/actualites.htm#accesContent" accesskey="2" tabindex="2">Aller au contenu</a></li>
      <li><a href="/actualites.ht"m#navigationNiveau1Ancre" accesskey="3" tabindex="3">Aller a la navigation</a></li>
    </ul>
  </nav>
  <nav class="navbar-default sub-nav hidden-xs hidden-sm">
    <h2 class="sr-only">Menu transverse</h2>
    <div class="container">
      <ul class="nav navbar-nav navbar-right">
        <li><a href="http://www.orleans-metropole.fr" class="visible-lg visible-md"><span class="fa fa-globe" aria-hidden="true"></span> Orléans-Métropole.fr</a> </li>
        <li><a href="connexion.php"><span class="fa fa-sign-in" aria-hidden="true"></span> Connexion</a> </li>
      </ul>
    </div>
  </nav>
  <div class="container">
    <div class="navbar-wrapper">
      <div class="row no-gutters">
        <div class="col-md-6 col-sm-7">
          <button class="navbar-toggle collapsed" type="button" data-toggle="collapse" data-target="#navbar-main-collapse" aria-controls="navbar-main-collapse" aria-expanded="false"> <span class="sr-only">Ouvrir le menu</span> <span class="icon-bar icon-bar-1"></span> <span class="icon-bar icon-bar-2"></span> <span class="icon-bar icon-bar-3"></span> </button>
          <h1 class="brand"><a href="index.php"><strong>Portail citoyen</strong> - <small>Orléans</small></a></h1>
        </div>
      </div>
    </div>
  </div>
  <div class="container">
    <nav class="navbar-default navbar-inverse mega-menu mega-menu-full">
      <h2 class="sr-only">Menu principal</h2>
      <!-- Brand and toggle get grouped for better mobile display -->
      <div class="navbar-header"></div>
      <!-- Collect the nav links, forms, and other content for toggling -->
      <div class="collapse navbar-collapse" id="navbar-main-collapse">
        <ul class="nav navbar-nav main-nav">
          <li class="hidden-xs hidden-sm"><a href="index.php" ><span class="fa fa-home"></span><span class="hidden-lg hidden-md"> Page d'accueil</span></a> </li>
            <?php foreach($main_menus as $menu) : ?>
                <li class="dropdown"><a class="" href="<?php echo $menu['url'] ?>"><?php echo $menu['label'] ?></a> </li>
            <?php endforeach; ?>
        </ul>
      </div>
      <!-- /.navbar-collapse -->
    </nav>
  </div>


</header>
