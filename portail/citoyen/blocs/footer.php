<span id="top-link-block" class="text-right"> <a href="#top" class="btn rounded" onclick=""> <i class="fa fa-chevron-up"></i> </a> </span>
<footer>
    <p class="small text-center">Conformément à la loi Informatique et Libertés du 6 janvier 1978, vous disposez d'un droit d'accès, de modification, de rectification et de suppression des données vous concernant. Pour exercer ce droit, nous vous invitons à nous contacter.</p>
  <div class="infos-footer">
    <div class="container">
      <section>
        <h2 class="sr-only">Coordonnés et horaires</h2>
        <div class="row">
          <div class="col-md-5">
            <h3>Mairie d'Orléans</h3>
            <br>
            <div class="row">
              <div class="col-sm-5 col-md-6"> <img src="theme/images/logo_mairie.png" class="img-responsive" alt="logo mairie d'orleans"> </div>
              <br class="visible-xs">
              <address class="col-sm-7 col-md-6">
              <p><span class="fa fa-map-marker"></span> 1 place de l'Étape<br>
                45040 Orléans Cedex 1<br>
                <span class="fa fa-phone"></span> 02 38 79 22 22</p>
              </address>
            </div>
            <h4>Horaires de la Mairie :</h4>
            <ul class="horaires">
              <li><i>lundi au jeudi :</i> 8h30 - 17h30</li>
              <li><i>vendredi :</i> 8h30 - 17h00</li>
              <li><i>Samedi :</i> 9h00 - 12h00 (uniquement les affaires administratives)</li>
            </ul>
          </div>
          <div class="col-md-5 pull-right-lg">
            <h3>Orléans Métropole</h3>
            <br>
            <div class="row">
              <div class="col-sm-5 col-md-7"> <img src="theme/images/logo_metropole.png" class="img-responsive" alt="logo agglomération d'orleans"> </div>
              <br class="visible-xs">
              <address class="col-sm-7 col-md-5">
              <p><span class="fa fa-map-marker"></span> 5 Place 6 Juin 1944<br>
                45000 Orléans<br>
                <span class="fa fa-phone"></span> 02 38 78 75 75</p>
              </address>
            </div>
            <h4>Horaires d'Orléans Métropole :</h4>
            <ul class="horaires">
              <li><i>lundi au vendredi :</i> 8h00 - 18h00</li>
            </ul>
          </div>
        </div>
      </section>
      <hr>
      <section>
        <h2 class="sr-only">Nos partenaires</h2>
        <ul class="list-unstyled list-inline text-center partenaires">
          <li> <img src="theme/images/logo_loireco.png" alt="logo loireco"> </li>
          <li> <img src="theme/images/logo_loireco.png" alt="logo loireco"> </li>
          <li> <img src="theme/images/logo_loireco.png" alt="logo loireco"> </li>
        </ul>
      </section>
    </div>
  </div>
  <nav class="bottom-bar">
    <h2 class="sr-only">Menu secondaire</h2>
    <div class="text-center text-capitalize">
      <ul class="list-inline">
        <li><a href="/contact.htm" class="title"><span class="fa fa-envelope"></span> Contact</a></li>
        <li><a href="/outils-et-services/wifi-orleans.htm" class="title"><span class="fa fa-newspaper-o"></span> Espace presse</a></li>
        <li><a href="/outils-et-services/mentions-legales.htm" class="title"><span class="fa fa-file-text-o"></span> Mentions légales</a></li>
      </ul>
    </div>
  </nav>
</footer>

<div id="google_translate_element" style="display: none;"></div>
<script type="text/javascript">
    function googleTranslateElementInit() {
        new google.translate.TranslateElement({pageLanguage: 'fr', includedLanguages: 'en,fr,zh-CN', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, autoDisplay: false}, 'google_translate_element');
    }
</script>
<script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>

<?php if (!empty($info)) : ?>
<!--    <div class="alert alert-info alert-dismissible small" style="position: fixed; bottom: 0; right: 0">-->
<!--        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
<!--        --><?php //echo $info ?>
<!--        <ul class="list-unstyled small">-->
<!--            <li><a href="--><?php //echo getPathUrl('index.php') ?><!--">Dashboard</a></li>-->
<!--            <li><a href="--><?php //echo getPathUrl('formulaire.php') ?><!--">Exemple formulaire (style)</a></li>-->
<!--            <li><a href="--><?php //echo getPathUrl('connexion.php') ?><!--">Page de connexion</a></li>-->
<!--            <li><a href="--><?php //echo getPathUrl('facture.php') ?><!--">Facture</a></li>-->
<!--            <li><a href="--><?php //echo getPathUrl('demandes.php') ?><!--">Demandes</a></li>-->
<!--            <li><a href="--><?php //echo getPathUrl('compte.php') ?><!--">Compte</a></li>-->
<!--        </ul>-->
<!--    </div>-->
<?php endif; ?>