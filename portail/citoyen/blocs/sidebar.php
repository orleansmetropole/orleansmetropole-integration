<nav class="quicklink text-center">
  <h2 class="sr-only">Accès rapides</h2>
  <div class="well dashboard">
    <ul class="list-unstyled row ">
      <li class="col-xs-6 col-sm-4 col-lg-6"> <a href="https://www.espace-citoyens.net/orleans/espace-citoyens/CompteCitoyen" class="btn btn-default "><span class="fa fa-child rounded"></span><br>
          Famille</a>
      </li>
      <li class="col-xs-6 col-sm-4 col-lg-6"> <a href="demandes.php" class="btn btn-default "><span class="fa fa-file rounded"></span><br>
          Mes demandes</a>
      </li>
      <li class="col-xs-6 col-sm-4 col-lg-6"> <a href="contact.php" class="btn btn-default"><span class="fa fa-envelope rounded"></span><br>
          Contact</a>
      </li>
      <li class="col-xs-12 col-sm-4 col-lg-12">
        <div class="dropup">
          <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuCompte" data-toggle="dropdown" aria-expanded="true">
            <span class="fa  fa-user inverse rounded"></span>
            <br>Mon compte
          </button>
          <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenuCompte">
            <li role="presentation"><a role="menuitem" tabindex="-1" href="compte.php"><span class="fa  fa-pencil"></span> Modifier mes informations</a>
            </li>
            <li role="presentation"><a role="menuitem" tabindex="-1" href="index.php"><span class="fa  fa-close"></span> Se déconnecter</a>
            </li>
          </ul>
        </div>
      </li>
    </ul>
  </div>
</nav>
<div class="well sidebar">
  <div class="row entete">
    <h3>Suivre une demande ?</h3>
  </div>
  <form class="form-horizontal text-center">
    <div class="form-group input-group">
      <input type="text" class="form-control" id="inputCode" placeholder="Code">
      <span class="input-group-btn">
        <button type="submit" class="btn btn-primary align-center"><span class="fa fa-search"></span></button>
      </span>
    </div>
  </form>
</div>