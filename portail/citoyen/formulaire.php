<?php
require_once "config.php";
$info = "Formulaire style";
$is_connected = false;
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>Portail citoyen Orléans - Accueil</title>
  <?php include( 'blocs/styles.php') ?>

<body>
<?php include( 'blocs/header.php') ?>
        <div class="container formulaire">
            <div class="row">
                <div class="col-lg-9">
                    <ol class="breadcrumb hidden-xs">
                        <li><a href="index.php">Accueil</a>
                        </li>
                        <li><a href="#">Formulaire de contact</a>
                        </li>
                    </ol>
                    <section>
                        <header class="header-article accroche">
                            <h1>Demande de contact</h1>
                            <div class="infos-formulaire"><strong>Vous pouvez faire une demande :</strong>
                                <ul>
                                    <li>lorsqu'il y a une gêne à la circulation (véhicules, cycles, piétons),</li>
                                    <li>lorsqu'il y a besoin impératif de neutraliser du stationnement,</li>
                                    <li>lorsqu'il n'y a pas de stationnement.</li>
                                </ul>
                                <p>Veuillez effectuer votre demande 15 jours ouvrés&nbsp;avant le début de l'intervention.</p>
                            </div>
                        </header>
                        <div class="alert alert-success" role="alert">
                            <span class="fa fa-ok" aria-hidden="true"></span>
                            <span class="sr-only">Succès :</span>
                            Le formulaire a été envoyé avec succès.
                        </div>
                        <div class="alert alert-danger hidden" role="alert">
                            <span class="fa fa-ok" aria-hidden="true"></span>
                            <span class="sr-only">Erreur :</span>
                            Un problème est survenu.
                        </div>
                        <div class="alert alert-info hidden" role="alert">
                            <span class="fa fa-ok" aria-hidden="true"></span>
                            <span class="sr-only">Information :</span>
                            Les champs précédé d'une * sont obligatoires.
                        </div>
                        <h2 class="sr-only">Formulaire de demande</h2>


                        <form class="form-horizontal" method="post">
                            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="headingOne">
                                        <h3 class="facture">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="tab-form">Demandeur</a>
                                        </h3>
                                        <div class="panel-actions">
                                            <h4><i class="fa fa-caret-down"></i></h4>
                                        </div>
                                    </div>
                                    <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                        <div class="panel-body">
                                            <div class="require">
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">*Vous êtes</label>
                                                    <div class="col-md-8">
                                                        <div class="radio">
                                                            <label>
                                                                <input type="radio" class="tx-artificaform-pi1-2782-condition-field1385635257-0" value="une entreprise" name="tx_artificaform_pi1[field2]">une entreprise
                                                            </label>
                                                            <br>
                                                            <label>
                                                                <input type="radio" class="tx-artificaform-pi1-2782-condition-field1385635257-1" value="un particulier" name="tx_artificaform_pi1[field2]">un particulier
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="require">
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">*Adresse</label>
                                                    <div class="col-md-8">
                                                        <textarea class="form-control" rows="3" placeholder="Adresse"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="require">
                                                <div class="form-group has-success">
                                                    <label class="control-label col-md-4" for="inputSuccess1">*Code postal</label>
                                                    <div class="col-md-8">
                                                        <input type="text" class="form-control" placeholder="Code postal" id="inputSuccess1">
                                                        <span class="fa fa-ok form-control-feedback" aria-hidden="true"></span>
                                                        <span id="inputSuccess1Status" class="sr-only">(success)</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="require">
                                                <div class="form-group has-error">
                                                    <label class="control-label col-md-4" for="inputError1">*Ville</label>
                                                    <div class="col-md-8">
                                                        <input class="form-control" placeholder="Ville" id="inputError1">
                                                        <span class="fa fa-remove form-control-feedback" aria-hidden="true"></span>
                                                        <span id="inputError1Status" class="sr-only">(error)</span>
                                                        <p class="help-block error">Vous n'avez pas saisi correctement ce champ</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="require">
                                                <div class="form-group">
                                                    <div class="col-md-4 col-md-push-4">
                                                        <p class="comments"><span class="fa fa-info-circle fa-3x info pull-left"></span> <span class="sr-only">A savoir :</span> <em>Nous pouvons vous livrer de 1 à 3 bacs 2 roues (240L), si vous voulez plus de bacs ou des bacs 4 roues, veuillez renseigner le document associé (voir pour renvoyer vers le .pdf qui serait hébergé sur le site)</em></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="require">
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">*Téléphone</label>
                                                    <div class="col-md-8">
                                                        <input class="form-control" placeholder="Téléphone">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-4">Fax</label>
                                                <div class="col-md-8">
                                                    <input class="form-control" placeholder="Fax">
                                                </div>
                                            </div>
                                            <div class="require">
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">*Email</label>
                                                    <div class="col-md-8">
                                                        <input type="email" class="form-control" id="inputEmail3" placeholder="Email">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-4">Pour le compte de</label>
                                                <div class="col-md-8">
                                                    <input class="form-control" placeholder="Pour le compte de">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-4">Nom du responsable</label>
                                                <div class="col-md-8">
                                                    <input class="form-control" placeholder="Nom du responsable">
                                                </div>
                                            </div>
                                            <div class="require">
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">*Dossier suivi par la Ville d'Orléans ou l'agglomération</label>
                                                    <div class="col-md-8">
                                                        <div class="radio">
                                                            <label>
                                                                <input type="radio" class="tx-artificaform-pi1-2782-condition-field1385635257-0" value="oui" name="tx_artificaform_pi1[field2]">oui
                                                            </label>
                                                            <br>
                                                            <label>
                                                                <input type="radio" class="tx-artificaform-pi1-2782-condition-field1385635257-1" value="non" name="tx_artificaform_pi1[field2]">non
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="headingTwo">
                                        <h3 class="facture">
                                            <a class="collapsed tab-form" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">Informations sur les travaux</a>
                                        </h3>
                                        <div class="panel-actions">
                                            <h4><i class="fa fa-caret-down"></i></h4>
                                        </div>
                                    </div>
                                    <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                        <div class="panel-body">
                                            <div class="form-group">
                                                <label class="control-label col-md-4">N° de dossier ville d’Orléans</label>
                                                <div class="col-md-8">
                                                    <input class="form-control" placeholder="N° de dossier ville d’Orléans">
                                                </div>
                                            </div>
                                            <div class="require">
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">*Lieu des travaux</label>
                                                    <div class="col-md-8">
                                                        <input class="form-control" placeholder="Lieu des travaux">
                                                        <p class="help-block">Indiquez ici le(s) nom(s) de rue(s) concernée(s), en précisant les limites éventuelles de tronçons, en les répétants soit par le nom des rues des intersections, ou par des adresses postales en utilisant au besoin les points cardinaux. Préciser si nécessaire avec un plan ou un croquis.</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-4">Croquis</label>
                                                <div class="col-md-8">
                                                    <input type="file" id="exampleInputFile">
                                                    <p class="help-block">(3000 Ko max,jpg,png,doc,pdf)</p>
                                                </div>
                                            </div>
                                            <div class="require">
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">*Date de début</label>
                                                    <div class="col-md-8">
                                                        <div class="input-group date">
                                                            <input class="form-control" placeholder="jj/mm/aaaa">
                                                            <span class="input-group-addon">
                                                    <span class="fa fa-calendar"></span>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="require">
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">*Date de fin</label>
                                                    <div class="col-md-8">
                                                        <div class="input-group date">
                                                            <input class="form-control" placeholder="jj/mm/aaaa">
                                                            <span class="input-group-addon">
                                                    <span class="fa fa-calendar"></span>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="require hidden">
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">*Date de début</label>
                                                    <div class="raw">
                                                        <div class="col-md-1">
                                                            <select class="form-control">
                                                                <option value="" selected disabled>Jour</option>
                                                                <option value="">1</option>
                                                                <option value="">2</option>
                                                                <option value="">3</option>
                                                                <option value="">4</option>
                                                                <option value="">5</option>
                                                                <option value="">6</option>
                                                                <option value="">7</option>
                                                                <option value="">8</option>
                                                                <option value="">9</option>
                                                                <option value="">10</option>
                                                                <option value="">11</option>
                                                                <option value="">12</option>
                                                                <option value="">13</option>
                                                                <option value="">14</option>
                                                                <option value="">15</option>
                                                                <option value="">16</option>
                                                                <option value="">17</option>
                                                                <option value="">18</option>
                                                                <option value="">19</option>
                                                                <option value="">20</option>
                                                                <option value="">21</option>
                                                                <option value="">22</option>
                                                                <option value="">23</option>
                                                                <option value="">24</option>
                                                                <option value="">25</option>
                                                                <option value="">26</option>
                                                                <option value="">27</option>
                                                                <option value="">28</option>
                                                                <option value="">29</option>
                                                                <option value="">30</option>
                                                                <option value="">31</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-md-1">
                                                            <select class="form-control">
                                                                <option value="" selected disabled>Mois</option>
                                                                <option value="">Janvier</option>
                                                                <option value="">Février</option>
                                                                <option value="">Mars</option>
                                                                <option value="">Avril</option>
                                                                <option value="">Mai</option>
                                                                <option value="">Juin</option>
                                                                <option value="">Juillet</option>
                                                                <option value="">Août</option>
                                                                <option value="">Septembre</option>
                                                                <option value="">Octobre</option>
                                                                <option value="">Novembre</option>
                                                                <option value="">Décembre</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <select class="form-control">
                                                                <option value="" selected disabled>Année</option>
                                                                <option value="">2015</option>
                                                                <option value="">2016</option>
                                                                <option value="">2017</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <p class="help-block">15 jours avant le début des travaux</p>
                                                </div>
                                            </div>
                                            <div class="require hidden">
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">*Date de fin</label>
                                                    <div class="raw">
                                                        <div class="col-md-1">
                                                            <select class="form-control">
                                                                <option value="" selected disabled>Jour</option>
                                                                <option value="">1</option>
                                                                <option value="">2</option>
                                                                <option value="">3</option>
                                                                <option value="">4</option>
                                                                <option value="">5</option>
                                                                <option value="">6</option>
                                                                <option value="">7</option>
                                                                <option value="">8</option>
                                                                <option value="">9</option>
                                                                <option value="">10</option>
                                                                <option value="">11</option>
                                                                <option value="">12</option>
                                                                <option value="">13</option>
                                                                <option value="">14</option>
                                                                <option value="">15</option>
                                                                <option value="">16</option>
                                                                <option value="">17</option>
                                                                <option value="">18</option>
                                                                <option value="">19</option>
                                                                <option value="">20</option>
                                                                <option value="">21</option>
                                                                <option value="">22</option>
                                                                <option value="">23</option>
                                                                <option value="">24</option>
                                                                <option value="">25</option>
                                                                <option value="">26</option>
                                                                <option value="">27</option>
                                                                <option value="">28</option>
                                                                <option value="">29</option>
                                                                <option value="">30</option>
                                                                <option value="">31</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-md-1">
                                                            <select class="form-control">
                                                                <option value="" selected disabled>Mois</option>
                                                                <option value="">Janvier</option>
                                                                <option value="">Février</option>
                                                                <option value="">Mars</option>
                                                                <option value="">Avril</option>
                                                                <option value="">Mai</option>
                                                                <option value="">Juin</option>
                                                                <option value="">Juillet</option>
                                                                <option value="">Août</option>
                                                                <option value="">Septembre</option>
                                                                <option value="">Octobre</option>
                                                                <option value="">Novembre</option>
                                                                <option value="">Décembre</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <select class="form-control">
                                                                <option value="" selected disabled>Année</option>
                                                                <option value="">2015</option>
                                                                <option value="">2016</option>
                                                                <option value="">2017</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="require">
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">*Durée effective des travaux</label>
                                                    <div class="col-md-8">
                                                        <input class="form-control" placeholder="Durée effective des travaux">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="require">
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">*Chantier nocturne</label>
                                                    <div class="col-md-8">
                                                        <div class="row">
                                                            <div class="col-md-2 radio">
                                                                <label>
                                                                    <input type="radio" value="oui" name="tx_artificaform_pi1[field2]">oui
                                                                </label>
                                                                <br>
                                                                <label>
                                                                    <input type="radio" value="non" name="tx_artificaform_pi1[field2]">non
                                                                </label>
                                                            </div>
                                                            <div class="col-md-10">
                                                                <p class="help-block">entre 20h00 et 7h00</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="require">
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">*Durée effective des travaux</label>
                                                    <div class="col-md-8">
                                                        <input class="form-control" placeholder="Durée effective des travaux">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="require">
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">*Nature des travaux</label>
                                                    <div class="raw checkbox">
                                                        <div class="col-md-2">
                                                            <label>
                                                                <input type="checkbox" value="">eau
                                                            </label>
                                                            <br>
                                                            <label>
                                                                <input type="checkbox" value="">assainissement
                                                            </label>
                                                            <br>
                                                            <label>
                                                                <input type="checkbox" value="">gaz
                                                            </label>
                                                            <br>
                                                            <label>
                                                                <input type="checkbox" value="">électricité
                                                            </label>
                                                            <br>
                                                            <label>
                                                                <input type="checkbox" value="">téléphone
                                                            </label>
                                                            <br>
                                                            <label>
                                                                <input type="checkbox" value="">réseau câblé
                                                            </label>
                                                            <br>
                                                            <label>
                                                                <input type="checkbox" value="">éclairage public
                                                            </label>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <label>
                                                                <input type="checkbox" value="">réfection de chaussée
                                                            </label>
                                                            <br>
                                                            <label>
                                                                <input type="checkbox" value="">réfection de trottoir
                                                            </label>
                                                            <br>
                                                            <label>
                                                                <input type="checkbox" value="">espaces verts
                                                            </label>
                                                            <br>
                                                            <label>
                                                                <input type="checkbox" value="">déménagement
                                                            </label>
                                                            <br>
                                                            <label>
                                                                <input type="checkbox" value="">nettoyage de vitres
                                                            </label>
                                                            <br>
                                                            <label>
                                                                <input type="checkbox" value="">autres
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-4">Autres à préciser</label>
                                                <div class="col-md-8">
                                                    <input class="form-control" placeholder="Autres">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="headingThree">
                                        <h3 class="facture">
                                            <a class="collapsed tab-form" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">Mesures de circulation sollicitées</a>
                                        </h3>
                                        <div class="panel-actions">
                                            <h4><i class="fa fa-caret-down"></i></h4>
                                        </div>
                                    </div>
                                    <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                        <div class="panel-body">
                                            <div class="require">
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">*Voie(s)</label>
                                                    <div class="col-md-8">
                                                        <textarea class="form-control" rows="3" placeholder="Voie(s)"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-4">Intervention sur une rue où passe le tramway</label>
                                                <div class="col-md-8">
                                                    <div class="radio">
                                                        <label>
                                                            <input type="radio" value="oui" name="tx_artificaform_pi1[field2]">oui
                                                        </label>
                                                        <br>
                                                        <label>
                                                            <input type="radio" value="non" name="tx_artificaform_pi1[field2]">non
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="require">
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">*Stationnement à interdire</label>
                                                    <div class="col-md-8">
                                                        <div class="radio">
                                                            <label>
                                                                <input type="radio" value="" name="tx_artificaform_pi1[field2]">aucun
                                                            </label>
                                                            <br>
                                                            <label>
                                                                <input type="radio" value="" name="tx_artificaform_pi1[field2]">côté n° pairs
                                                            </label>
                                                            <br>
                                                            <label>
                                                                <input type="radio" value="" name="tx_artificaform_pi1[field2]">côté n° impairs
                                                            </label>
                                                            <br>
                                                            <label>
                                                                <input type="radio" value="" name="tx_artificaform_pi1[field2]">des deux côtés
                                                            </label>
                                                            <br>
                                                            <label>
                                                                <input type="radio" value="" name="tx_artificaform_pi1[field2]">du n° au n°
                                                            </label>
                                                            <br>
                                                            <label>
                                                                <input type="radio" value="" name="tx_artificaform_pi1[field2]">sur quelques mètres
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="require">
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">*Rue barrée</label>
                                                    <div class="col-md-8">
                                                        <div class="radio">
                                                            <label>
                                                                <input type="radio" value="oui" name="tx_artificaform_pi1[field2]">oui
                                                            </label>
                                                            <br>
                                                            <label>
                                                                <input type="radio" value="non" name="tx_artificaform_pi1[field2]">non
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="require">
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">*Suppression d'un sens de circulation</label>
                                                    <div class="col-md-8">
                                                        <div class="radio">
                                                            <label>
                                                                <input type="radio" value="oui" name="tx_artificaform_pi1[field2]">oui
                                                            </label>
                                                            <br>
                                                            <label>
                                                                <input type="radio" value="non" name="tx_artificaform_pi1[field2]">non
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="require">
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">*Chaussée rétrécie</label>
                                                    <div class="col-md-8">
                                                        <div class="radio">
                                                            <label>
                                                                <input type="radio" value="oui" name="tx_artificaform_pi1[field2]">oui
                                                            </label>
                                                            <br>
                                                            <label>
                                                                <input type="radio" value="non" name="tx_artificaform_pi1[field2]">non
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="require">
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">*Neutralisation d'un couloir de circulation</label>
                                                    <div class="col-md-8">
                                                        <div class="radio">
                                                            <label>
                                                                <input type="radio" value="oui" name="tx_artificaform_pi1[field2]">oui
                                                            </label>
                                                            <br>
                                                            <label>
                                                                <input type="radio" value="non" name="tx_artificaform_pi1[field2]">non
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-4">Limitation de vitesse</label>
                                                <div class="col-md-8">
                                                    <div class="radio">
                                                        <label>
                                                            <input type="radio" value="oui" name="tx_artificaform_pi1[field2]">oui
                                                        </label>
                                                        <br>
                                                        <label>
                                                            <input type="radio" value="non" name="tx_artificaform_pi1[field2]">non
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="require">
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">*Circulation alternée</label>
                                                    <div class="col-md-8">
                                                        <div class="radio">
                                                            <label>
                                                                <input type="radio" value="oui" name="tx_artificaform_pi1[field2]">oui
                                                            </label>
                                                            <br>
                                                            <label>
                                                                <input type="radio" value="non" name="tx_artificaform_pi1[field2]">non
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="require">
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">*Circulation des vélos</label>
                                                    <div class="col-md-8">
                                                        <div class="radio">
                                                            <label>
                                                                <input type="radio" value="" name="tx_artificaform_pi1[field2]">maintenue
                                                            </label>
                                                            <br>
                                                            <label>
                                                                <input type="radio" value="" name="tx_artificaform_pi1[field2]">supprimée
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="require">
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">*Circulation des piétons</label>
                                                    <div class="col-md-8">
                                                        <div class="radio">
                                                            <label>
                                                                <input type="radio" value="oui" name="tx_artificaform_pi1[field2]">oui
                                                            </label>
                                                            <br>
                                                            <label>
                                                                <input type="radio" value="non" name="tx_artificaform_pi1[field2]">non
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-4">Autres mesures sollicitées</label>
                                                <div class="col-md-8">
                                                    <textarea class="form-control" rows="3" placeholder="Autres mesures sollicitées"></textarea>
                                                </div>
                                            </div>
                                            <div class="require">
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">*Informations riverains</label>
                                                    <div class="col-md-8">
                                                        <div class="radio">
                                                            <label>
                                                                <input type="radio" value="" name="tx_artificaform_pi1[field2]">pas nécessaire
                                                            </label>
                                                            <br>
                                                            <label>
                                                                <input type="radio" value="" name="tx_artificaform_pi1[field2]">prévue
                                                            </label>
                                                            <br>
                                                            <label>
                                                                <input type="radio" value="" name="tx_artificaform_pi1[field2]">médias utilisés
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="headingFour">
                                        <h3 class="facture">
                                            <a class="collapsed tab-form" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">Validation</a>
                                        </h3>
                                        <div class="panel-actions">
                                            <h4><i class="fa fa-caret-down"></i></h4>
                                        </div>
                                    </div>
                                    <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
                                        <div class="panel-body">
                                            <p class="obligatoires">Les demandes sont à adresser au minimum 15 jours ouvrables avant l'intervention.</p>
                                            <p class="text-center">Pour valider le formulaire, saisissez les <strong>4 derniers caractères</strong> de la série.</p>
                                            <ul class="list-inline list-unstyled text-center">
                                                <li>Q</li>
                                                <li>N</li>
                                                <li>L</li>
                                                <li>K</li>
                                                <li>6</li>
                                                <li>M</li>
                                                <li>X</li>
                                                <li>S</li>
                                            </ul>
                                            <div class="require">
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">*La clé de validation</label>
                                                    <div class="col-md-8">
                                                        <input class="form-control" placeholder="La clé de validation">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <p class="text-right obligatoires">*Champs obligatoires</p>
                            <p class="text-center"><a href="#" class="btn btn-primary btn-lg"><span class="fa fa-send"></span> Valider</a>
                            </p>
                        </form>
                    </section>
                </div>

                <div class="col-lg-3">
                  <?php include('blocs/sidebar.php') ?>
                </div>
            </div>
        </div>
        <?php include( 'blocs/footer.php'); ?>
        <?php include( 'blocs/scripts.php'); ?>
    </body>

</html>



