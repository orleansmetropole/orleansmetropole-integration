<?php
require_once "config.php";
$info = "Page de connexion";
$is_connected = false;
?>
<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
    <title>Portail citoyen Orléans - Connexion</title>
  <?php include( 'blocs/styles.php') ?>
</head>

<body>
  <?php include( 'blocs/header.php') ?>
  <div class="container connexion">
  <section>
    <div class="row">
      <div class="col-md-12">
        <ol class="breadcrumb hidden-xs">
          <li><a href="index.php">Accueil</a>
          </li>
          <li class="active">Page de connexion</li>
        </ol>
        <div class="infos">
          <br>
          <form class="form-horizontal" method="post">
            <div class="form-group">
              <label class="control-label col-md-4">Identifiant</label>
              <div class="col-md-4">
                <input type="email" class="form-control" placeholder="Identifiant">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-4">Mot de passe</label>
              <div class="col-md-4">
                <input type="password" class="form-control" placeholder="Mot de passe">
              </div>
            </div>
            <div class="checkbox">
              <div class="col-md-8 pull-right">
                <label>
                  <input type="checkbox">Se souvenir du mot de passe.
                </label>
              </div>
            </div>
            <br>
            <p class="text-center">
              <input class="btn btn-warning btn-lg text-uppercase" type="submit" value="Se connecter">
            </p>
          </form>
        </div>
      </div>
    </div>
    </scetion>
  </div>
  <?php include( 'blocs/footer.php'); ?>
  <?php include( 'blocs/scripts.php'); ?>
</body>

</html>

