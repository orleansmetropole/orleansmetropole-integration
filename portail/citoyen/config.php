<?php
/**
 * Created by PhpStorm.
 * User: geoffroycochard
 * Date: 13/02/2018
 * Time: 11:37
 */
$is_connected = false;
function debug($datas, $die = false) {
  echo '<pre>';
  var_dump($datas);
  echo '</pre>';
  if($die) die();
}


require_once "src/Provider/MenuProvider.php";

$menu = new MenuProvider(__DIR__."/Publik_Ergo - ergo.csv");

//debug($menu,1);
function getPathUrl($path) {
  return $path;
}

$populars = [
  'etat-civil' => [
    'label' => 'Actes d\'état civil',
    'url' => 'https://moncompte.orleans-metropole.fr/demarches/',
    'icon' => 'file-text-o',
    'description' => 'Acte de naissance, acte de mariage, ...',
  ],
  'famille' => [
    'label' => 'Espace Famille',
    'url' => '',
    'icon' => 'child',
    'description' => 'Inscription accueil de loisirs, Orléans vous coach',
  ],
  'autorisation' => [
    'label' => 'Demande d\'arrêté de circulation',
    'url'   => 'http://www.orleans-metropole.fr/1210/demande-darrete-de-circulation-travaux-demenagement.htm',
    'icon'  => 'road',
    'description' => 'travaux, déménagement...',
  ],
];

$sub_menus = [
  'famille' => [
    'label' => 'Famille',
    'url' => '',
    'icon' => 'child'
  ],
  'demandes' => [
    'label' => 'Mes demandes',
    'url' => 'demandes.php',
    'icon' => 'file'
  ],
  'contact' => [
    'label' => 'Contact',
    'url' => 'contact',
    'icon' => 'envelope'
  ]
];

$main_menus = [
  'mes-demarches' => [
    'label' => 'Mes demarches',
    'url' => 'index.php'
  ],
  'mes-demandes' => [
    'label' => 'Mes demandes',
    'url' => 'demandes.php'
  ],
  'contact' => [
    'label' => 'Contact',
    'url' => 'contact.php'
  ],
  'conditions' => [
    'label' => 'Conditions d\'utilisation',
    'url' => 'conditions.php'
  ]
];

$menus = [
  'famille' => [
    'label' => 'Famille',
    'icon'  => 'child',
    'items' => [
      'inscription_sportive' => [
        'label' => 'Inscription sportive',
        'url'   => 'https://www.espace-citoyens.net/orleans/espace-citoyens/CompteCitoyen',
        'description' => null,
        'connected' => true
      ],
      'inscription_creche' => [
        'label' => 'Inscription en crèche',
        'url'   => 'https://www.espace-citoyens.net/orleans/espace-citoyens/CompteCitoyen',
        'description' => null,
        'connected' => true
      ],
      'inscription_alsh' => [
        'label' => 'Inscription accueil de loisirs',
        'url'   => 'https://www.espace-citoyens.net/orleans/espace-citoyens/CompteCitoyen',
        'description' => null,
        'connected' => true
      ],
    ]
  ],
  'etat-civil' => [
    'label' => 'Etat civil',
    'icon'  => 'file-text-o',
    'items' => [
      'acte' => [
        'label' => 'Demande d\'actes',
        'url'   => 'https://moncompte.orleans-metropole.fr/demarches/',
        'description' => null,
        'connected' => false
      ],
      'livret_famille' => [
        'label' => 'Livret de famille',
        'url'   => 'https://moncompte.orleans-metropole.fr/demarches/',
        'description' => null,
        'connected' => false
      ]
    ]
  ],
  'piece_identite' => [
    'label' => 'Pièces d\'identité',
    'icon'  => 'address-card-o',
    'items' => [
      'passeport' => [
        'label' => 'Informations passeport',
        'url'   => 'https://moncompte.orleans-metropole.fr/demarches/',
        'description' => null,
        'connected' => false
      ],
      'cni' => [
        'label' => 'Informations carte d\'identité',
        'url'   => 'https://moncompte.orleans-metropole.fr/demarches/',
        'description' => null,
        'connected' => true
      ],
      'attestation_accueil' => [
        'label' => 'RDV attestation d\'accueil',
        'url'   => 'https://teleservices.moncompte.orleans-metropole.fr/affaires-generales/RDV_ADA/',
        'description' => null,
        'connected' => false
      ]
    ],
  ],
  'commerce' => [
    'label' => 'Commerce',
    'icon'  => 'credit-card',
    'items' => [
      'liquidation' => [
        'label' => 'Vente en liquidation',
        'url'   => 'https://moncompte.orleans-metropole.fr/demarches/',
        'description' => null,
        'connected' => false
      ],
      'boissons' => [
        'label' => 'Débit de boisson',
        'url'   => 'https://teleservices.moncompte.orleans-metropole.fr/affaires-generales/debit-de-boissons-temporaire/',
        'description' => null,
        'connected' => false
      ]
    ]
  ],
  'intervention' => [
    'label' => 'Intervention',
    'icon'  => 'road',
    'items' => [
      'autorisation' => [
        'label' => 'Demande d\'arrêté de circulation (travaux, déménagement...)',
        'url'   => 'http://www.orleans-metropole.fr/1210/demande-darrete-de-circulation-travaux-demenagement.htm',
        'description' => null,
        'connected' => true
      ],
      'eclairage' => [
        'label' => 'Eclairage',
        'url'   => 'http://www.orleans-metropole.fr/510/eclairage.htm',
        'description' => null,
        'connected' => false
      ],
      'voirie' => [
        'label' => 'Voirie dégradé',
        'url'   => 'http://www.orleans-metropole.fr/513/voirie-degradee.htm',
        'description' => null,
        'connected' => false
      ]
    ]
  ],
  'dechet' => [
    'label' => 'Déchets',
    'icon'  => 'trash',
    'items' => [
      'demande-de-bac' => [
        'label' => 'Demande de bacs',
        'url'   => 'http://www.orleans-metropole.fr/1449/demande-de-bac-les-collectes-sajustent.htm',
        'description' => null,
        'connected' => false
      ],
      'reparation' => [
        'label' => 'Réparation de bac',
        'url'   => 'http://www.orleans-metropole.fr/1450/reparation-de-bac.htm',
        'description' => null,
        'connected' => false
      ]
    ]
  ],
  'logement' => [
    'label' => 'Logement / habitat',
    'icon'  => 'home',
    'items' => [
      'information' => [
        'label' => 'Demande d\'information',
        'url'   => 'http://www.orleans-metropole.fr/815/demande-dinformation-logement-social.htm',
        'description' => null,
        'connected' => false
      ],
      'urbanisame' => [
        'label' => 'Demande de réglementation d\'urbanisame',
        'url'   => 'http://www.orleans-metropole.fr/816/demande-de-reglementation-durbanisme.htm',
        'description' => null,
        'connected' => false
      ]
    ]
  ],
  'emploi' => [
    'label' => 'Emploi / Stage',
    'icon'  => 'suitcase',
    'items' => [
      'candidature' => [
        'label' => 'Dépôt de candidatures',
        'url'   => 'http://www.orleans-metropole.fr/1449/demande-de-bac-les-collectes-sajustent.htm',
        'description' => 'Présenter sa candidature à une offre d\'emploi, un stage ou une alternance ...',
        'connected' => false
      ]
    ]
  ],
  'association' => [
    'label' => 'Vie associative',
    'icon'  => 'th-large',
    'items' => [
      'demande-de-bac' => [
        'label' => 'Demande d\'informations',
        'url'   => 'http://www.orleans-metropole.fr/1449/demande-de-bac-les-collectes-sajustent.htm',
        'description' => 'Faire une demande ou poser une question au service des associations ...',
        'connected' => false
      ]
    ]
  ],
  'tourisme' => [
    'label' => 'Tourisme',
    'icon'  => 'sun-o',
    'items' => [
      'renseignement' => [
        'label' => 'Demande de renseignement',
        'url'   => 'http://www.orleans-metropole.fr/1521/demande-de-renseignement-touristique.htm',
        'description' => null,
        'connected' => false
      ],
      'taxe' => [
        'label' => 'Taxe de séjour',
        'url'   => 'https://taxesejour.orleans-metropole.fr/',
        'description' => null,
        'connected' => false
      ],
      'meuble' => [
        'label' => 'Meublé de tourisme',
        'url'   => 'https://teleservices.moncompte.orleans-metropole.fr/affaires-generales/meuble-de-tourisme/',
        'description' => null,
        'connected' => false
      ]
    ]
  ],
  'contact' => [
    'label' => 'Nous contacter',
    'icon'  => 'envelope-o',
    'items' => [
      'contact' => [
        'label' => 'Formulaire de contact',
        'url'   => 'http://www.orleans-metropole.fr/822/contacter-la-mairie-dorleans-ou-orleans-metropole.htm',
        'description' => null,
        'connected' => false
      ]
    ]
  ],

];
//debug($menu->getMenu(),1);
$menus = $menu->getMenu();

// Var header
$header = !empty($_GET['h']) ? $_GET['h'] : 1;