<?php
require_once "config.php";
$info = "Formulaire style";
$is_connected = false;
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>Portail citoyen Orléans - Accueil</title>
  <?php include( 'blocs/styles.php') ?>

<body>
<?php include( 'blocs/header.php') ?>
        <div class="container formulaire">
            <div class="row">
                <div class="col-lg-9">
                    <ol class="breadcrumb hidden-xs">
                        <li><a href="index.php">Accueil</a>
                        </li>
                        <li><a href="#">Formulaire de contact</a>
                        </li>
                    </ol>
                    <section>
                        <header class="header-article accroche">
                            <h1>Demande de contact</h1>
                        </header>
                        <div class="alert alert-success" role="alert">
                            <span class="fa fa-ok" aria-hidden="true"></span>
                            <span class="sr-only">Succès :</span>
                            Le formulaire a été envoyé avec succès.
                        </div>
                        <h2 class="sr-only">Formulaire de demande</h2>


                        <form class="form-horizontal" method="post">
                            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="headingOne">
                                        <h3 class="facture">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="tab-form">Votre demande</a>
                                        </h3>
                                        <div class="panel-actions">
                                            <h4><i class="fa fa-caret-down"></i></h4>
                                        </div>
                                    </div>
                                    <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                        <div class="panel-body">
                                            <div class="require">
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">*Civilité</label>
                                                    <div class="col-md-8">
                                                        <div class="radio">
                                                            <label>
                                                                <input type="radio" class="tx-artificaform-pi1-2782-condition-field1385635257-0" value="une entreprise" name="tx_artificaform_pi1[field2]">Monsieur
                                                            </label>
                                                            <br>
                                                            <label>
                                                                <input type="radio" class="tx-artificaform-pi1-2782-condition-field1385635257-1" value="un particulier" name="tx_artificaform_pi1[field2]">Madame
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="require">
                                                <div class="form-group has-success">
                                                    <label class="control-label col-md-4" for="inputSuccess1">*Prénom</label>
                                                    <div class="col-md-8">
                                                        <input type="text" class="form-control" placeholder="Prénom" id="inputSuccess1">
                                                        <span class="fa fa-ok form-control-feedback" aria-hidden="true"></span>
                                                        <span id="inputSuccess1Status" class="sr-only">(success)</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="require">
                                                <div class="form-group has-success">
                                                    <label class="control-label col-md-4" for="inputSuccess1">*Nom</label>
                                                    <div class="col-md-8">
                                                        <input type="text" class="form-control" placeholder="Code postal" id="inputSuccess1">
                                                        <span class="fa fa-ok form-control-feedback" aria-hidden="true"></span>
                                                        <span id="inputSuccess1Status" class="sr-only">(success)</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="require">
                                                <div class="form-group has-error">
                                                    <label class="control-label col-md-4" for="inputError1">*Ville</label>
                                                    <div class="col-md-8">
                                                        <input class="form-control" placeholder="Ville" id="inputError1">
                                                        <span class="fa fa-remove form-control-feedback" aria-hidden="true"></span>
                                                        <span id="inputError1Status" class="sr-only">(error)</span>
                                                        <p class="help-block error">Vous n'avez pas saisi correctement ce champ</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="require">
                                                <div class="form-group">
                                                    <div class="col-md-4 col-md-push-4">
                                                        <p class="comments"><span class="fa fa-info-circle fa-3x info pull-left"></span> <span class="sr-only">A savoir :</span> <em>Nous pouvons vous livrer de 1 à 3 bacs 2 roues (240L), si vous voulez plus de bacs ou des bacs 4 roues, veuillez renseigner le document associé (voir pour renvoyer vers le .pdf qui serait hébergé sur le site)</em></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="require">
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">*Téléphone</label>
                                                    <div class="col-md-8">
                                                        <input class="form-control" placeholder="Téléphone">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-4">Fax</label>
                                                <div class="col-md-8">
                                                    <input class="form-control" placeholder="Fax">
                                                </div>
                                            </div>
                                            <div class="require">
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">*Email</label>
                                                    <div class="col-md-8">
                                                        <input type="email" class="form-control" id="inputEmail3" placeholder="Email">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-4">Pour le compte de</label>
                                                <div class="col-md-8">
                                                    <input class="form-control" placeholder="Pour le compte de">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-4">Nom du responsable</label>
                                                <div class="col-md-8">
                                                    <input class="form-control" placeholder="Nom du responsable">
                                                </div>
                                            </div>
                                            <div class="require">
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">*Dossier suivi par la Ville d'Orléans ou l'agglomération</label>
                                                    <div class="col-md-8">
                                                        <div class="radio">
                                                            <label>
                                                                <input type="radio" class="tx-artificaform-pi1-2782-condition-field1385635257-0" value="oui" name="tx_artificaform_pi1[field2]">oui
                                                            </label>
                                                            <br>
                                                            <label>
                                                                <input type="radio" class="tx-artificaform-pi1-2782-condition-field1385635257-1" value="non" name="tx_artificaform_pi1[field2]">non
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="headingFour">
                                        <h3 class="facture">
                                            <a class="collapsed tab-form" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">Validation</a>
                                        </h3>
                                        <div class="panel-actions">
                                            <h4><i class="fa fa-caret-down"></i></h4>
                                        </div>
                                    </div>
                                    <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
                                        <div class="panel-body">
                                            <p class="obligatoires">Les demandes sont à adresser au minimum 15 jours ouvrables avant l'intervention.</p>
                                            <p class="text-center">Pour valider le formulaire, saisissez les <strong>4 derniers caractères</strong> de la série.</p>
                                            <ul class="list-inline list-unstyled text-center">
                                                <li>Q</li>
                                                <li>N</li>
                                                <li>L</li>
                                                <li>K</li>
                                                <li>6</li>
                                                <li>M</li>
                                                <li>X</li>
                                                <li>S</li>
                                            </ul>
                                            <div class="require">
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">*La clé de validation</label>
                                                    <div class="col-md-8">
                                                        <input class="form-control" placeholder="La clé de validation">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <p class="text-right obligatoires">*Champs obligatoires</p>
                            <p class="text-center"><a href="#" class="btn btn-primary btn-lg"><span class="fa fa-send"></span> Valider</a>
                            </p>
                        </form>
                    </section>
                </div>

                <div class="col-lg-3">
                  <?php include('blocs/sidebar.php') ?>
                </div>
            </div>
        </div>
        <?php include( 'blocs/footer.php'); ?>
        <?php include( 'blocs/scripts.php'); ?>
    </body>

</html>



