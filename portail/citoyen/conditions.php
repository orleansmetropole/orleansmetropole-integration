<?php
require_once "config.php";
$info = "Formulaire style";
$is_connected = false;
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>Portail citoyen Orléans - Accueil</title>
  <?php include( 'blocs/styles.php') ?>

<body>
<?php include( 'blocs/header.php') ?>
        <div class="container formulaire">
            <div class="row">
                <div class="col-lg-9">
                    <ol class="breadcrumb hidden-xs">
                        <li><a href="index.php">Accueil</a>
                        </li>
                        <li><a href="#">Conditions d'utlisation</a>
                        </li>
                    </ol>
                    <section>
                        <header class="header-article accroche">
                            <h1>Conditions d'utilisation</h1>
                        </header>



                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus, beatae dolorem eos error excepturi illum laudantium minima neque pariatur porro quos reiciendis, repellat rerum sunt vel? Blanditiis eos inventore neque?</p>

                    </section>
                </div>

                <div class="col-lg-3">
                  <?php include('blocs/sidebar.php') ?>
                </div>
            </div>
        </div>
        <?php include( 'blocs/footer.php'); ?>
        <?php include( 'blocs/scripts.php'); ?>
    </body>

</html>



