<?php
/**
 * Created by PhpStorm.
 * User: geoffroycochard
 * Date: 22/10/2019
 * Time: 14:38
 */

/**
 * Class MenuProvider
 */
class MenuProvider
{
  /**
   * @var string
   */
  private $file;

  /**
   * @var array
   */
  private $menu;

  /**
   * MenuProvider constructor.
   * @param string $file
   */
  public function __construct(string $file)
  {
    $this->setFile($file);
    $this->prepare();
  }

  /**
   * @return string
   */
  public function getFile() : string
  {
    return $this->file;
  }

  /**
   * @param string $file
   * @return MenuProvider
   */
  public function setFile(string  $file) : MenuProvider
  {
    $this->file = $file;
    return $this;
  }

  /**
   * @param $text
   * @return string
   */
  private function sanitize($text) : string
  {
    // replace non letter or digits by -
    $text = preg_replace('~[^\pL\d]+~u', '-', $text);

    // transliterate
    $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

    // remove unwanted characters
    $text = preg_replace('~[^-\w]+~', '', $text);

    // trim
    $text = trim($text, '-');

    // remove duplicate -
    $text = preg_replace('~-+~', '-', $text);

    // lowercase
    $text = strtolower($text);

    if (empty($text)) {
      return 'n-a';
    }

    return $text;
  }

  /**
   *
   */
  private function prepare()
  {
    $a = [];
    $row = 1;
    if (($handle = fopen($this->getFile(), "r")) !== FALSE) {
      while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
        if (array_key_exists($this->sanitize($data[1]),$a)) {
          $items = $a[$this->sanitize($data[1])]['items'];
        } else {
          $a[$this->sanitize($data[1])] = [
            'label' => $data[1],
            'icon' => $data[6],
            'items' => null,
          ];
          $items = [];
        }
        $items[$this->sanitize($data[2])] = [
          'label' => $data[2],
          'url'   => $data[9],
          'description' => $data[7],
          'connected' => $data[8] === "TRUE" ? true : false
        ];

        $a[$this->sanitize($data[1])]['items'] = $items;

      }
      fclose($handle);
    }
    $this->menu = $a;
  }

  /**
   * @return array
   */
  public function getMenu(): array
  {
    return $this->menu;
  }

  /**
   * @param array $menu
   */
  public function setMenu(array $menu)
  {
    $this->menu = $menu;
  }

}