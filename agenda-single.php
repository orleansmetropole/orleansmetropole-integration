<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Le site d'Orléans et son AgglO - Liste d'événements</title>
<? include( 'blocs/styles.php') ?>
</head>
<body>
<? include( 'blocs/header.php') ?>
<div class="container agenda">
  <div class="row">
    <div class="col-md-8 col-lg-9">
      <ol class="breadcrumb hidden-xs">
        <li><a href="index.php">Accueil</a> </li>
        <li><a href="agenda-list.php">Agenda</a> </li>
        <li class="active">Je me souviens Ceux de 14…</li>
      </ol>
      <article>
        <header class="row header-article">
          <div class="col-sm-8">
            <h1>Je me souviens Ceux de 14…</h1>
            <span class="date">Du
            <time datetime="2015-03-28">Samedi 28 Mars 2015</time>
            au
            <time datetime="2015-06-07">Dimanche 07 Juin 2015</time>
            </span>
            <p class="badge-agenda"><span class="badge">Exposition&nbsp;</span> </p>
          </div>
          <div class="col-sm-4 hidden-xs">
            <? include( "blocs/social.php"); ?>
          </div>
        </header>
        <div class="row">
          <div class="col-sm-8">
            <p>Années de commémorations, de mémoire, de passation de flambeau aux jeunes générations. Le musée des Beaux-Arts met en lumière les destinées de Maurice Genevoix et des soldats de la Grande Guerre, à travers une exposition événement, « Maurice Genevoix, Je me souviens Ceux de 14… », visible du 28 mars au 7 juin.</p>
            <p>Panneaux didactiques, objets du fonds de l’écrivain, tels son uniforme de lieutenant ou encore sa tenue d’académicien, et objets militaires viennent plonger le visiteur dans cette époque trouble de notre histoire. Une manière de ne jamais oublier ceux tombés au combat et les autres, les soldats survivants, témoins des épreuves d’une génération sacrifiée, comme le romancier lui-même qui écrivit dans Ceux de 14, son recueil de récits de guerre : « Il faut que les vivants se retrouvent et se comptent, pour reprendre mieux possession les uns des autres, pour se serrer plus fort les uns contre les autres, se lier plus étroitement de toutes les récentes absences. »</p>
            <aside class="infos exhibit">
              <h2 class="h3">Contact pour cet évènement</h2>
              <h3>Edelweiss 45</h3>
              <p>Mini texte de présentation de l'organisateur ad lorem ipsum lorem dolorem sit amet et consequenter</p>
              <ul class="list-unstyled">
                <li><span class="fa fa-phone"> </span> 06.08.91.13.86</li>
                <li> <a href="http://www.edelweiss45.asso.fr" target="_blank"><span class="fa fa-link"></span> Site Web</a></li>
                <li> <a href="javascript:linkTo_UnCryptMailto('ocknvq,gfgnygkuu67Bgfgnygkuu670cuuq0ht');"><span class="fa fa-send"></span> Courriel</a></li>
                <li> <a href="/associations/repertoire/details/ficheAnnu/edelweiss-45-1.htm"><span class="fa fa-plus-circle"></span> d'infos sur l'association</a></li>
              </ul>
            </aside>
          </div>
          <div class="col-sm-4">
            <div class="thumbnail">
              <figure> <img class="img-responsive center-block" alt="Je me souviens Ceux de 14…" src="http://www.orleans.fr/typo3temp/pics/ef26f7afcc.jpg">
                <figcaption class="sr-only">Je me souviens Ceux de 14…</figcaption>
              </figure>
              <aside class="exhibit">
                <h2 class="h3">Infos pratiques</h2>
                <address>
                <p><strong>Lieux :</strong> <br>
                  Maison des Associations<br>
                </p>
                <p> <strong>Adresse :</strong><br>
                  46 ter, rue Sainte-Catherine<br>
                  45000 - Orléans</p>
                </address>
              </aside>
            </div>
          </div>
        </div>
      </article>
      <br>
      <div class="visible-xs text-center">
        <? include( "blocs/social.php"); ?>
      </div>
    </div>
    <div class="col-md-4 col-lg-3">
      <? include( "blocs/agenda-filter.php"); ?>
    </div>
  </div>
</div>
<? include( 'blocs/footer.php'); ?>
<? include( 'blocs/scripts.php'); ?>
</body>
</html>
