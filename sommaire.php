<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <title>Le site d'Orléans et son AgglO - Sommaire</title>
  <?php include( 'blocs/styles.php') ?>

    <body>
        <?php include( 'blocs/header.php') ?>
        <div class="container sommaire">
            <section>
                <ol class="breadcrumb hidden-xs">
                    <li><a href="index.php">Accueil</a>
                    </li>
                    <li><a href="#">Mes loisirs</a>
                    </li>
                    <li><a href="#">Expositions - Musées</a>
                    </li>
                    <li class="active">Musées</li>
                </ol>
                <h1>Musées</h1>
                <div class="row liste-item">
                    <article class="col-sm-6 col-md-4 col-lg-3 item">
                        <div class="visuel">
                            <a href="#">
                                <figure>
                                    <img class="img-responsive" src="http://www.orleans.fr/uploads/pics/musee_beaux_arts_bandeau.jpg" alt="">
                                    <figcaption class="sr-only"></figcaption>
                                </figure>
                            </a>
                        </div>
                        <div class="caption sommaire">
                            <h3><a href="#">Le musée des Beaux-Arts</a></h3>
                            <p>Découvrez le programme du 25 septembre 2015 au 25 mars 2016.</p>
                        </div>
                    </article>
                    <article class="col-sm-6 col-md-4 col-lg-3">
                        <div class="visuel">
                            <a href="#">
                                <figure>
                                    <img class="img-responsive" src="http://www.orleans.fr/uploads/pics/musee_historique_bandeau.jpg" alt="">
                                    <figcaption class="sr-only"></figcaption>
                                </figure>
                            </a>
                        </div>
                        <div class="caption sommaire">
                            <h3><a href="#">Le musée historique et archéologique</a></h3>
                            <p>Installé dans l'hôtel Cabu, le musée abrite le trésor de Neuvy-en-Sullias, un ensemble exceptionnel de bronzes gaulois et gallo-romains...</p>
                        </div>
                    </article>
                    <article class="col-sm-6 col-md-4 col-lg-3">
                        <div class="visuel">
                            <a href="#">
                                <figure>
                                    <img class="img-responsive" src="http://www.orleans.fr/typo3temp/pics/7ebf5000c6.jpg" alt="">
                                    <figcaption class="sr-only"></figcaption>
                                </figure>
                            </a>
                        </div>
                        <div class="caption sommaire">
                            <h3><a href="#">Le fonds régional d'art contemporain (FRAC)</a></h3>
                            <p>Depuis sa création en 1991, le FRAC Centre constitue et enrichit une collection exceptionnelle consacrée à l'art contemporain.</p>
                        </div>
                    </article>
                    <article class="col-sm-6 col-md-4 col-lg-3">
                        <div class="visuel">
                            <a href="#">
                                <figure>
                                    <img class="img-responsive" src="http://www.orleans.fr/typo3temp/pics/645200d9ac.jpg" alt="">
                                    <figcaption class="sr-only"></figcaption>
                                </figure>
                            </a>
                        </div>
                        <div class="caption sommaire">
                            <h3><a href="#">Le Centre Charles Péguy</a></h3>
                            <p>Cet établissement municipal à caractère scientifique est entièrement consacré à l'écrivain, natif d'Orléans (1873-1914)...</p>
                        </div>
                    </article>
                    <article class="col-sm-6 col-md-4 col-lg-3">
                        <div class="visuel">
                            <a href="#">
                                <figure>
                                    <img class="img-responsive" src="http://www.orleans.fr/uploads/pics/maison_jeanne_darc_bandeau.jpg" alt="">
                                    <figcaption class="sr-only"></figcaption>
                                </figure>
                            </a>
                        </div>
                        <div class="caption sommaire">
                            <h3><a href="#">La maison de Jeanne d'Arc</a></h3>
                            <p>Cette maison à pans de bois était celle de Jacques Boucher, qui hébergea Jeanne d'Arc lorsqu'elle vint libérer la ville en 1429...</p>
                        </div>
                    </article>
                    <article class="col-sm-6 col-md-4 col-lg-3">
                        <div class="visuel">

                        </div>
                        <div class="caption sommaire">
                            <h3><a href="#">Le muséum des sciences naturelles</a></h3>
                            <p>Le muséum abrite des collections parmi les plus riches de France, réparties sur trois niveaux et présentées de façon vivante et attrayante...</p>
                        </div>
                    </article>
                    <article class="col-sm-6 col-md-4 col-lg-3">
                        <div class="visuel">
                            <a href="#">
                                <figure>
                                    <img class="img-responsive" src="http://www.orleans.fr/uploads/pics/cercil_bandeau.jpg" alt="">
                                    <figcaption class="sr-only"></figcaption>
                                </figure>
                            </a>
                        </div>
                        <div class="caption sommaire">
                            <h3><a href="#">CERCIL</a></h3>
                            <p>A la fois musée, mémorial et centre de documentation, le CERCIL a pour objectif d'expliquer le processus qui a rendu possible la Shoah...</p>
                        </div>
                    </article>
                    <article class="col-sm-6 col-md-4 col-lg-3">
                        <div class="visuel">
                            <a href="#">
                                <figure>
                                    <img class="img-responsive" src="http://www.orleans.fr/uploads/pics/musee_beaux_arts_bandeau.jpg" alt="">
                                    <figcaption class="sr-only"></figcaption>
                                </figure>
                            </a>
                        </div>
                        <div class="caption sommaire">
                            <h3><a href="#">Le musée des Beaux-Arts</a></h3>
                            <p>Découvrez le programme du 25 septembre 2015 au 25 mars 2016.</p>
                        </div>
                    </article>
            </div>
                </section>
        </div>
        </div>

        <?php include( 'blocs/footer.php'); ?>
        <?php include( 'blocs/scripts.php'); ?>
    </body>

</html>

