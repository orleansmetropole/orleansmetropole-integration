<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Le site d'Orléans et son AgglO - Liste d'événements</title>
<? include( 'blocs/styles.php') ?>
</head>
<body>
<? include( 'blocs/header.php') ?>
<div class="container agenda">
  <section>
    <div class="row">
      <div class="col-md-8 col-lg-9">
        <ol class="breadcrumb hidden-xs">
          <li><a href="index.php">Accueil</a> </li>
          <li class="active">Agenda</li>
        </ol>
        <h1>L'agenda</h1>
        <div id="agenda">
          <div class="row liste-item">
            <div class="col-sm-6 col-lg-4">
              <article> <a href="agenda-single.php">
                <div class="visuel">
                  <figure> <img class="img-responsive" src="http://www.orleans.fr/typo3temp/pics/a7a5423722.jpg" alt=""><span class="badge">Urbanisme </span>
                    <figcaption class="sr-only">Argonne : le quartier en pleine transformation</figcaption>
                  </figure>
                </div>
                <h2>Argonne : le quartier en pleine transformation</h2>
                </a> <span class="date">Le
                <time datetime="2015-02-03">Mardi 3 Février</time>
                </span>
                <p>Lancé en 2004 à l’Argonne, le programme de rénovation est aujourd’hui réalisé à 60%.</p>
              </article>
            </div>
            <div class="col-sm-6 col-lg-4">
              <article> <a href="agenda-single.php">
                <div class="visuel">
                  <figure> <img class="img-responsive" src="http://www.orleans.fr/typo3temp/pics/1a7a3a406f.jpg" alt=""><span class="badge">Festival de Loire </span>
                    <figcaption class="sr-only">Inscriptions pour les professionnels</figcaption>
                  </figure>
                </div>
                <h2>Inscriptions pour les professionnels</h2>
                </a> <span class="date">Le
                <time datetime="2015-02-03">Mardi 3 Février</time>
                </span>
                <p>Restaurateurs, artisans, commerçants … sont invités à envoyer leur candidature</p>
              </article>
            </div>
            <div class="col-sm-6 col-lg-4">
              <article> <a href="agenda-single.php">
                <div class="visuel">
                  <figure> <img class="img-responsive" src="http://www.orleans.fr/typo3temp/pics/a593400cd0.jpg" alt=""><span class="badge">Sport</span>
                    <figcaption class="sr-only">6 800 spectateurs dans les tribunes</figcaption>
                  </figure>
                </div>
                <h2>6 800 spectateurs dans les tribunes</h2>
                </a> <span class="date">Le
                <time datetime="2015-02-02">Lundi 2 Février</time>
                </span>
                <p>La compagnie du masque d'Or investit le Domaine de la Paillerie</p>
              </article>
            </div>
            <div class="col-sm-6 col-lg-4">
              <article> <a href="agenda-single.php">
                <div class="visuel">
                  <figure> <img class="img-responsive" src="http://www.orleans.fr/typo3temp/pics/6f9f5cf379.jpg" alt=""><span class="badge">Éducation </span>
                    <figcaption class="sr-only">Émis : vacances d'hiver sportives</figcaption>
                  </figure>
                </div>
                <h2>Émis : vacances d'hiver sportives</h2>
                </a> <span class="date">Le
                <time datetime="2015-01-30">Vendredi 30 Janvier</time>
                </span>
                <p>L’École municipale d’initiation sportive organise des stages à destination des 4-11 ans</p>
              </article>
            </div>
            <div class="col-sm-6 col-lg-4">
              <article> <a href="agenda-single.php">
                <div class="visuel">
                  <figure> <img class="img-responsive" src="http://www.orleans.fr/uploads/tx_artificaevents/carrefour_parents_bandeau.jpg" alt=""><span class="badge">Solidarité - santé</span>
                    <figcaption class="sr-only">Carrefour des parents : rendez-vous !</figcaption>
                  </figure>
                </div>
                <h2>Carrefour des parents : rendez-vous !</h2>
                </a> <span class="date">Le
                <time datetime="2015-01-29">Jeudi 29 Janvier</time>
                </span>
                <p>Il est ouvert à tous et organise chaque mois des échanges autour de la relation parents-enfants</p>
              </article>
            </div>
            <div class="col-sm-6 col-lg-4">
              <article> <a href="agenda-single.php">
                <div class="visuel">
                  <figure> <img class="img-responsive" src="http://www.orleans.fr/typo3temp/pics/388d6dd0f6.jpg" alt=""><span class="badge">Mairie - citoyenneté</span>
                    <figcaption class="sr-only">Recensement de la population</figcaption>
                  </figure>
                </div>
                <h2>Recensement de la population</h2>
                </a>
                <p>Le recensement de la population aura lieu du jeudi 15 janvier au samedi 21 février.</p>
              </article>
            </div>
            <div class="col-sm-6 col-lg-4">
              <article> <a href="agenda-single.php">
                <div class="visuel">
                  <figure> <img class="img-responsive" src="img/braderie.jpg" alt=""><span class="badge">Fête - foire - salon </span>
                    <figcaption class="sr-only">Braderie d’hiver</figcaption>
                  </figure>
                </div>
                <h2>Braderie d’hiver</h2>
                </a> <span class="date">Du
                <time datetime="2015-02-13">13 Février</time>
                au
                <time datetime="2015-02-14">14 Février</time>
                </span>
                <p>Vous êtes un aficionado de la Grande Braderie qui, chaque année, à la fin août, vous donne envie de démarrer la rentrée du bon pied !</p>
              </article>
            </div>
            <div class="col-sm-6 col-lg-4">
              <article> <a href="agenda-single.php">
                <div class="visuel">
                  <figure> <img class="img-responsive" src="img/drone.jpg" alt=""><span class="badge">Orléanoide</span>
                    <figcaption class="sr-only">« Lâcher de drones » par Hypnos 451</figcaption>
                  </figure>
                </div>
                <h2>« Lâcher de drones » par Hypnos 451</h2>
                </a> <span class="date">Du
                <time datetime="2015-02-13">13 Février</time>
                au
                <time datetime="2015-02-14">14 Février</time>
                </span>
                <p>La compagnie du masque d'Or investit le Domaine de la Paillerie</p>
              </article>
            </div>
            <div class="col-sm-6 col-lg-4">
              <article> <a href="agenda-single.php">
                <div class="visuel">
                  <figure> <img class="img-responsive" src="http://www.orleans.fr/typo3temp/pics/a7a5423722.jpg" alt=""><span class="badge">Economie</span>
                    <figcaption class="sr-only">Le Lab’O, incubateur numérique à Orléans</figcaption>
                  </figure>
                </div>
                <h2>Le Lab’O, incubateur numérique à Orléans</h2>
                </a> <span class="date">Le
                <time datetime="2015-01-28">Mercredi 28 Janvier</time>
                </span>
                <p>Famar est en cours de réhabilitation pour accueillir des entreprises innovantes du numérique.</p>
              </article>
            </div>
            <div class="col-sm-6 col-lg-4">
              <article> <a href="agenda-single.php">
                <div class="visuel">
                  <figure> <img class="img-responsive" src="http://www.orleans.fr/typo3temp/pics/a7a5423722.jpg" alt=""><span class="badge">Urbanisme </span>
                    <figcaption class="sr-only">Argonne : le quartier en pleine transformation</figcaption>
                  </figure>
                </div>
                <h2>Argonne : le quartier en pleine transformation</h2>
                </a> <span class="date">Le
                <time datetime="2015-02-03">Mardi 3 Février</time>
                </span>
                <p>Lancé en 2004 à l’Argonne, le programme de rénovation est aujourd’hui réalisé à 60%.</p>
              </article>
            </div>
            <div class="col-sm-6 col-lg-4">
              <article> <a href="agenda-single.php">
                <div class="visuel">
                  <figure> <img class="img-responsive" src="http://www.orleans.fr/typo3temp/pics/1a7a3a406f.jpg" alt=""><span class="badge">Festival de Loire </span>
                    <figcaption class="sr-only">Inscriptions pour les professionnels</figcaption>
                  </figure>
                </div>
                <h2>Inscriptions pour les professionnels</h2>
                </a> <span class="date">Le
                <time datetime="2015-02-03">Mardi 3 Février</time>
                </span>
                <p>Restaurateurs, artisans, commerçants … sont invités à envoyer leur candidature</p>
              </article>
            </div>
            <div class="col-sm-6 col-lg-4">
              <article> <a href="agenda-single.php">
                <div class="visuel">
                  <figure> <img class="img-responsive" src="http://www.orleans.fr/typo3temp/pics/a593400cd0.jpg" alt=""><span class="badge">Sport</span>
                    <figcaption class="sr-only">6 800 spectateurs dans les tribunes</figcaption>
                  </figure>
                </div>
                <h2>6 800 spectateurs dans les tribunes</h2>
                </a> <span class="date">Le
                <time datetime="2015-02-02">Lundi 2 Février</time>
                </span>
                <p>La compagnie du masque d'Or investit le Domaine de la Paillerie</p>
              </article>
            </div>
          </div>
          <nav class="text-center">
            <h2 class="sr-only">Pagination</h2>
            <? include( "blocs/pagination.php") ?>
          </nav>
        </div>
      </div>
      <div class="col-md-4 col-lg-3">
        <? include( "blocs/agenda-filter.php"); ?>
      </div>
    </div>
  </section>
</div>
<? include( 'blocs/footer.php'); ?>
<? include( 'blocs/scripts.php'); ?>
</body>
</html>
