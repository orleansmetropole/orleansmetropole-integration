<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Le site d'Orléans et son AgglO - Accueil</title>
<?php include('blocs/styles.php') ?>
<body>
<?php include('blocs/header.php') ?>
<div class="container formulaire">
	<ol class="breadcrumb hidden-xs">
		<li><a href="index.php">Accueil</a></li>
		<li><a href="#">Formulaire de contacte</a></li>
		<li><a href="#">Intervention </a></li>
		<li class="active"> Demande d'arrêté de circulation</li>
	</ol>
                <header class="header-article accroche">
                    <h1>Demande d'arrêté de circulation</h1>
                    <div class="infos-formulaire"><strong>Vous pouvez faire une demande :</strong>
                        <ul>
                            <li>lorsqu'il y a une gêne à la circulation (véhicules, cycles, piétons),</li>
                            <li>lorsqu'il y a besoin impératif de neutraliser du stationnement,</li>
                            <li>lorsqu'il n'y a pas de stationnement.</li>
                        </ul>
                        <p>Veuillez effectuer votre demande 15 jours ouvrés&nbsp;avant le début de l'intervention.</p>
                    </div>
                </header>
	<p class="text-center"><a href="formulaire-etape1.php" class="btn-lg btn-primary"><span class="fa fa-arrow-right"></span> Faire une demande</a></p><br>       	
</div>
<?php include('blocs/footer.php'); ?>
<?php include('blocs/scripts.php'); ?>
</body>
</html>



