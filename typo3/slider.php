<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>Le site d'Orléans et son AgglO - Liste d'événements</title>
    <!-- CSS en environnement de d�veloppement -->
    <!--<link href="theme/dist/css/bootstrap.css" rel="stylesheet">-->
    <!--<link href="theme/dist/css/bootstrap-theme.css" rel="stylesheet">-->
    <link href="../theme_2/dist/css/main.css" rel="stylesheet">
    <!-- ---------------------------------- -->
    <!-- CSS en environnement de production -->
    <!-- <link href="theme/dist/css/bootstrap.min.css" rel="stylesheet"> -->
    <!-- <link href="theme/dist/css/bootstrap-theme.min.css" rel="stylesheet"> -->

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]--></head>

<body>
<?php include('../blocs/header.php') ?>

<div class="container">

    <div class="row">

        <div class="col-lg-9">
            <section>
                <div id="carousel-home" class="carousel carousel-home slide carousel-fade" data-ride="carousel">
                    <h2 class="sr-only">Slider</h2>
                    <!-- Indicators -->
                    <ol class="carousel-indicators">
                        <li data-target="#carousel-home" data-slide-to="0" class="active"></li>
                        <li data-target="#carousel-home" data-slide-to="1" class=""></li>
                        <li data-target="#carousel-home" data-slide-to="2" class=""></li>
                        <li data-target="#carousel-home" data-slide-to="3" class=""></li>
                    </ol>

                    <!-- Wrapper for slides -->
                    <div class="carousel-inner">
                        <section class="item active">
                            <div class="bg"></div>
                            <figure>
                                <img class="img-responsive"
                                     src="http://www.orleans-metropole-typo3-9x.test/fileadmin/_processed_/2/7/csm_delaperche002_7627286f0f.jpg"
                                     width="940" height="410" alt="">
                                <figcaption class="sr-only">Jean-Marie Delaperche, un artiste face aux tourments de
                                    l'histoire
                                </figcaption>
                            </figure>
                            <div class="carousel-caption">
                                <span class="badge"><!-- <f : render partial="Category/Primary" arguments="{datas:data.category}" /> --></span>
                                <h3><a href="">Jean-Marie Delaperche, un artiste face aux tourments de l'histoire</a>
                                </h3>
                                <p>Du 1er février au 14 juin 2020, le Musée des beaux-arts d’Orléans présente une
                                    exposition exceptionnelle, révélant l’histoire et le parcours d’un artiste qui était
                                    resté dans l’ombre jusque-là. Une enquête artistique absolument bouleversante.</p>
                                <p class="text-right-lg hidden-sm hidden-xs">
                                    <a href="" target="_self" class="btn btn-warning btn-alt text-uppercase">
                                        <span class="fa fa-plus"></span> Lire la suite
                                    </a>
                                </p>
                            </div>
                        </section>


                        <section class="item">
                            <div class="bg"></div>


                            <figure>

                                <img class="img-responsive"
                                     src="http://www.orleans-metropole-typo3-9x.test/fileadmin/_processed_/c/c/csm_michel-dubois_ac608eadca.jpg"
                                     width="940" height="410" alt="">

                                <figcaption class="sr-only">La sérigraphie s’affiche</figcaption>
                            </figure>


                            <div class="carousel-caption">
                                <span class="badge"><!-- <f : render partial="Category/Primary" arguments="{datas:data.category}" /> --></span>
                                <h3><a href="">La sérigraphie s’affiche</a></h3>
                                <p>Du 8 février au 5 avril 2020, la Ville d’Orléans présente l’exposition « La
                                    sérigraphie s’affiche ». Une plongée dans l’univers du célèbre sérigraphe et acteur
                                    passionné de la vie culturelle orléanaise Michel Dubois, à la Collégiale
                                    Saint-Pierre-le-Puellier.</p>
                                <p class="text-right-lg hidden-sm hidden-xs">
                                    <a href="" target="_self" class="btn btn-warning btn-alt text-uppercase">
                                        <span class="fa fa-plus"></span> Lire la suite
                                    </a>
                                </p>
                            </div>
                        </section>


                        <section class="item">
                            <div class="bg"></div>


                            <figure>

                                <img class="img-responsive"
                                     src="http://www.orleans-metropole-typo3-9x.test/fileadmin/_processed_/b/9/csm_saint-valentin-bandeau2020_b820e916c7.jpg"
                                     width="940" height="410" alt="">

                                <figcaption class="sr-only">Orléans fête la Saint Valentin</figcaption>
                            </figure>


                            <div class="carousel-caption">
                                <span class="badge"><!-- <f : render partial="Category/Primary" arguments="{datas:data.category}" /> --></span>
                                <h3><a href="">Orléans fête la Saint Valentin</a></h3>
                                <p>Pour la quatrième année consécutive, la mairie d’Orléans et les commerçants du
                                    centre-ville s’associent pour célébrer la Saint Valentin.</p>
                                <p class="text-right-lg hidden-sm hidden-xs">
                                    <a href="" target="_self" class="btn btn-warning btn-alt text-uppercase">
                                        <span class="fa fa-plus"></span> Lire la suite
                                    </a>
                                </p>
                            </div>
                        </section>


                        <section class="item">
                            <div class="bg"></div>


                            <figure>

                                <img class="img-responsive"
                                     src="http://www.orleans-metropole-typo3-9x.test/fileadmin/_processed_/c/e/csm_msp-hpm_35d8210bed.jpg"
                                     width="940" height="410" alt="">

                                <figcaption class="sr-only">Une 4ème maison de santé à Orléans</figcaption>
                            </figure>


                            <div class="carousel-caption">
                                <span class="badge"><!-- <f : render partial="Category/Primary" arguments="{datas:data.category}" /> --></span>
                                <h3><a href="">Une 4ème maison de santé à Orléans</a></h3>
                                <p>Une nouvelle maison de santé verra le jour d’ici début juin 2020 et se situera dans
                                    les locaux des urgences pédiatriques de l’ancien hôpital Porte Madeleine.</p>
                                <p class="text-right-lg hidden-sm hidden-xs">
                                    <a href="" target="_self" class="btn btn-warning btn-alt text-uppercase">
                                        <span class="fa fa-plus"></span> Lire la suite
                                    </a>
                                </p>
                            </div>
                        </section>

                    </div>
                    <!-- Controls -->
                    <a class="left carousel-control" href="#carousel-home" role="button" data-slide="prev"> <span
                                class="fa fa-chevron-left"></span></a>
                    <a class="right carousel-control" href="#carousel-home" role="button" data-slide="next"> <span
                                class="fa fa-chevron-right"></span></a>
                </div>
            </section>
        </div>

        <div class="col-lg-3">
            <section>
                <nav class="quicklink text-center">
                    <h2 class="sr-only">Accès rapides</h2>
                    <div class="well">
                        <ul class="list-unstyled row">


                            <li class="col-xs-12 col-sm-12 col-md-12 col-lg-12">


                                <a href="/demarches" class="btn btn-default">
                                    <span class="fa rounded "></span><br>
                                    <span class="text-inner">Démarches en ligne</span>
                                </a>
                            </li>


                            <li class="col-xs-6 col-sm-6 col-md-6 col-lg-6">


                                <a href="/sortir" class="btn btn-default">
                                    <span class="fa rounded "></span><br>
                                    <span class="text-inner">Notre agenda</span>
                                </a>
                            </li>


                            <li class="col-xs-6 col-sm-6 col-md-6 col-lg-6">


                                <a href="/3/plan-dorleans" class="btn btn-default">
                                    <span class="fa rounded "></span><br>
                                    <span class="text-inner">Plan de la ville</span>
                                </a>
                            </li>


                            <li class="col-xs-6 col-sm-6 col-md-6 col-lg-6">


                                <a href="/3/plan-dorleans" class="btn btn-default">
                                    <span class="fa rounded "></span><br>
                                    <span class="text-inner">Plan de la ville</span>
                                </a>
                            </li>


                            <li class="col-xs-6 col-sm-6 col-md-6 col-lg-6">


                                <a href="/dechets" class="btn btn-default">
                                    <span class="fa rounded "></span><br>
                                    <span class="text-inner">Les déchets</span>
                                </a>
                            </li>


                            <li class="col-xs-6 col-sm-4 col-md-2 col-lg-12">
                                <div class="dropup">
                                    <button class="btn btn-primary dropdown-toggle" type="button"
                                            id="dropdownAccesDirect" data-toggle="dropdown" aria-expanded="true"><span
                                                class="caret hidden"></span> <br class="hidden">
                                        <span class="fa fa-plus inverse rounded"></span> <br> Autres
                                    </button>
                                    <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownAccesDirect">


                                        <li role="presentation"><a href="/economie-emploi">Emploi</a></li>


                                        <li role="presentation"><a href="/publications">Publications</a></li>


                                        <li role="presentation"><a href="/1/photos/videos">Photos / vidéos</a></li>
                                    </ul>
                                </div>
                            </li>


                        </ul>
                    </div>
                </nav>
            </section>
        </div>

    </div>

</div>

<div class="zoom-sur">
    <div class="container">
        <h2>Zoom sur...</h2>
        <div id="carousel-zoom" class="carousel carousel-zoom slide carousel-fade" data-ride="carousel">

            <h2 class="sr-only">Slider</h2>

            <!-- Indicators -->
            <ol class="carousel-indicators">

                <li data-target="#carousel-zoom" data-slide-to="0" class="active"></li>

                <li data-target="#carousel-zoom" data-slide-to="1" class=""></li>

                <li data-target="#carousel-zoom" data-slide-to="2" class=""></li>

                <li data-target="#carousel-zoom" data-slide-to="3" class=""></li>

                <li data-target="#carousel-zoom" data-slide-to="4" class=""></li>

                <li data-target="#carousel-zoom" data-slide-to="5" class=""></li>

                <li data-target="#carousel-zoom" data-slide-to="6" class=""></li>

            </ol>

            <!-- Wrapper for slides -->
            <div class="carousel-inner">


                <section class="item active">
                    <div class="bg"></div>


                    <figure>

                        <img class="img-responsive"
                             src="http://www.orleans-metropole-typo3-9x.test/fileadmin/_processed_/4/1/csm_vitre-bus-elec_1162f600e3.jpg"
                             width="940" height="410" alt="">

                        <figcaption class="sr-only">Orléans Métropole sur la voie du zéro carbone</figcaption>
                    </figure>


                    <div class="carousel-caption">
                        <span class="badge"><!-- <f : render partial="Category/Primary" arguments="{datas:data.category}" /> --></span>
                        <h3><a href="">Orléans Métropole sur la voie du zéro carbone</a></h3>
                        <p>Suite à l’expérimentation de différents modèles depuis 2017, la commission d’appel
                            d’offres métropolitaine s’est réunie le 19 décembre afin d’attribuer le marché de bus
                            électriques. La société espagnole Irizar est pressentie.</p>
                        <p class="text-right-lg hidden-sm hidden-xs">
                            <a href="" target="_self" class="btn btn-warning btn-alt text-uppercase">
                                <span class="fa fa-plus"></span> Lire la suite
                            </a>
                        </p>
                    </div>
                </section>


                <section class="item">
                    <div class="bg"></div>


                    <figure>

                        <img class="img-responsive"
                             src="http://www.orleans-metropole-typo3-9x.test/fileadmin/_processed_/e/2/csm_anru2-lasource_5c39fd8f7e.jpg"
                             width="940" height="410" alt="">

                        <figcaption class="sr-only">Rénovation urbaine : les quartiers se transforment</figcaption>
                    </figure>


                    <div class="carousel-caption">
                        <span class="badge"><!-- <f : render partial="Category/Primary" arguments="{datas:data.category}" /> --></span>
                        <h3><a href="">Rénovation urbaine : les quartiers se transforment</a></h3>
                        <p>Orléans Métropole, les villes d’Orléans et de Saint-Jean-de-la-Ruelle, les bailleurs,
                            l’État, la Région et l’Anru viennent de signer la convention pluriannuelle des projets
                            de renouvellement urbain sur le territoire. </p>
                        <p class="text-right-lg hidden-sm hidden-xs">
                            <a href="" target="_self" class="btn btn-warning btn-alt text-uppercase">
                                <span class="fa fa-plus"></span> Lire la suite
                            </a>
                        </p>
                    </div>
                </section>


                <section class="item">
                    <div class="bg"></div>


                    <figure>

                        <img class="img-responsive"
                             src="http://www.orleans-metropole-typo3-9x.test/fileadmin/_processed_/1/6/csm_parc-loire004_de1347593b.jpg"
                             width="940" height="410" alt="">

                        <figcaption class="sr-only">Parc de Loire</figcaption>
                    </figure>


                    <div class="carousel-caption">
                        <span class="badge"><!-- <f : render partial="Category/Primary" arguments="{datas:data.category}" /> --></span>
                        <h3><a href="">Parc de Loire</a></h3>

                        <p class="text-right-lg hidden-sm hidden-xs">
                            <a href="" target="_self" class="btn btn-warning btn-alt text-uppercase">
                                <span class="fa fa-plus"></span> Lire la suite
                            </a>
                        </p>
                    </div>
                </section>


                <section class="item">
                    <div class="bg"></div>


                    <figure>

                        <img class="img-responsive"
                             src="http://www.orleans-metropole-typo3-9x.test/fileadmin/_processed_/f/5/csm_metropole-vue-aerienne_bb12d01999.jpg"
                             width="940" height="410" alt="">

                        <figcaption class="sr-only">Projet métropolitain 2017-2030</figcaption>
                    </figure>


                    <div class="carousel-caption">
                        <span class="badge"><!-- <f : render partial="Category/Primary" arguments="{datas:data.category}" /> --></span>
                        <h3><a href="">Projet métropolitain 2017-2030</a></h3>

                        <p class="text-right-lg hidden-sm hidden-xs">
                            <a href="" target="_self" class="btn btn-warning btn-alt text-uppercase">
                                <span class="fa fa-plus"></span> Lire la suite
                            </a>
                        </p>
                    </div>
                </section>


                <section class="item">
                    <div class="bg"></div>


                    <figure>

                        <img class="img-responsive"
                             src="http://www.orleans-metropole-typo3-9x.test/fileadmin/_processed_/1/b/csm_comet002_8d3f28c67e.jpg"
                             width="940" height="410" alt="">

                        <figcaption class="sr-only">Centre Orléans Métropole (CO'Met)</figcaption>
                    </figure>


                    <div class="carousel-caption">
                        <span class="badge"><!-- <f : render partial="Category/Primary" arguments="{datas:data.category}" /> --></span>
                        <h3><a href="">Centre Orléans Métropole (CO'Met)</a></h3>

                        <p class="text-right-lg hidden-sm hidden-xs">
                            <a href="" target="_self" class="btn btn-warning btn-alt text-uppercase">
                                <span class="fa fa-plus"></span> Lire la suite
                            </a>
                        </p>
                    </div>
                </section>


                <section class="item">
                    <div class="bg"></div>


                    <figure>

                        <img class="img-responsive"
                             src="http://www.orleans-metropole-typo3-9x.test/fileadmin/_processed_/d/f/csm_centre-aqualudique-002_e17686b0db.jpg"
                             width="940" height="410" alt="">

                        <figcaption class="sr-only">Centre aqualudique L’O</figcaption>
                    </figure>


                    <div class="carousel-caption">
                        <span class="badge"><!-- <f : render partial="Category/Primary" arguments="{datas:data.category}" /> --></span>
                        <h3><a href="">Centre aqualudique L’O</a></h3>

                        <p class="text-right-lg hidden-sm hidden-xs">
                            <a href="" target="_self" class="btn btn-warning btn-alt text-uppercase">
                                <span class="fa fa-plus"></span> Lire la suite
                            </a>
                        </p>
                    </div>
                </section>


                <section class="item">
                    <div class="bg"></div>


                    <figure>

                        <img class="img-responsive"
                             src="http://www.orleans-metropole-typo3-9x.test/fileadmin/_processed_/1/7/csm_CNLS-3_b29bdc3798.jpg"
                             width="940" height="410" alt="">

                        <figcaption class="sr-only">Réhabilitation du Complexe Nautique de La Source</figcaption>
                    </figure>


                    <div class="carousel-caption">
                        <span class="badge"><!-- <f : render partial="Category/Primary" arguments="{datas:data.category}" /> --></span>
                        <h3><a href="">Réhabilitation du Complexe Nautique de La Source</a></h3>
                        <p>Le groupement pressenti pour cette réhabilitation a été annoncé lors d'une conférence de
                            presse le vendredi 19 janvier au Complexe Nautique d’Orléans La Source</p>
                        <p class="text-right-lg hidden-sm hidden-xs">
                            <a href="" target="_self" class="btn btn-warning btn-alt text-uppercase">
                                <span class="fa fa-plus"></span> Lire la suite
                            </a>
                        </p>
                    </div>
                </section>

            </div>
            <!-- Controls -->
            <a class="left carousel-control" href="#carousel-zoom" role="button" data-slide="prev"> <span
                        class="fa fa-chevron-left"></span></a>
            <a class="right carousel-control" href="#carousel-zoom" role="button" data-slide="next"> <span
                        class="fa fa-chevron-right"></span></a>
        </div>
    </div>

</div>

<div class="container">
    <h2>Slider grand projet ([zoom])</h2>
    <div id="carousel-zoom-project" class="carousel carousel-zoom slide carousel-fade" data-ride="carousel">

        <h2 class="sr-only">Slider</h2>

        <!-- Indicators -->
        <ol class="carousel-indicators">

            <li data-target="#carousel-zoom-project" data-slide-to="0" class=""></li>

            <li data-target="#carousel-zoom-project" data-slide-to="1" class=""></li>

            <li data-target="#carousel-zoom-project" data-slide-to="2" class=""></li>

            <li data-target="#carousel-zoom-project" data-slide-to="3" class="active"></li>

        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner">


            <section class="item" style="opacity: 1;">
                <div class="bg"></div>









                <figure>

                    <img class="img-responsive" src="http://www.orleans-metropole-typo3-9x.test/fileadmin/_processed_/1/b/csm_comet002_8d3f28c67e.jpg" width="940" height="410" alt="">

                    <figcaption class="sr-only">Métropole à seulement une heure de Paris,</figcaption>
                </figure>


                <div class="carousel-caption">
                    <span class="badge"><!-- <f : render partial="Category/Primary" arguments="{datas:data.category}" /> --></span>
                    <h3><a href="">Métropole à seulement une heure de Paris,</a></h3>
                    <p>Orléans veut se doter d’un outil « tout en un », baptisé CO'Met, capable d’accueillir une large gamme de manifestations nationales et internationales, économiques, culturelles et sportives.</p>

                </div>
            </section>


            <section class="item" style="opacity: 1;">
                <div class="bg"></div>









                <figure>

                    <img class="img-responsive" src="http://www.orleans-metropole-typo3-9x.test/fileadmin/_processed_/9/9/csm_comet005_06de5b90af.jpg" width="940" height="410" alt="">

                    <figcaption class="sr-only">Avec une jauge «discriminante» de 8000 places,</figcaption>
                </figure>


                <div class="carousel-caption">
                    <span class="badge"><!-- <f : render partial="Category/Primary" arguments="{datas:data.category}" /> --></span>
                    <h3><a href="">Avec une jauge «discriminante» de 8000 places,</a></h3>
                    <p>la salle de sport répond aux cahiers des charges de toutes les fédérations sportives.? Orléans Loiret basket en sera le club-résident.</p>

                </div>
            </section>


            <section class="item" style="opacity: 1;">
                <div class="bg"></div>









                <figure>

                    <img class="img-responsive" src="http://www.orleans-metropole-typo3-9x.test/fileadmin/_processed_/2/9/csm_comet006_586be8e414.jpg" width="940" height="410" alt="">

                    <figcaption class="sr-only">Le lauréat pressenti</figcaption>
                </figure>


                <div class="carousel-caption">
                    <span class="badge"><!-- <f : render partial="Category/Primary" arguments="{datas:data.category}" /> --></span>
                    <h3><a href="">Le lauréat pressenti</a></h3>
                    <p>à la réalisation du projet a été présenté par Olivier Carré, Maire d’Orléans, Président d’Orléans Métropole et Philippe Pezet, conseiller municipal en charge du dossier, le 11 juillet 2017</p>

                </div>
            </section>


            <section class="item active" style="opacity: 1;">
                <div class="bg"></div>









                <figure>

                    <img class="img-responsive" src="http://www.orleans-metropole-typo3-9x.test/fileadmin/orleans/MEDIA/editorial/urbanisme_habitat/projets_urbains/comet-1ere-pierre.jpg" width="952" height="476" alt="">

                    <figcaption class="sr-only">La pose de la première pierre</figcaption>
                </figure>


                <div class="carousel-caption">
                    <span class="badge"><!-- <f : render partial="Category/Primary" arguments="{datas:data.category}" /> --></span>
                    <h3><a href="">La pose de la première pierre</a></h3>
                    <p>le 12 septembre 2019 marque le début d’un chantier hors-norme qui permettra à Orléans Métropole de se doter d’un outil unique en France, vecteur de rayonnement pour son territoire</p>

                </div>
            </section>

        </div>
        <!-- Controls -->
        <a class="left carousel-control" href="#carousel-zoom-project" role="button" data-slide="prev"> <span class="fa fa-chevron-left"></span></a>
        <a class="right carousel-control" href="#carousel-zoom-project" role="button" data-slide="next"> <span class="fa fa-chevron-right"></span></a>
    </div>
</div>


<?php include('../blocs/footer.php'); ?>
<script src="../theme_2/dist/js/main.js"></script>

</body>

</html>

