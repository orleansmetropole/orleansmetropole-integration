<!DOCTYPE html>
<html>

<head>
<meta charset="utf-8">
  <title>Le site d'Orléans et son AgglO - Liste d'associations</title>
  <? include( 'blocs/styles.php') ?>

    <body>
        <? include( 'blocs/header.php') ?>
        <div class="container asso">
        <section>
            <div class="row">
                <div class="col-md-8 col-lg-9">
                    <ol class="breadcrumb hidden-xs">
                        <li><a href="index.php">Accueil</a></li>
                        <li class="active">Associations</li>
                    </ol>
                    <h1>Annuaire des associations</h1>
                    <div class="row">
                        <article class="col-sm-6 col-lg-4 ">
                            <div class="infos asso">
                                <span class="badge">Nature-Environnement</span>
                                <h2><a href="asso-single.php">1 Terre Actions</a></h2>
                                <p>46 ter, rue Sainte Catherine
                                    <br>45000 Orléans</p>
                                <a href="#" target="_blank"><span class="fa fa-send"></span> Courriel</a>
                                <br>
                                <a href="#" target="_blank"><span class="fa fa-link"></span> Site web</a>
                                <br>
                                <span class="fa fa-phone"></span> : 06 02 50 09 23
                            </div>
                        </article>
                        <article class="col-sm-6 col-lg-4">
                            <div class="infos asso">
                                <span class="badge">Culture</span>
                                <h2><a href="asso-single.php">1, 2, 3... Lyrique</a></h2>
                                <p>29, rue Basse-d'Ingré
                                    <br>45000 Orléans</p>
                                <a href="#" target="_blank"><span class="fa fa-send"></span> Courriel</a>
                                <br>
                                <a href="#" target="_blank"><span class="fa fa-link"></span> Site web</a>
                                <br>
                                <span class="fa fa-phone"></span> : 02 38 53 22 82
                            </div>
                        </article>
                        <article class="col-sm-6 col-lg-4">
                            <div class="infos asso">
                                <span class="badge">Sports</span>
                                <h2><a href="asso-single.php">1ère Compagnie d'Arc d'Orléans</a></h2>
                                <p>Rue Eloy d'Ammerval
                                    <br>45000 Orléans</p>
                                <a href="#" target="_blank"><span class="fa fa-send"></span> Courriel</a>
                                <br>
                                <a href="#" target="_blank"><span class="fa fa-link"></span> Site web</a>
                                <br>
                                <span class="fa fa-phone"></span> : 02 38 62 17 22
                            </div>
                        </article>
                        <article class="col-sm-6 col-lg-4">
                            <div class="infos asso">
                                <span class="badge">Culture</span>
                                <h2><a href="asso-single.php">2 temps 3 mouvements</a></h2>
                                <p>108, rue de Bourgogne
                                    <br>45000 Orléans</p>
                                <a href="#" target="_blank"><span class="fa fa-send"></span> Courriel</a>
                                <br>
                                <a href="#" target="_blank"><span class="fa fa-link"></span> Site web</a>
                                <br>
                                <span class="fa fa-phone"></span> : 02 38 62 34 06
                            </div>
                        </article>
                        <article class="col-sm-6 col-lg-4">
                            <div class="infos asso">
                                <span class="badge">Culture</span>
                                <h2><a href="asso-single.php">45 Crew</a></h2>
                                <p>5, place Sainte Beuve
                                    <br>45100 Orléans</p>
                                <a href="#" target="_blank"><span class="fa fa-send"></span> Courriel</a>
                                <br>
                                <a href="#" target="_blank"><span class="fa fa-link"></span> Site web</a>
                                <br>
                                <span class="fa fa-phone"></span> : 06 19 01 53 20
                            </div>
                        </article>
                        <article class="col-sm-6 col-lg-4">
                            <div class="infos asso">
                                <span class="badge">Santé-Handicap</span>
                                <h2><a href="asso-single.php">A Bras Ouverts</a></h2>
                                <p>90, avenue de Suffren
                                    <br>75015 Paris</p>
                                <a href="#" target="_blank"><span class="fa fa-send"></span> Courriel</a>
                                <br>
                                <a href="#" target="_blank"><span class="fa fa-link"></span> Site web</a>
                                <br>
                                <span class="fa fa-phone"></span> : 09 82 46 60 83
                            </div>
                        </article>
                        <article class="col-sm-6 col-lg-4">
                            <div class="infos asso">
                                <span class="badge">Culture</span>
                                <h2><a href="asso-single.php">A Cent Danses</a></h2>
                                <p>Lieudit les Chateliers
                                    <br>45460 Bouzy la Forêt</p>
                                <a href="#" target="_blank"><span class="fa fa-send"></span> Courriel</a>
                                <br>
                                <a href="#" target="_blank"><span class="fa fa-link"></span> Site web</a>
                                <br>
                                <span class="fa fa-phone"></span> : 06 26 84 02 34
                            </div>
                        </article>
                        <article class="col-sm-6 col-lg-4">
                            <div class="infos asso">
                                <span class="badge">Culture</span>
                                <h2><a href="asso-single.php">A Corps &amp; Danse 45</a></h2>
                                <p>22, rue Auguste de St Hilaire apt 50
                                    <br>45000 Orléans</p>
                                <a href="#" target="_blank"><span class="fa fa-send"></span> Courriel</a>
                                <br>
                                <a href="#" target="_blank"><span class="fa fa-link"></span> Site web</a>
                                <br>
                                <span class="fa fa-phone"></span> : 06 26 84 02 34
                            </div>
                        </article>
                        <article class="col-sm-6 col-lg-4">
                            <div class="infos asso">
                                <span class="badge">Nature-Environnement</span>
                                <h2><a href="asso-single.php">1 Terre Actions</a></h2>
                                <p>46 ter, rue Sainte Catherine
                                    <br>45000 Orléans</p>
                                <br>
                                <a href="#" target="_blank"><span class="fa fa-send"></span> Courriel</a>
                                <br>
                                <a href="#" target="_blank"><span class="fa fa-link"></span> Site web</a>
                                <br>
                                <span class="fa fa-phone"></span> : 06 02 50 09 23
                            </div>
                        </article>
                        <article class="col-sm-6 col-lg-4">
                            <div class="infos asso">
                                <span class="badge">Culture</span>
                                <h2><a href="asso-single.php">1, 2, 3... Lyrique</a></h2>
                                <p>29, rue Basse-d'Ingré
                                    <br>45000 Orléans</p>
                                <a href="#" target="_blank"><span class="fa fa-send"></span> Courriel</a>
                                <br>
                                <a href="#" target="_blank"><span class="fa fa-link"></span> Site web</a>
                                <br>
                                <span class="fa fa-phone"></span> : 02 38 53 22 82
                            </div>
                        </article>
                        <article class="col-sm-6 col-lg-4">
                            <div class="infos asso">
                                <span class="badge">Sports</span>
                                <h2><a href="asso-single.php">1ère Compagnie d'Arc d'Orléans</a></h2>
                                <p>rue Eloy d'Ammerval
                                    <br>45000 Orléans</p>
                                <a href="#" target="_blank"><span class="fa fa-send"></span> Courriel</a>
                                <br>
                                <a href="#" target="_blank"><span class="fa fa-link"></span> Site web</a>
                                <br>
                                <span class="fa fa-phone"></span> : 02 38 62 17 22
                            </div>
                        </article>
                        <article class="col-sm-6 col-lg-4">
                            <div class="infos asso">
                                <span class="badge">Culture</span>
                                <h2><a href="asso-single.php">2 temps 3 mouvements</a></h2>
                                <p>108, rue de Bourgogne
                                    <br>45000 Orléans</p>
                                <a href="#" target="_blank"><span class="fa fa-send"></span> Courriel</a>
                                <br>
                                <a href="#" target="_blank"><span class="fa fa-link"></span> Site web</a>
                                <br>
                                <span class="fa fa-phone"></span> : 02 38 62 34 06
                            </div>
                        </article>
                        <article class="col-sm-6 col-lg-4">
                            <div class="infos asso">
                                <span class="badge">Culture</span>
                                <h2><a href="asso-single.php">45 Crew</a></h2>
                                <p>5, place Sainte Beuve
                                    <br>45100 Orléans</p>
                                <a href="#" target="_blank"><span class="fa fa-send"></span> Courriel</a>
                                <br>
                                <a href="#" target="_blank"><span class="fa fa-link"></span> Site web</a>
                                <br>
                                <span class="fa fa-phone"></span> : 06 19 01 53 20
                            </div>
                        </article>
                        <article class="col-sm-6 col-lg-4">
                            <div class="infos asso">
                                <span class="badge">Santé-Handicap</span>
                                <h2><a href="asso-single.php">A Bras Ouverts</a></h2>
                                <p>90, avenue de Suffren
                                    <br>75015 Paris</p>
                                <a href="#" target="_blank"><span class="fa fa-send"></span> Courriel</a>
                                <br>
                                <a href="#" target="_blank"><span class="fa fa-link"></span> Site web</a>
                                <br>
                                <span class="fa fa-phone"></span> : 09 82 46 60 83
                            </div>
                        </article>
                        <article class="col-sm-6 col-lg-4 hidden-sm hidden-md">
                            <div class="infos asso">
                                <span class="badge">Culture</span>
                                <h2><a href="asso-single.php">A Cent Danses</a></h2>
                                <p>Lieudit les Chateliers
                                    <br>45460 Bouzy la Forêt</p>
                                <a href="#" target="_blank"><span class="fa fa-send"></span> Courriel</a>
                                <br>
                                <a href="#" target="_blank"><span class="fa fa-link"></span> Site web</a>
                                <br>
                                <span class="fa fa-phone"></span> : 06 26 84 02 34
                            </div>
                        </article>
                    </div>
                    <nav class="text-center">
                    <h2 class="sr-only">Pagination</h2>
                        <? include( "blocs/pagination.php") ?>
                    </nav>
                </div>
                <div class="col-md-4 col-lg-3">
                    <? include( 'blocs/annuaire-filter.php'); ?>
                </div>
            </div>
            </section>
        </div>
        <? include( 'blocs/footer.php'); ?>
        <? include( 'blocs/scripts.php'); ?>
    </body>

</html>