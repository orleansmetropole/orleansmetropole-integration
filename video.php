<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Le site d'Orléans et son AgglO - Liste d'événements</title>
<? include( 'blocs/styles.php') ?>
</head>
<body>
<? include( 'blocs/header.php') ?>
<div class="container video">
  <section>
    <div class="row">
      <div class="col-md-8 col-lg-9">
        <ol class="breadcrumb hidden-xs hidden-sm">
          <li><a href="index.php"><span class="fa fa-home"></span></a> </li>
          <li><a href="#">Vidéos</a> </li>
          <li class="active">Vidéos</li>
        </ol>
        <header>
          <div class="row">
            <div class="col-sm-8">
              <h1>Vidéos</h1>
            </div>
            <div class="col-sm-4 hidden-xs">
              <? include( "blocs/social.php"); ?>
            </div>
          </div>
          <div class="accroche">
            <div class="embed-responsive embed-responsive-16by9">
              <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/NQFuS5tBVTQ?hl=fr&amp;fs=1&amp;rel=0&amp;wmode=opaque&amp;display=iframe&amp;pautohide=1"></iframe>
            </div>
            <div class="texte-accroche video">
              <h2>Hommage à Jean Zay : discours d'Olivier Carré</h2>
              <p>Depuis le mois d'octobre, le premier Fab Lab <i>(laboratoire de fabrication)</i> du département a ouvert ses portes à La Source.</p>
              <span class="date">Publiée le
              <time datetime="2015-04-09">Jeudi 9 Avril 2015</time>
              </span> </div>
          </div>
        </header>
        <div class="row liste-item">
          <div class="col-sm-6 col-lg-4">
            <article> <a href="#">
              <div class="visuel">
                <figure> <img class="img-responsive" src="http://img.youtube.com/vi/NQFuS5tBVTQ/0.jpg" alt="">
                  <figcaption class="sr-only"></figcaption>
                </figure>
              </div>
              <h3>Hommage à Jean Zay : discours d'Olivier Carré</h3>
              </a>
              <time datetime="2015-01-01" class="date">Publiée le Mardi 3 Février 2015</time>
              <p>Discours d'Olivier Carré, premier maire-adjoint d'Orléans à l'occasion de l'hommage d'Orléans à Jean Zay.</p>
            </article>
          </div>
          <div class="col-sm-6 col-lg-4">
            <article> <a href="#">
              <div class="visuel">
                <figure> <img class="img-responsive" src="http://img.youtube.com/vi/iHox9NyeV5g/0.jpg" alt="">
                  <figcaption class="sr-only"></figcaption>
                </figure>
              </div>
              <h3>Fêtes de Jeanne d'Arc 2015 - Défilé du 8 mai</h3>
              </a>
              <time datetime="2015-01-01" class="date">Publiée le Mardi 3 Février 2015</time>
              <p>Ambiances du défilé le 8 mai 2015 à Orléans en présence d'Audrey Pulvar.</p>
            </article>
          </div>
          <div class="col-sm-6 col-lg-4">
            <article> <a href="#">
              <div class="visuel">
                <figure> <img class="img-responsive" src="http://img.youtube.com/vi/awgbEoqPudU/0.jpg" alt="">
                  <figcaption class="sr-only"></figcaption>
                </figure>
              </div>
              <h3>Fêtes de Jeanne d'Arc 2015 - Audrey Pulvar, présidente des fêtes</h3>
              </a>
              <time datetime="2015-01-01" class="date">Publiée le Mardi 3 Février 2015</time>
              <p>Audrey Pulvar est accueillie par Olivier Carré, à l'Hôtel Groslot d'Orléans lors des fêtes de Jeanne d'Arc 2015.</p>
            </article>
          </div>
          <div class="col-sm-6 col-lg-4">
            <article> <a href="#">
              <div class="visuel">
                <figure> <img class="img-responsive" src="http://img.youtube.com/vi/k3CFpPEKuYg/0.jpg" alt="">
                  <figcaption class="sr-only"></figcaption>
                </figure>
              </div>
              <h3>Fêtes de Jeanne d'Arc 2015 - Discours d'Olivier Carré</h3>
              </a>
              <time datetime="2015-01-01" class="date">Publiée le Mardi 3 Février 2015</time>
              <p>Discours d'Olivier CARRÉ, Premier Maire-Adjoint de la ville d'Orléans, lors de l'hommage officiel à Jeanne d'Arc.</p>
            </article>
          </div>
          <div class="col-sm-6 col-lg-4">
            <article> <a href="#">
              <div class="visuel">
                <figure> <img class="img-responsive" src="http://img.youtube.com/vi/YgZG9PlBKiY/0.jpg" alt="">
                  <figcaption class="sr-only"></figcaption>
                </figure>
              </div>
              <h3>Fêtes de Jeanne d'Arc 2015 - Discours d'Audrey Pulvar</h3>
              </a>
              <time datetime="2015-01-01" class="date">Publiée le Mardi 3 Février 2015</time>
              <p>Discours de la journaliste Audrey Pulvar, présidente les fêtes de Jeanne d'Arc 2015 d'Orléans.</p>
            </article>
          </div>
          <div class="col-sm-6 col-lg-4">
            <article> <a href="#">
              <div class="visuel">
                <figure> <img class="img-responsive" src="http://img.youtube.com/vi/H8Cj-kreTzo/0.jpg" alt="">
                  <figcaption class="sr-only"></figcaption>
                </figure>
              </div>
              <h3>Fêtes de Jeanne d'Arc 2015 - Set Electro</h3>
              </a>
              <time datetime="2015-01-01" class="date">Publiée le Mardi 3 Février 2015</time>
              <p>Ambiances du set électro des fêtes de Jeanne d'Arc - Le Jeudi 07 Mai 2015 - Avec Simon La Fougère Lauréat du « Orléans DJ CAST 2015 »</p>
            </article>
          </div>
          <div class="col-sm-6 col-lg-4">
            <article> <a href="#">
              <div class="visuel">
                <figure> <img class="img-responsive" src="http://img.youtube.com/vi/NQFuS5tBVTQ/0.jpg" alt="">
                  <figcaption class="sr-only"></figcaption>
                </figure>
              </div>
              <h3>Hommage à Jean Zay : discours d'Olivier Carré</h3>
              </a>
              <time datetime="2015-01-01" class="date">Publiée le Mardi 3 Février 2015</time>
              <p>Discours d'Olivier Carré, premier maire-adjoint d'Orléans à l'occasion de l'hommage d'Orléans à Jean Zay.</p>
            </article>
          </div>
          <div class="col-sm-6 col-lg-4">
            <article> <a href="#">
              <div class="visuel">
                <figure> <img class="img-responsive" src="http://img.youtube.com/vi/iHox9NyeV5g/0.jpg" alt="">
                  <figcaption class="sr-only"></figcaption>
                </figure>
              </div>
              <h3>Fêtes de Jeanne d'Arc 2015 - Défilé du 8 mai</h3>
              </a>
              <time datetime="2015-01-01" class="date">Publiée le Mardi 3 Février 2015</time>
              <p>Ambiances du défilé le 8 mai 2015 à Orléans en présence d'Audrey Pulvar.</p>
            </article>
          </div>
          <div class="col-sm-6 col-lg-4">
            <article> <a href="#">
              <div class="visuel">
                <figure> <img class="img-responsive" src="http://img.youtube.com/vi/awgbEoqPudU/0.jpg" alt="">
                  <figcaption class="sr-only"></figcaption>
                </figure>
              </div>
              <h3>Fêtes de Jeanne d'Arc 2015 - Audrey Pulvar, présidente des fêtes</h3>
              </a>
              <time datetime="2015-01-01" class="date">Publiée le Mardi 3 Février 2015</time>
              <p>Audrey Pulvar est accueillie par Olivier Carré, à l'Hôtel Groslot d'Orléans lors des fêtes de Jeanne d'Arc 2015.</p>
            </article>
          </div>
          <div class="col-sm-6 col-lg-4">
            <article> <a href="#">
              <div class="visuel">
                <figure> <img class="img-responsive" src="http://img.youtube.com/vi/k3CFpPEKuYg/0.jpg" alt="">
                  <figcaption class="sr-only"></figcaption>
                </figure>
              </div>
              <h3>Fêtes de Jeanne d'Arc 2015 - Discours d'Olivier Carré</h3>
              </a>
              <time datetime="2015-01-01" class="date">Publiée le Mardi 3 Février 2015</time>
              <p>Discours d'Olivier CARRÉ, Premier Maire-Adjoint de la ville d'Orléans, lors de l'hommage officiel à Jeanne d'Arc.</p>
            </article>
          </div>
          <div class="col-sm-6 col-lg-4">
            <article> <a href="#">
              <div class="visuel">
                <figure> <img class="img-responsive" src="http://img.youtube.com/vi/YgZG9PlBKiY/0.jpg" alt="">
                  <figcaption class="sr-only"></figcaption>
                </figure>
              </div>
              <h3>Fêtes de Jeanne d'Arc 2015 - Discours d'Audrey Pulvar</h3>
              </a>
              <time datetime="2015-01-01" class="date">Publiée le Mardi 3 Février 2015</time>
              <p>Discours de la journaliste Audrey Pulvar, présidente les fêtes de Jeanne d'Arc 2015 d'Orléans.</p>
            </article>
          </div>
          <div class="col-sm-6 col-lg-4">
            <article> <a href="#">
              <div class="visuel">
                <figure> <img class="img-responsive" src="http://img.youtube.com/vi/H8Cj-kreTzo/0.jpg" alt="">
                  <figcaption class="sr-only"></figcaption>
                </figure>
              </div>
              <h3>Fêtes de Jeanne d'Arc 2015 - Set Electro</h3>
              </a>
              <time datetime="2015-01-01" class="date">Publiée le Mardi 3 Février 2015</time>
              <p>Ambiances du set électro des fêtes de Jeanne d'Arc - Le Jeudi 07 Mai 2015 - Avec Simon La Fougère Lauréat du « Orléans DJ CAST 2015 »</p>
            </article>
          </div>
        </div>
        <div class="visible-xs text-center">
          <? include( "blocs/social.php"); ?>
        </div>
      </div>
      <div class="col-md-4 col-lg-3">
        <? include( "blocs/diapo-filter.php"); ?>
      </div>
    </div>
  </section>
</div>
<? include( 'blocs/footer.php'); ?>
<? include( 'blocs/scripts.php'); ?>
</body>
</html>
